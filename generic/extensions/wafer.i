/* -*- mode: c++ -*- */
/*
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%include <generic/extensions/macros.i>

GETSTRING(oaDesignInst, getName);
GETSTRINGWITHNS(oaDesignInst, getLibName);
GETSTRINGWITHNS(oaDesignInst, getCellName);
GETSTRINGWITHNS(oaDesignInst, getViewName);
GETSTRING(oaFrame, getName);
GETSTRING(oaFrameInst, getName);
GETSTRING(oaImage, getName);
GETSTRING(oaReticle, getName);
GETSTRINGWITHNS(oaReticleRef, getLibName);
GETSTRINGWITHNS(oaReticleRef, getCellName);
GETSTRINGWITHNS(oaReticleRef, getViewName);
GETSTRINGWITHNS(oaWafer, getLibName);
GETSTRINGWITHNS(oaWafer, getCellName);
GETSTRINGWITHNS(oaWafer, getViewName);
GETSTRING(oaWaferDesc, getName);
GETSTRING(oaWaferFeature, getName);
GETPOINT(oaDesignInst, getOrigin);
GETPOINT(oaFrameInst, getOrigin);
GETTRANSFORM(oaDesignInst, getTransform);
GETTRANSFORM(oaFrameInst, getTransform);
