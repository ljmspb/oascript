/* -*- mode: c++ -*- */
/*
   Copyright 2011 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%inline %{

class oaUUPoint
{
public:
    oaUUPoint(const OpenAccess_4::oaPoint& point, OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mPoint(point)
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    oaUUPoint(OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mPoint()
     , mTech(tech)
     , mViewType(viewType)
    {
    }

    oaUUPoint(OpenAccess_4::oaDouble xValIn, OpenAccess_4::oaDouble yValIn,
              OpenAccess_4::oaTech* tech, OpenAccess_4::oaViewType* viewType)
     : mPoint(tech->uuToDBU(viewType, xValIn), tech->uuToDBU(viewType, yValIn))
     , mTech(tech)
     , mViewType(viewType)
    {
        set(xValIn, yValIn);
    }

    oaUUPoint(const oaUUPoint& point, const OpenAccess_4::oaTransform& transform)
     : mPoint(point.getPoint(), transform)
     , mTech(point.mTech)
     , mViewType(point.mViewType)
    {
    }

    ~oaUUPoint()
    {
    }

    OpenAccess_4::oaDouble x() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mPoint.x());
    }

    OpenAccess_4::oaDouble y() const
    {
        checkTech();
        return mTech->dbuToUU(mViewType, mPoint.y());
    }

    void set(OpenAccess_4::oaDouble x, OpenAccess_4::oaDouble y)
    {
        checkTech();
        mPoint.set(mTech->uuToDBU(mViewType, x), mTech->uuToDBU(mViewType, y));
    }

    void transform(const OpenAccess_4::oaTransform& xform, oaUUPoint& result) const
    {
        mPoint.transform(xform, result.mPoint);
    }

    void transform(const OpenAccess_4::oaTransform& xform)
    {
        mPoint.transform(xform);
    }

    void transform(OpenAccess_4::oaDouble scale, OpenAccess_4::oaDouble angle, oaUUPoint& result) const
    {
        mPoint.transform(scale, angle, result.mPoint);
    }

    void transform(OpenAccess_4::oaDouble scale, OpenAccess_4::oaDouble angle)
    {
        mPoint.transform(scale, angle);
    }

    OpenAccess_4::oaDouble distanceFrom2(const oaUUPoint& point) const
    {
        checkTech();
        oaDouble d = mPoint.distanceFrom2(point.mPoint);
        return d / mTech->getDBUPerUU(mViewType);
    }

    oaUUPoint operator-() const
    {
        OpenAccess_4::oaPoint p = -mPoint;
        return oaUUPoint(p, mTech, mViewType);
    }

    oaUUPoint operator-(const oaUUPoint& pt) const
    {
        OpenAccess_4::oaPoint p = mPoint - pt.mPoint;
        return oaUUPoint(p, mTech, mViewType);
    }

    oaUUPoint operator+(const oaUUPoint& pt) const
    {
        OpenAccess_4::oaPoint p = mPoint + pt.mPoint;
        return oaUUPoint(p, mTech, mViewType);
    }

    const OpenAccess_4::oaPoint& getPoint() const
    {
        return mPoint;
    }

    OpenAccess_4::oaPoint& getPoint()
    {
        return mPoint;
    }

    OpenAccess_4::oaTech* getTech() const
    {
        return mTech;
    }

    OpenAccess_4::oaViewType* getViewType() const
    {
        return mViewType;
    }

private:
    void checkTech() const
    {
        if (!mTech->isValid()) {
            // make this into an exception!
            printf("invalid tech!\n");
        }
    }

private:
    OpenAccess_4::oaPoint mPoint;
    OpenAccess_4::oaTech* mTech;
    OpenAccess_4::oaViewType* mViewType;
};

%}

