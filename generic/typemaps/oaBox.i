/* ****************************************************************************

   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************

   Change Log:
     02/16/2010: Copied from Python bindings (authored by AMD) and modified by
                 Intel Corporation for Ruby.

 *************************************************************************** */

%header %{

int
langobj_to_oaBox (LANG_VALUE_TYPE langBox, oaBox& box)
{
    if (LANG_ARRAYP(langBox) && LANG_ARRAY_SIZE(langBox) == 4) {

	LANG_VALUE_TYPE langElt = LANG_ARRAY_INDEX(langBox, 0);
	oaInt4 intVal = (oaInt4) LANG_NUM_TO_LONG(langElt);
	box.left() = intVal;

	langElt = LANG_ARRAY_INDEX(langBox, 1);
	intVal = (oaInt4) LANG_NUM_TO_LONG(langElt);
	box.bottom() = intVal;

	langElt = LANG_ARRAY_INDEX(langBox, 2);
	intVal = (oaInt4) LANG_NUM_TO_LONG(langElt);
	box.right() = intVal;

	langElt = LANG_ARRAY_INDEX(langBox, 3);
	intVal = (oaInt4) LANG_NUM_TO_LONG(langElt);
	box.top() = intVal;

	return SWIG_OK;
    }

    void *argp;

    if (SWIG_IsOK(SWIG_ConvertPtr(langBox, &argp, SWIGTYPE_p_OpenAccess_4__oaBox,  0)) && argp != NULL) {
	box = *(reinterpret_cast<oaBox*>(argp));
	return SWIG_OK;
    }

    return SWIG_ERROR;
}

%}

%typemap(in) const OpenAccess_4::oaBox& (OpenAccess_4::oaBox tmpBox)
{
    int res;
    void *argp;
    
    if (SWIG_IsOK((res = langobj_to_oaBox($input, tmpBox))))
	$1 = &tmpBox;
    else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))) && argp != NULL)
	$1 = reinterpret_cast<$1_basetype *>(argp);
    else
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
}

%typemap(in) OpenAccess_4::oaBox
{
    int res;
    void *argp;
    
    if (SWIG_IsOK((res = langobj_to_oaBox($input, $1)))) {}
    else if (SWIG_IsOK((res = SWIG_ConvertPtr($input, &argp, $1_descriptor,  0))) && argp != NULL)
	$1 = *(reinterpret_cast<$1_basetype *>(argp));
    else
	SWIG_exception_fail(SWIG_ArgError(res), "in method '" "$symname" "', argument " "$argnum"" of type '" "$1_type""'");
}

%typecheck(0) const OpenAccess_4::oaBox&
{
  void *tmpVoidPtr;
  $1 = ((LANG_ARRAYP($input) && LANG_ARRAY_SIZE($input) == 4) ||
        SWIG_IsOK(SWIG_ConvertPtr($input, &tmpVoidPtr, $1_descriptor, 0)));
}

%typecheck(0) OpenAccess_4::oaBox = (const OpenAccess_4::oaBox&);

