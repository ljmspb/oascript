##########################################################################
#
# Copyright 2010 Si2, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
##########################################################################

# The log file needs to be started from scratch each run.
#
# We cannot fold this into testtarget.mk because the labs' makefile.defs
# files include this by itself.

ifeq "$(MAKELEVEL)" "0"
  CHECKLOG = checklog
  export LAB_LOGFILE = $(CURDIR)/laberrors.log
  $(shell rm -rf $(LAB_LOGFILE))
  $(info Logging to file $(LAB_LOGFILE))
endif


.PHONY: checklog

checklog: 
		@if [ -f $(LAB_LOGFILE) ] ;\
	    then  \
				echo "" ;\
	      echo "********** lab failures ******************" ;\
	      cat $(LAB_LOGFILE) ;\
	      echo "" ;\
	      exit 2 ;\
	    else \
				 echo "" ;\
	       echo "********** No errors in labs ******************" ;\
				 echo "" ;\
	  fi ;\
    echo ""
