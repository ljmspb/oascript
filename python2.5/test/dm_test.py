#!/usr/bin/env python
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

import sys
import os
here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '..'))

from optparse import OptionParser

import oa.dm

def main():
    parser = OptionParser()
    parser.add_option("--libdefs", action="store", type="string", dest="libdefs", metavar="FILE",
                      help="Location of OA library definition file.")
    parser.add_option("-q", "--quiet", action="store_true", dest="quiet", default=False,
                      help="Suppress output.")
    (options, args) = parser.parse_args()
    
    oa.oaDMInit()
    if (options.libdefs) :
        oa.oaLibDefList.openLibs(options.libdefs)
    else :
        oa.oaLibDefList.openLibs()

    ns = oa.oaNativeNS()

    for lib in oa.oaLib.getOpenLibs():
        libname = oa.oaString()
        lib.getName(ns, libname)
        if (not lib.getAccess(oa.oaLibAccess(oa.oacReadLibAccess))):
            print "lib %s: No access! " % libname
            continue
        libpath = oa.oaString()
        lib.getPath(libpath)
        if (not options.quiet) :
            print "lib %s path=%s" % (libname, libpath)
        for cell in lib.getCells():
            cellname = oa.oaString()
            cell.getName(ns, cellname)
            if (not options.quiet):
                print "  %s" % cellname
            for cv in cell.getCellViews():
                view = cv.getView()
                viewname = oa.oaString()
                view.getName(ns, viewname)
                if (not options.quiet):
                    print "    %s" % viewname
                for file in cv.getDMFiles():
                    filename = oa.oaString()
                    file.getPath(filename)
                    size = file.getOnDiskSize()
                    if (not options.quiet):
                        print "      %s (%d bytes)" % (os.path.basename(str(filename)), size)
                    
if __name__ == "__main__":
    main()
