#!/usr/bin/env python
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

import sys
import os
import os.path
import time
from optparse import OptionParser

here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '..'))

import oa.design

##############################################################################
# DEFAULTS
##############################################################################

# Layer definitions: name, lay_num, material_str
layers = [['poly',    1, 'poly'],
          ['ndiff',   2, 'nDiff'],
          ['pdiff',   3, 'pDiff'],
          ['contact', 4, 'cut'],
          ['metal1',  5, 'metal'],
          ['via1',    6, 'cut']]

unit_size = 500

ns = oa.oaNativeNS()

##############################################################################
# FUNCTIONS
##############################################################################
def show_time(title, fun):
    print '%-20s\n' % title
    timer = oa.oaTimer()

    fun();
    print '%4.2f\n' % timer.getElapsed()

def create_rects():
  # Create a bunch of rectangles
  for i in range(1, options.nshapes):
      box = oa.oaBox(i*unit_size, 0, (i+1)*unit_size, unit_size)
      oa.oaRect.create(block, use_lnum, use_pnum, box)

def create_paths():
  # Create a bunch of paths
  for i in range(1, options.nshapes):
      st = i*2*unit_size
      pts = [oa.oaPoint(st, 0), oa.oaPoint(st+i*unit_size, 0), oa.oaPoint(st+i*unit_size, i*unit_size)]
      oa.oaPath.create(block, use_lnum, use_pnum, unit_size/4, pts)

def iterate_shapes():
  # Iterate through all shapes in the block
  #??block.getShapes.each {|shape| shape.isValid}}
    for shape in (block.getShapes()) :
        shape.isValid()

##############################################################################
# ARGUMENTS
##############################################################################

parser = OptionParser()
parser.set_defaults(nshapes=1000000)

parser.add_option("-l", "--lib", dest="libname", help="library name to create (required)")
parser.add_option("-c", "--cell", dest="cellname", help="cell name to create (required)")
parser.add_option("-v", "--view", dest="viewname", help="view name to create (required)")
parser.add_option("-d", "--libdefs", dest="libdefs",
                  help="path to lib.defs library definition file")
parser.add_option("-n", "--nshapes", type="int", dest="nshapes", help="number of shapes to create")
                  
(options, args) = parser.parse_args()

sys.exit("Missing mandatory --lib argument") if options.libname is None else ''
sys.exit("Missing mandatory --cell argument") if options.cellname is None else ''
sys.exit("Missing mandatory --view argument") if options.viewname is None else ''

oa.oaDesignInit()

if options.libdefs:
    oa.oaLibDefList.openLibs(options.libdefs)
else:
    try:
        oa.oaLibDefList.openLibs()
    except:
        pass
    
# Create the library
if (os.path.exists(options.libname)):
    sys.exit("Directory already exists - remove the '" + options.libname + "' directory")

libname_scname = oa.oaScalarName(ns, options.libname)
cellname_scname = oa.oaScalarName(ns, options.cellname)
viewname_scname = oa.oaScalarName(ns, options.viewname)

#lib = oa.oaLib.create(libname_scname, options.libname,
#                      oa.oaLibMode(oa.oacSharedLibMode), 'oaDMFileSys')
lib = oa.oaLib.create(libname_scname, options.libname,
                      oa.oaLibMode('shared'), 'oaDMFileSys')

if (lib.getAccess(oa.oaLibAccess('write')) == None):
    die("lib '" + options.libname + "': Cannot get write access!")

# Create the tech in the new library
tech = oa.oaTech.create(lib)
for layer in layers:
    name, num, mat_str = layer
    oa.oaPhysicalLayer.create(tech, name, num, oa.oaMaterial(mat_str))
    
# Use the first layer number in creating shapes on drawing purpose
use_lnum = layers[1][1]
use_pnum = oa.oavPurposeNumberDrawing

# Create a design and top block
design = oa.oaDesign.open(libname_scname, cellname_scname, viewname_scname,
                         oa.oaViewType.get(oa.oaReservedViewType('maskLayout')),
                         'w')

block = oa.oaBlock.create(design)

show_time('Create rectangles', create_rects)
show_time('Create paths', create_paths)
show_time('Iterate shapes', iterate_shapes)

tech.save()
design.save()
design.close()
