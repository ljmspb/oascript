#!/usr/bin/env python2.5
################################################################################
#  Copyright {c} 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 {the "License" you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   04/27/11  10.0     bpfeil, Si2     Tutorial 10th Edition 
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction {CAI} projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library {UNIX man pages}
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation {www.swig.org}
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

# This is a simplified EMH lister.
# Changes from C++ version:
#     This does not print object IDs
#     This only deals with the 5-box model for each Domain:
#         Block/Module/Occ, Nets, Terms, Insts, InstTerms
#     The following are cut:
#          Pcell Params, Assignments, MustJoin
#

import sys
import os
here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '../..'))

from optparse import OptionParser

import oa.design



# ****************************** Globals ******************************

#>>>class EmhGlobals
#>>>  private_class_method :new
#>>>  @@globs              = nil

# Indentation increment. This does not change.
indent_inc = 2

# Global indent value for prettier log printing
indent = 0

# Global flags for clients to set based on how much info they want to see.
#
# Default to NOT expanding the Occ hierarchy into other Designs.
expand_occDesignInst_masters = False

dump_block_domain = True
dump_mod_domain   = True
dump_occ_domain   = True

skip_if_zero      = False
dont_skip_if_zero = False
expand_occDesignInst_masters = 0
dumpBlockDomain = 1;
dumpModDomain   = 1;
dumpOccDomain   = 1;

ns = oa.oaNativeNS()
 
oi_header_array = []
oih_null_status = 0




def add_indent():
    global indent 
    indent += indent_inc
  

def sub_indent():  
    global indent
    indent -= indent_inc
  

  # shc make sure this works
def do_indent():
    global indent
    space = " "
    for i in range(indent):
        pstr = "%s" % (space)
        sys.stdout.write(pstr) 
  

def log (mesg):
    do_indent()
    pstr = "%s" %  (mesg)
    sys.stdout.write(pstr)

def assertq (condition, statement_text):
    if not condition:
        pstr = "\n***ASSERT[FAIL] %s\n" % (statement_text);sys.stdout.write(pstr)
 #>>>   else:
#>>>        pstr = "\n***ASSERT[PASS] %s\n" % (statement_text)
    

def print_obj_type (obj):
    pstr = " %s" % ( obj.getType().getName() )
    sys.stdout.write(pstr)


def new_obj_base_id (obj):
    pstr = "\n"; sys.stdout.write(pstr) 
    do_indent()
    print_obj_type(obj)
#>>>    pstr = "%s" % ( " " ); sys.stdout.write(pstr)


def bound_status (ref): 
    if ref.isBound():
      pstr = " BOUND "
    else:
      pstr = " UNBOUND "
    sys.stdout.write(pstr)


def print_obj_name_scalar (obj, prepend=''):
    ns = oa.oaNativeNS()
    scalar_name = oa.oaScalarName( ns, " ")
    obj.getName(scalar_name )
    simple_string = scalar_name.get()
    pstr ="%s\"%s\"" % ( prepend, simple_string )
    sys.stdout.write(pstr)


def print_obj_name (obj, prepend='' ):
    if (obj.isOccObject()):
        if ( obj.isOccNet() | obj.isOccInst() ):
           pstr = "%s \"%s\"" % ( prepend, obj.getPathName() )
        else:
           pstr = "%s\"%s\"" % ( prepend, obj.getName() )
    else:
        pstr = "%s\"%s\"" % ( prepend, obj.getName() )
    sys.stdout.write(pstr)


def print_transform  (inst): 
    tranny = inst.getTransform()
    pstr = " %s" % ( tranny.orient().getName() )
    sys.stdout.write(pstr)
    pstr = " (%d,%d)" % ( tranny.xOffset(), tranny.yOffset() )
    sys.stdout.write(pstr) 


def print_only_lcv (container):
    global ns
    lib = oa.oaScalarName( ns, " ")
    cell = oa.oaScalarName( ns, " ")
    view = oa.oaScalarName( ns, " ")
    container.getLibName(lib) 
    container.getCellName(cell)
    container.getViewName(view)
    pstr = "\"%s|%s|%s\"" % ( lib.get(), cell.get(), view.get() )
    sys.stdout.write(pstr)


def get_obj_name (obj):
    return obj.getName()



def print_orig_lcv (mod): 
    mod.getOrig(oa.oaScalarName( ns, scNameOrig=''), 
                oa.oaScalarName( ns, scNameLib=''),
                oa.oaScalarName( ns, scNameCell=''), 
                oa.oaScalarName( ns, scNameView=''))

    do_indent()
    pstr = "ORIG=%s[%s|%s|%s]" % ( scNameOrig, scNameLib, scNameCell, scNameView )
    sys.stdout.write(pstr)


# Note to Barbara: this is a variable argument list. The last argument is
# optional and has the default value of true.
# shc make sure the following works and doesn't get warnings
def print_header_line (count, category, skip_if_zero=True ):
    if ( count == 0 and skip_if_zero):
        return False
  
    # force a linefeed
    pstr = "\n" ;sys.stdout.write(pstr)
    do_indent()

    if (count != 1 ):
        pstr = "[%d]%ss" % ( count, category )
    else:
        pstr = "[%d]%s" % ( count, category )
    sys.stdout.write(pstr)
    return True



# shc Note to Barbara: "objdef" was "def" in C++ and Tcl, but it's a reserved word in ruby.
#
def dump_busTermDefs (container, descrip):
    if  ( not print_header_line( container.getBusTermDefs().getCount(), descrip) ):
        return

    for objdef in container.getBusTermDefs():
#>>>  container.getBusTermDefs.each do |objdef|
        new_obj_base_id( objdef )
        pstr = ": BITS %d" % ( objdef.getNumBits() ); sys.stdout.write(pstr)
        pstr = " MINIX=%d" % ( objdef.getMinIndex() ); sys.stdout.write(pstr)
        pstr = " MAXIX=%d" % ( objdef.getMaxIndex() ); sys.stdout.write(pstr)
        pstr = "  BASENAME=%s" % ( objdef.getName() ); sys.stdout.write(pstr)
        pstr = "  BITS:",
        for bit in objdef.getBusterm_bits():
           pstr = " %d" % ( bit.getBitIndex() ); sys.stdout.write(pstr)



def print_sss (bus):  
    pstr = "[%d" % ( bus.getStart() ); sys.stdout.write(pstr)
    pstr =  ":%d" % ( bus.getStop() ); sys.stdout.write(pstr)
    pstr =  ":%d]" % ( bus.getStep() ); sys.stdout.write(pstr)



def dump_bits (obj): 
    new_obj_base_id ( obj )
    print_obj_name ( obj )
    if obj.isImplicit():
        pstr = " IM"
    else:
        pstr = " EX"
    sys.stdout.write(pstr)
  
    pstr = "BITS=%d" % ( obj.getNumBits() );sys.stdout.write(pstr)


def print_net_summary (net): 
    dump_bits( net )
    if ( net.getType() == oa.oacBusNetType ):
        print_sss( net )
  
    if net.isBitNet():
      # The following is programmed to more closely follow the C++ lab,
      # in which you have to downcast the Net
        bitNet = net
        if bitNet.isPreferredEquivalent():
            pstr = " ISPREF";sys.stdout.write(pstr)

        if not bitNet.getEquivalentNets().isEmpty():
            pstr = " EQUIVALENTS:";sys.stdout.write(pstr)
            for eqNet in bitNet.getEquivalentNets():
                pstr = " %s" % ( eqNet.getName() );sys.stdout.write(pstr)


    if not net.getTerms().isEmpty(): 
        pstr = "\n";sys.stdout.write(pstr)
        add_indent()
        log( "CONNECTS Terms:" )
  
        for term in net.getTerms():
             print_obj_name(term, " ")
  
        sub_indent()

    if (not net.getInstTerms().isEmpty() ):
        pstr = "\n";sys.stdout.write(pstr)
        add_indent()
        log( "CONNECTS InstTerms:")
        for instTerm in net.getInstTerms():
            instTerm.getTerm()
            if ( instTerm.isBound() ):
                pstr = " ";sys.stdout.write(pstr)
            else:
                pstr = " CANTBIND";sys.stdout.write(pstr)

            pstr = "\"%s|%s\"" % ( instTerm.getInst().getName(), instTerm.getTermName() )
            sys.stdout.write(pstr)
        
        sub_indent()
   


def print_domain_ref (ref): 
    if ( ref  and ref.isValid() ):
        pstr = "  ->";sys.stdout.write(pstr)
        print_obj_type( ref )
        pstr = " ";sys.stdout.write(pstr)
        return True
    
    return False


def print_occNet_summary (net):
  dump_bits( net )
  if ( net.getType() == oa.oacOccBusNetType ):
      print_sss( net )
  if ( net.getNumBits() == 1 ):
      obNet = net
      if (not obNet.getEquivalentNets().isEmpty() ):
          pstr = " EQUIVALENTS:";sys.stdout.write(pstr)
          for eqNet in obNet.getEquivalentNets():
              pstr = " ";sys.stdout.write(pstr)
              print_obj_name( eqNet, " ")
          
 
  if (not net.getTerms().isEmpty() ):
      pstr = "\n";sys.stdout.write(pstr)
      add_indent()
      log( "CONNECTS OccTerms:" )
      for term in net.getTerms():
          print_obj_name(term, " ");
          sub_indent()
 

  if (not net.getInstTerms().isEmpty() ):
      pstr = "\n";sys.stdout.write(pstr)
      add_indent()
      log( "CONNECTS OccInstTerms:" )
      for instTerm in net.getInstTerms():
        pstr = "\"%s" % ( instTerm.getInst().getName() );sys.stdout.write(pstr)
        pstr = "|%s\"" % ( instTerm.getTermName() );sys.stdout.write(pstr)
      
      sub_indent()
  
  # Cross-domain references
  #
  pstr = "\n";sys.stdout.write(pstr)
  do_indent()
  if ( print_domain_ref( net.getNet() ) ):
      print_obj_name( net.getNet(), "" )
  if ( print_domain_ref( net.getModNet() ) ):
      print_obj_name(net.getModNet(), "" )


def print_modNet_summary (net):
    dump_bits( net )
    if ( net.getType() == oa.oacModBusNetType ):
        print_sss( net ) 
    if ( net.getNumBits() == 1 ):
        mbNet = net
        if not mbNet.getEquivalentNets().isEmpty():
            pstr = " EQUIVALENTS:";sys.stdout.write(pstr)
        for eqNet in mbNet.getEquivalentNets():
            pstr = " %s" % ( eqNet.getName() );sys.stdout.write(pstr)
    
 
    if ( not net.getTerms().isEmpty() ):
        pstr = "\n";sys.stdout.write(pstr)
        add_indent()
        log( "CONNECTS ModTerms:" )
        for term in net.getTerms():
            print_obj_name(term, " ")
        sub_indent()
    
    if ( not net.getInstTerms().isEmpty() ):
        pstr = "\n";sys.stdout.write(pstr);sys.stdout.write(pstr)
        add_indent()
        log(  "CONNECTS ModInstTerms:" )
        for instTerm in net.getInstTerms():
            pstr = "\"%s"  % ( instTerm.getInst().getName() );sys.stdout.write(pstr)
            pstr = "|%s\"" % ( instTerm.getTermName() );sys.stdout.write(pstr)
      
        sub_indent()
    



def dump_net_members (net_collection):
    for net in net_collection:
        print_net_summary( net )
  
        name =  net.getType().getName()
        if ( name == "ScalarNet" ):
              x=0
        elif ( name == "BusNetBit"):
              x=0
        elif ( name == "BusNet" ):
            add_indent()
            dump_net_members( net.getSingleBitMembers() )
            sub_indent()
        elif ( name == "BundleNet" ):
            add_indent()
            dump_net_members( net.getMembers() )
            sub_indent()
        else : 
            msg = "Unrecognized net type: %s" % (net.getType().getName()),
            log( msg )
      
    


def dump_modNet_members (net_collection):

    for net in net_collection:  
        print_modNet_summary( net )
        name =  net.getType().getName()
        if ( name ==  "ModScalarNet" ):
            x=0
        elif ( name ==  "ModBusNetBit" ):
            x=0
        elif ( name == "ModBusNet" ):
            add_indent()
            dump_modNet_members( net.getSingleBitMembers() )
            sub_indent()
        elif ( name == "ModBundleNet" ):
            add_indent()
            dump_modNet_members( net.getMembers() )
            sub_indent()
        else:
            msg = "Unrecognized net type: %s" % (net.getType.getName())
            log( msg )
      
    

def dump_occNet_members (net_collection):
    for net in net_collection:  
        print_occNet_summary( net )
        name =  net.getType().getName()
        if ( name ==  "OccScalarNet" ):
            x=0
        elif ( name ==  "OccBusNetBit" ):
            x=0
        elif ( name == "OccBusNet" ):
            add_indent()
            dump_occNet_members( net.getSingleBitMembers() )
            sub_indent()
        elif ( name == "OccBundleNet" ):
            add_indent()
            dump_occNet_members( net.getMembers() )
            sub_indent()
        else:
            msg =  "Unrecognized net type: %s" % ( net.getType.getName() )
            log( msg )
      


def print_master_info (ref, nameType): 
    if ref.isBound() :
        master = ref.getMaster()
        pstr =  " BOUND=>";sys.stdout.write(pstr)
        print_obj_type( master )
        print_only_lcv( master )
    else:
        pstr = " UNBOUND=> %s" % ( nameType );sys.stdout.write(pstr)
  


# Specialization: Occ[Mod]Module masters have just a name, no LCV.
# Plus the hier traversal name for oaModModuleInstHeader
# is getMasterModule, not getMaster!
#
def print_occ_master_info (ref): 
    if ref.isBound():
        modMaster = ref.getMaster()
        pstr = " BOUND=>";sys.stdout.write(pstr)
        print_obj_type( modMaster )
        print_obj_name( modMaster )
    else:
        pstr = " UNBOUND=>MODULE";sys.stdout.write(pstr)


def print_mod_master_info (ref):
    if ref.isBound():
        modMaster = ref.getMasterModule()
        pstr = " BOUND=>";sys.stdout.write(pstr)
        print_obj_type( modMaster )
        print_obj_name( modMaster )
    else:
        pstr = " UNBOUND=>MODULE";sys.stdout.write(pstr)



def dump_master_occ (inst):
    global expand_occDesignInst_masters
    global oi_header_array

    wasBound = inst.isBound()
  
    pstr = "\n";sys.stdout.write(pstr)
    add_indent()
    if wasBound:
        log( "MASTEROCC=BOUND2=" )
    else:
        log( "MASTEROCC=UNBOUND" )
    
  
    name = inst.getType().getName()
    if ( name == "OccVectorInst" ): 
       x=0
    elif (  name == "OccVectorInstBit" ):
        pstr = "NOMASTER";sys.stdout.write(pstr)
    else:
        master_occ = inst.getMasterOccurrence()
        if master_occ.isValid():
            master_type = master_occ.getType().getName()
            assertq(eval('master_type == "Occurrence"'),
                  'master_type == "Occurrence"')
          #
          # Always expand OccModuleInsts, since they are part of this Design.
          # But only expand beyond the frontier into other Designs if flag is set.
          #
            if ( inst.isOccModuleInst() | expand_occDesignInst_masters ):
                if (not wasBound ): 
                    pstr = " BINDING2=";sys.stdout.write(pstr)

                dump_occ( master_occ )

        else:
          pstr = "CANTBIND";sys.stdout.write(pstr)

    sub_indent()


def maybe_add_new_occInstHeader (inst):
    global oi_header_array
    ih = inst.getHeader()
  
    # Add the Header to the list if it's not already there.
   

    not_found = True
    for oih in oi_header_array:
        if ( oih.getCellName() == ih.getCellName() ):
            not_found = False 
            break

    if not_found:
        oi_header_array.append(ih) 
  


def dump_occDesignInsts (occ):
    global oi_header_array
    global oih_null_status
    odi_array = []
  
    # Organize only the OccDesignInsts into a list
    #
    for inst in occ.getInsts():
#>>>    occ.getInsts.each {|inst| odi_array.push(inst) if inst.isOccDesignInst}
        if inst.isOccDesignInst():
            odi_array.append(inst)

  
    num_insts = len(odi_array)
  
    if ( not print_header_line(num_insts, "OCCDESIGNINST") ):
        return 
  
    for inst in odi_array:
      new_obj_base_id( inst )
      print_obj_name( inst )
  
      # If we were NOT called [indirectly] from dump_occDesignInsts_from_header
      # shc NOT SURE HOW TO DO THIS.
      if not oih_null_status :
          maybe_add_new_occInstHeader(inst) 
  
      # Cross-domain references
      #
      pstr = "\n";sys.stdout.write(pstr)
      do_indent()
      if print_domain_ref( inst.getInst() ):
           print_obj_name( inst.getInst() )
      if print_domain_ref( inst.getModInst() ):
           print_obj_name( inst.getModInst() ) 
  
      # No XFORMS in Occ Domain!!



def dump_occDesignInsts_from_header (ih):
    global oih_null_status
#>>>
    sys.stdout.write("\ndump_OccDesignInstsFromHeader\n")
#>>>
    if not print_header_line(ih.getInsts().getCount(), "OCCDESIGNINST"):
        return 
#>>>
    pstr= "\ndump_OccDesignInstsFromHeader getCount()=%d\n" % (ih.getInsts().getCount())
    sys.stdout.write(pstr)     
#>>>
    for inst in ih.getInsts():
      new_obj_base_id( inst )
      print_obj_name( inst )
  
      # Cross-domain references
      #
      pstr = "\n";sys.stdout.write(pstr)
      do_indent()
      if ( print_domain_ref( inst.getInst() ) ):
          print_obj_name( inst.getInst() )

      if ( print_domain_ref( inst.getModInst() ) ):
          print_obj_name( inst.getModInst() ) 
  
      # No XFORMS in Occ Domain!!
  
      add_indent()
      dump_one_inst( inst )
      sub_indent()
      oih_null_status = 1
      dump_master_occ( inst)
      oih_null_status = 0


def dump_occInstHeaders ():
    global oi_header_array
    # Need this cheesy code because there's no getInstHeaders in oaOccurrence
    # and OccInstHeaders are scoped to the Design.
    if not print_header_line( len(oi_header_array) , "OCCINSTHEADER"):  
        return
    for ih in oi_header_array:
      # Pcell processing is taken out of this ruby version.
      #
      if ( ih.isSubHeader() | ih.isSuperHeader() ):
         continue

      new_obj_base_id( ih )
      print_only_lcv( ih )
      print_master_info( ih, "DESIGN")
      pstr = "\n";sys.stdout.write(pstr)
      do_indent()
      print_domain_ref( ih.getInstHeader() )
      print_domain_ref( ih.getModInstHeader() )
      add_indent()
      dump_occDesignInsts_from_header( ih )
      sub_indent()



def dump_occModuleInsts (ih):

    if not print_header_line(ih.getInsts().getCount(), "OCCMODULEINST"):
        return
    for inst in ih.getInsts():
      new_obj_base_id( inst )
      print_obj_name( inst )
      add_indent()
      dump_one_inst( inst )
      sub_indent()
      dump_master_occ(inst)


def  dump_occModuleInstHeaders (occ):

    ih_coll = occ.getModuleInstHeaders()

    if not print_header_line(ih_coll.getCount(), "OCCMODULEINSTHEADER"):    
        return

    for ih in ih_coll:
      new_obj_base_id( ih )
      print_obj_name( ih )
      print_occ_master_info( ih )
  
      # Cross-domain reference
      #
      mmIH = ih.getModModuleInstHeader()
      pstr = "\n";sys.stdout.write(pstr)
      do_indent()
      print_domain_ref( mmIH )
      print_obj_name_scalar( mmIH )
  
      add_indent()
      dump_occModuleInsts( ih )
      sub_indent()


def dump_occNets (occ): 
    netColl = occ.getNets( oa.oacNetIterAll )
    if not print_header_line( netColl.getCount(), "OCCNET"):
        return
    dump_occNet_members( netColl )


def print_occTerm_summary (term):
    dump_bits( term )
    if ( term.getType().getName() == "OccBusTerm" ):
        print_sss( term ) 
  
    pstr = " %-6s" % ( term.getTermType().getName() );sys.stdout.write(pstr)
  
    con_net = term.getNet()
    if con_net.isValid():
        print_obj_name( con_net, " CONN2" ) 
  
    # Cross-domain references
    #
    pstr = "\n";sys.stdout.write(pstr)
    do_indent()
    if ( print_domain_ref( term.getTerm() ) ):
        print_obj_name( term.getTerm() )
    if ( print_domain_ref( term.getModTerm() ) ):
        print_obj_name( term.getModTerm() )

  
  
def dump_occTerm_members (term_coll):
    for term in term_coll:
        print_occTerm_summary(term)
        type = term.getType()
        if ( int(type) == int(oa.oacOccScalarTermType) ):
            x=0  
        elif ( int(type) == int(oa.oacOccBusTermBitType) ):
            x=0
        elif ( int(type) == int(oa.oacOccBusTermType) ):
            occ   = term.getOccurrence()
            start = term.getStart()
            stop  = term.getStop()
            incr  = term.getStep()
              
            baseName = term.getDef().getName()
            add_indent()
    
            ix=start
            while ( stop > ix ):
               ix += incr
               term_bit = oa.oaOccBusTermBit.find( occ, baseName, ix )
               print_occTerm_summary( term_bit )
            
            sub_indent()
        elif ( int(type) ==  int(oa.oacOccBundleTermType) ):
            add_indent()
            for mem_term in term.getMembers():
                print_occTerm_summary( mem_term )
            sub_indent()
        else:
            msg =  "Unrecognized OccTerm type: %s" % ( term.getType().getName() ) 
            log( msg )
  


def dump_occTerms (occ):
    if (not print_header_line(occ.getTerms().getCount(), "OCCTERM") ):
        return
    dump_occTerm_members( occ.getTerms() )

  

def dump_occ (occ): 
    print_obj_type( occ )
  
    # Cross-domain references
    #
    pstr = "\n";sys.stdout.write(pstr)
    do_indent()
    block = occ.getBlock()
    if ( print_domain_ref( block ) ):
        print_only_lcv( block.getDesign() ) 
  
    mod = occ.getModule()
    if ( print_domain_ref( mod ) ):
        print_obj_name( mod ) 
  
    add_indent()
    dump_occModuleInstHeaders( occ )
    dump_occDesignInsts( occ )
    dump_occNets( occ )
    dump_occTerms( occ )
    sub_indent()


def dump_nets (block):
    netColl = block.getNets( oa.oacNetIterAll )
    if not print_header_line(netColl.getCount(), "NET"):  
        return
    dump_net_members( netColl )


def dump_modNets (mod):
    netColl = mod.getNets( oa.oacNetIterAll )
    if not print_header_line(netColl.getCount(), "MODNET"):
        return
    dump_modNet_members( netColl )


def print_term_summary (term): 
    dump_bits( term )
    if ( term.getType().getName() == "BusTerm" ):
        print_sss( term )
    pstr = " %-6s" % (term.getTermType().getName() );  sys.stdout.write(pstr)
    con_net = term.getNet()
    if con_net.isValid():                                                    
        print_obj_name( con_net, " CONN2")
    if term.isBitTerm():
        pstr = " PINCON=%s" % ( term.getPinConnectMethod().getName() ); sys.stdout.write(pstr) 
#>>>???? on do_indent here 051811
    do_indent()


def print_modTerm_summary (term):
    dump_bits( term )
    if ( term.getType().getName() == "ModBusTerm" ):
        print_sss( term )
    pstr = " %-6s" % (term.getTermType().getName() );sys.stdout.write(pstr)
    con_net = term.getNet()
    if con_net.isValid():
        print_obj_name( con_net, " CONN2" )


def dump_modTerm_members (term_coll):
    for  term in term_coll:
        print_modTerm_summary( term )
        name = term.getType().getName()
        if ( name == "ModScalarTerm" ):
            x=0
        elif ( name == "ModBusTermBit" ):
            x=0
        elif ( name == "ModBusTerm" ):
            mod   = term.getModule()
            start = term.getStart()
            stop  = term.getStop()
            incr  = term.getStep()
             
            baseName = term.getDef().getName()
            add_indent()
            ix=start
            while ( stop > ix ):
               ix += incr                                                    
               term_bit = oa.oaModBusTermBit.find( mod, baseName, ix )
               print_modTerm_summary( term_bit ) 
              
            sub_indent()
        elif ( name == "ModBundleTerm" ) :
            add_indent()
            for mem_term in term.getMembers():
                print_modTerm_summary( mem_term )
            sub_indent()
        else:
            msg = "Unrecognized ModTerm type: %s" % ( term.getType().getName() )
            log( msg )



def dump_term_members (term_coll):
    for term in term_coll:
        print_term_summary( term )
        name = term.getType().getName()
        if ( name ==  "ScalarTerm" ):
            x=0
        elif ( name == "BusTermBit" ):
            x=0
        elif (name == "BusTerm" ):
            block = term.getBlock()
            start = term.getStart()
            stop  = term.getStop()
            incr  = term.getStep()
              
            baseName = term.getDef().getName()
            add_indent()

            ix=start
            while ( stop > ix ):
               ix += incr    
               term_bit = oa.oaBusTermBit.find( block, baseName, ix )
               print_term_summary( term_bit )
            
            sub_indent()
        elif ( name == "BundleTerm" ):
              add_indent()
              for mem_term in term.getMembers():
                  print_term_summary( mem_term )
              sub_indent()
        else:
            msg = "Unrecognized Term type: %s" % ( term.getType().getName() )
            log( msg )


def dump_terms (block):
    if not print_header_line( block.getTerms().getCount(), "TERM"):
        return 
    dump_term_members( block.getTerms() )



def dump_modTerms (mod):
    if not print_header_line( mod.getTerms().getCount(), "MODTERM" ):
        return 
    dump_modTerm_members( mod.getTerms() )



def print_instTerm (instTerm):
    new_obj_base_id( instTerm )
  
    pstr = " \" %s\"" % ( instTerm.getTermName() );sys.stdout.write(pstr)
    if instTerm.isImplicit():
        pstr = " IM";sys.stdout.write(pstr)
    else:
        pstr = " EX";sys.stdout.write(pstr)


    pstr = " BITS=%s" % ( instTerm.getNumBits() );sys.stdout.write(pstr)
  
    if instTerm.isBound():
        pstr = " BOUND";sys.stdout.write(pstr)
    else:
        pstr = " UNBOUND";sys.stdout.write(pstr)

    if  instTerm.usesTermPosition():
        pstr = " POS";sys.stdout.write(pstr) 
  
    con_net = instTerm.getNet()
    if con_net.isValid():
        print_obj_name( con_net, " CONN2" )



def dump_InstTerms (container):
    if not print_header_line(container.getInstTerms().getCount(), "INSTTERM"):
        return
    for instTerm in container.getInstTerms():
        print_instTerm( instTerm )


def dump_one_inst (inst):
    dump_InstTerms( inst )



def dump_props (obj): 
    if not print_header_line( obj.getProps().getCount(), "PROP" ):
        return
    add_indent()
    for prop in obj.getProps():
      pstr = "\n";sys.stdout.write(pstr)
      log(prop.getType().getName())
      pstr = " %s" % ( prop.getName() );sys.stdout.write(pstr)
    
    sub_indent()



def dump_insts (container):
    if ( not print_header_line(container.getInsts().getCount(), "INST") ):
        return
    for inst in container.getInsts():
        new_obj_base_id( inst )
        print_obj_name( inst )
        print_transform( inst )
        add_indent()
        dump_one_inst( inst )
        sub_indent()


def dump_modDesignInsts (container):
    #
    # If container Type is Module the Iter has both kinds of ModInsts; so to get an
    # accurate count of only the ModDesignInsts, do an initial cycle through the iterator.
    #
    count = 0
    for inst in container.getInsts():
        if inst.isModDesignInst():
            count += 1 
  
    if not print_header_line(count, "MODDESIGNINST"):
        return
    #
    # Go through the iterater again, this time printing out ModInst information
    #
    for inst in container.getInsts():
        # These asserts won't work. Fake it.
        #assertq("#{inst.getDatabase.getType.getName} == Design")
        #assertq("#{inst.getDatabase} == #{container.getDatabase}")
        instDB = inst.getDatabase()
        instName = instDB.getType().getName()

        assertq(eval(' instName == "Design" '),
                'inst.getDatabase().getType().getName() == "Design"')
        assertq(eval('inst.getDesign().getCellName() == container.getDesign().getCellName()'),
                'inst.getDesign().getCellName() == container.getDesign().getCellName()')
        #
        # If the container Type is Module the Iter has both kinds of ModInsts.
        # Skip the ModModuleInsts since they are dumped from the ModModuleInstHeader
        #
        if inst.isModModuleInst():
           continue
        new_obj_base_id( inst )
        if ( container.getType().getName() != "Module" ):
            print_obj_name( inst.getModule() )
            pstr = "|" ;sys.stdout.write(pstr)
        
        print_obj_name( inst )
        #
        # No XFORMS in Mod Domain.
        # Don't need to repeat the guts in the ModInstHeader.
        # But DO need to in each Module, since the InstTerms could be connected
        # to different Nets, have different annotations, etc.
        #
        if ( container.getType().getName() == "Module" ):
            add_indent()
            dump_one_inst( inst )
            sub_indent()
        else:
            bound_status( inst )
            if inst.isBound():
                mod = inst.getMasterModule()
                if mod.isValid():
                    print_obj_name( mod )
                    if ( mod.getDesign().getTopModule().getName() == mod.getName() ):
                        pstr = "[TOP]";sys.stdout.write(pstr) 
                    else:
                        pstr = "***NULL MASTER MODULE!";sys.stdout.write(pstr)




def dump_modModuleInsts (container):
    add_indent()
    if not print_header_line(container.getInsts().getCount(), "MODMODULEINST"):
        return 
    for inst in container.getInsts():
      new_obj_base_id( inst )
      print_obj_name( inst )
      # No XFORMS in Mod Domain!!
      add_indent()
      dump_one_inst( inst )
      sub_indent()
    
    sub_indent()



def dump_busNetDefs (container, descrip):
  print_header_line( container.getBusNetDefs().getCount(), descrip )

  for objdef in container.getBusNetDefs():
      new_obj_base_id( objdef )
      pstr = " BITS=%d" % ( objdef.getNumBits() );sys.stdout.write(pstr)
      pstr = " MINIX=%d" % ( objdef.getMinIndex() );sys.stdout.write(pstr)
      pstr = " MAXIX=%d" % ( objdef.getMaxIndex() );sys.stdout.write(pstr)
      pstr = "  BASENAME=%s" % ( objdef.getName() );sys.stdout.write(pstr)
      pstr = "  BITS:", 
      for bit in objdef.getBusNetBits():
          pstr = " %d" % ( bit.getBitIndex() );sys.stdout.write(pstr)


def dump_modInstHeaders (design):
    global dont_skip_if_zero
    ih_coll = design.getModInstHeaders()
    if not print_header_line( ih_coll.getCount(), "MODINSTHEADER", dont_skip_if_zero ):
        return 

    for ih in ih_coll: 
        # Pcell processing is taken out of this tcl version.
        #
        if ( ih.isSubHeader() | ih.isSuperHeader() ):
            continue
      
        new_obj_base_id( ih )
        print_only_lcv( ih )
        print_master_info( ih, "DESIGN" )
        add_indent()
        dump_modDesignInsts( ih )
        sub_indent()


def dump_modModuleInstHeaders (mod):
    ih_coll = mod.getModuleInstHeaders()
    if not print_header_line( ih_coll.getCount(), "MODMODULEINSTHEADER"):
        return
  
    for ih in ih_coll:
        #
        # ModModules can't have parameters, so no Super/SubHeaders
        #
        new_obj_base_id( ih )
        print_obj_name_scalar( ih )
        print_mod_master_info( ih )
        dump_modModuleInsts ( ih )



def dump_instHeaders (block):
    ih_coll = block.getInstHeaders()
    if  not print_header_line( ih_coll.getCount(), "INSTHEADER"):
        return 
  
    for ih in ih_coll:
        # Pcell processing is taken out of this tcl version.
        #
        if ( ih.isSubHeader() | ih.isSuperHeader() ):
           continue

        new_obj_base_id( ih )
        print_master_info( ih, "DESIGN" )
        add_indent()
        dump_insts( ih )
        sub_indent()



def dump_modules (design):
    global dont_skip_if_zero
    modTop = design.getTopModule()
  
    # First dump the ModInstHeaders, which are scoped to the Design,
    # common across all Modules that use them.
    #
    dump_modInstHeaders( design )
  
    # Next take each Module in turn, start with its unique ModModuleInstHeaders.
    #
    mod_coll = design.getModules()
    pstr = "\n" ;sys.stdout.write(pstr)
    print_header_line( mod_coll.getCount() , "MODULE", dont_skip_if_zero)
    for mod in mod_coll:
        new_obj_base_id( mod )
        print_obj_name( mod )
    # shc NOTE: Ask James about the following, which fails. Why aren't the handles the same?
        #if mod == modTop
        if ( mod.getName() == modTop.getName() ):
            pstr = " TOP";sys.stdout.write(pstr)
        elif ( mod.isEmbedded() ):
            pstr = " EMBEDDED";sys.stdout.write(pstr)
        
        if mod.isDerived():
            print_orig_lcv( mod )
        
        add_indent()
        dump_modModuleInstHeaders( mod )
        dump_modDesignInsts( mod )
        dump_busNetDefs( mod, "MODBUSNETDEF" )
        dump_busTermDefs( mod, "MODBUSTERMDEF" )
        dump_modNets ( mod )
        dump_modTerms( mod )
        sub_indent()
      



def separator (mesg):
    pstr = "\n"; sys.stdout.write(pstr)
    msg = "==================== %s ====================" % ( mesg )
    log ( msg )


def dump_block (block):
    if block.isValid():
        print_header_line( 1, "BLOCK" )
        new_obj_base_id( block )
        add_indent()
        dump_instHeaders( block )
        dump_busNetDefs( block, "BUSNETDEF" )
        dump_busTermDefs( block, "BUSTERMDEF" )
        dump_nets( block )
        dump_terms( block )
        sub_indent()
    else:
        pstr = "\n";sys.stdout.write(pstr)
        log( "NO BLOCK" )

  

def dump_design (design):
    global oi_header_array
    global dump_block_domain, dump_mod_domain, dump_occ_domain
    pstr = "\n";sys.stdout.write(pstr)
    log( "______________________________ " )
    print_obj_type( design )
    print_only_lcv( design )
    add_indent()
    if design.isSubMaster():
        pstr = "\n";sys.stdout.write(pstr)
        log( "SUBMASTER" ) 
    
    sub_indent()
    dump_props( design )
    if ( dump_block_domain ):
        separator ( "BLOCK DOMAIN" )
        dump_block( design.getTopBlock() )
   
    if ( dump_mod_domain ):
        separator( "MODULE DOMAIN" ) 
        dump_modules( design )
    
    if ( dump_occ_domain ):
        separator( "OCCURRENCE DOMAIN" )
        pstr = "\n";sys.stdout.write(pstr)
        do_indent()
        occ_top = design.getTopOccurrence()
        if occ_top.isValid():
            pstr = "[TOP]";sys.stdout.write(pstr)
            oi_header_array = []
            dump_occ( occ_top)
          #
          # Dump out any OccInstHeaders we saved while iterating over
          # OccDesignInsts in the Occurrence.
          #
            dump_occInstHeaders( )
        else:
            pstr = "NO OCCURRENCE";sys.stdout.write(pstr) 
    
    pstr = "\n";sys.stdout.write(pstr)



def dump_open_designs(dumpblockd,dumpmodd,dumpoccd): 
    global dump_block_domain, dump_mod_domain, dump_occ_domain  

    dump_block_domain = dumpblockd
    dump_mod_domain   = dumpmodd
    dump_occ_domain   = dumpoccd

    pstr = "\n";sys.stdout.write(pstr) 
    log( "Currently open Designs:" )
    for design in oa.oaDesign.getOpenDesigns():
        dump_design( design )
    pstr = "\n";sys.stdout.write(pstr)


