#!/usr/bin/env python2.5
################################################################################
#  Copyright {c} 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 {the "License" you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE    VERSION  BY         DESCRIPTION
#   04/27/11  10.0   bpfeil, Si2   Tutorial 10th Edition 
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction {CAI} projects from various
#   industries, including related pedagogical techniques, exercise and lab
#   formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library {UNIX man pages}
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation {www.swig.org}
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#   Rudy Albachten, Chair
#   James Masters
#   Christian Delbaere
#   Steve Potter
#   Stefan Zager
#   Carl Olson
#   Doug Keller
#   Koushik Kalyanaraman
#   Brandon Barclay
#
################################################################################

# This is a driver for the utility functions in dumpUtils.py
# It can be called for any lib/cell/view whose Lib is defined in the
# local lib.defs file.
#
# Mostly, the dumpUtils.py module is sourced by other labs to print out
# the contents of OA Objects.

import sys
import os
here = os.path.abspath(sys._getframe().f_code.co_filename)
sys.path.append(os.path.join(os.path.dirname(here), '../..'))

import oa.design

# inport the dumpUtils.py utility functions
from dumpUtils import * 


# -------------------------------- main ------------------------------
oa.oaDesignInit()

ns = oa.oaNativeNS()

def main():

    if (len(sys.argv) < 4):
        print " Use : \n\toadump.py  libraryName cellName viewName\n"
        return 1

#>>>    ns = oa.oaNativeNS()

    libstr  = sys.argv[1] 
    cellstr = sys.argv[2] 
    viewstr = sys.argv[3]

    lib_defs = 'lib.defs'

    if ( not os.path.exists( lib_defs) ):
        print "***Missing lib.defs file"
        return 1

    oa.oaLibDefList.openLibs( lib_defs  )

    design = oa.oaDesign.open( libstr, cellstr, viewstr, 'r')

    dump_open_designs(True, True, True)


    print "\n\n.............. Normal Termination ........... \n\n"


if (__name__ == "__main__"):
    sys.exit(main())


