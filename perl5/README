This is a description of the perl bindings for OpenAccess.

There is no comprehensive API documentation for the perl interface; please
refer to the C++ OpenAccess API documentation.

There are test programs in the test/ directory.  If invoked without
arguments, they will allow OA to use its default search algorithm to find
a lib.defs (library definition file).  Alternately, you may invoke them as:

% (base|dm|tech|design)_test.pl -libdefs <path to lib.defs>

Getting Started
--------------------------------------------------------------------------------

Before you can run your own scripts, you'll need to tell the perl interpreter
where to find the OA perl modules.  There are typically two ways to do it -- in
the following, substitute the path to the top of your OA scripting installation
for <OASCRIPT_ROOT> :

1) Add the path to the environment variable PERL5LIB:

    In bash:
    
    $ export PERL5LIB="$PERL5LIB:<OASCRIPT_ROOT>/perl5"
    
    In csh:
    
    % setenv PERL5LIB "$PERL5LIB:<OASCRIPT_ROOT>/perl5"

-OR-

2) Add a 'use lib' statement to your script:

    use lib "<OASCRIPT_ROOT>/perl5";
    use oa;

Modules and Namespaces
--------------------------------------------------------------------------------

To keep the code size manageable, the API is split into parts, based on the
subdirectories in the OA source area.  Currently, the perl modules are:

oa::base
oa::common
oa::design
oa::dm
oa::plugin
oa::tech
oa::util
oa::wafer

However, to avoid unnecessary nesting of perl namespaces, all the classes
defined in these modules are promoted into the 'oa' namespace.  For
example, the C++ oaLib class, which is defined in module oa::dm, becomes
the oa::oaLib class in perl (rather than oa::dm::oaLib).

Class Instance Methods and Class Static Methods
--------------------------------------------------------------------------------

TODO

When calling an instance method, the perl interface will try to verify that the
calling object is valid (using the oaIsValid() from the C++ API).

Enumerated Types and Enum Wrappers
--------------------------------------------------------------------------------

All enumerated values in OA (e.g., oaLibAccessEnum) are available as integer
constants in the oa namespace:

my $access = $oa::oacReadLibAccess;

Classes that are simple wrappers around an enumerated value (e.g. oaLibAccess)
are supported in the wrappers:

my $access = new oa::oaLibAccess($oa::oacReadLibAccess);

As a convenience, when a function takes an enum wrapper argument, you may
either pass an enum wrapper object, or you may pass the enumerated value
directly.  Either of these will work:

$lib->getAccess(new oa::oaLibAccess($oa::oacReadLibAccess));
$lib->getAccess($oa::oacReadLibAccess);

Functions that return an enum wrapper will, instead, return the enumerated
value:

if ($ref->getOrient() == $oa::oacR90) {
    ...
}

oaString, oaName, and oaNameSpace
--------------------------------------------------------------------------------

The oaString class is translated as a plain perl string, and a perl string
is used in place of an oaString object for function arguments.  The perl
API uses the same pass-by-reference model that the C++ API uses.  For
example (from dm_test.pl):

    my $libpath;
    $lib->getPath($libpath);
    print "libpath = $libpath\n";

Note also that the "name" classes in OA (oaScalarName, oaSimpleName, etc.)
are not handled specially; they are fully-fledged class objects:

    my $ns = new oa::oaNativeNS();
    my $libname = new oa::oaScalarName($ns, "stdcell");
    my $lib = oa::oaLib::find($libname);

oaPoint, oaBox, oaTransform
--------------------------------------------------------------------------------

As in the C++ API, coordinates are always integer values in DB units.

The oaPoint class is translated as a two-element array reference:

my $point;
$ref->getOrigin($point);
my $x = $point->[0];
my $y = $point->[1];

my @new_point = ( $new_x, $new_y );
$ref->setOrigin(\@new_point);

The oaBox class is translated to a four-element array reference:

my $bbox;
$design->getBBox($bbox);
my $x1 = $bbox->[0];
my $y1 = $bbox->[1];
my $x2 = $bbox->[2];
my $y2 = $bbox->[3];

The oaTransform class is translated to a 3-element array:

my @transform = ( $x, $y, $oa::oacR90 );
$ref->setTransform(\@transform);

Collections and Iterators
--------------------------------------------------------------------------------

oaCollection is supported:

    my $cell_collection = $lib->getCells();
    if (!$cell_collection->isEmpty()) {
        ...
    }

For the templated class oaIter<class T>, a sub-namespace of oaIter is used
for each possible template parameter.  For example (from dm_test.pl):

    my $cell_iter = new oa::oaIter::oaCell($lib->getCells());
    while my (my $cell = $cell_iter->getNext()) {
        ...
    }

When an oaIter is exhausted, getNext() will return undef.

Arrays
--------------------------------------------------------------------------------

All oaArray<> template instantiations, and all classes that are derived from
an an oaArray<> template instantiation, are translated as regular perl arrays.
For example (from base_test.pl):

    my $bi_arr;
    oa::oaBuildInfo::getPackages($bi_arr);
    for my $bi (@$bi_arr) {
        print("package " . $bi->getPackageName() . "\n");
    }

Exceptions
--------------------------------------------------------------------------------

Exceptions thrown by the C++ API are caught, and the perl 'croak' function
is called.  Scripters are advised to wrap their oa calls inside an 'eval' block,
like this:

eval {
    # oa calls
};

if ($@) {
    print STDERR, "OA Exception: $@\n";
}
