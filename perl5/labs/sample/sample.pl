#! /usr/bin/env perl
################################################################################
#
#  Copyright (c) 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 (the "License"] you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#   Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   10/18/10  1.0      Si2              Tutorial 10th Edition - oasperl version
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction (CAI) projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library (UNIX man pages)
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation (www.swig.org)

#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################

#  Each PathSeg represented by a - line representing a different Route
#  with the function creating them listed underneath.
#  _________________________________________________________________________
#  |Top                                                                      |
#  |    ___________________    ___________________    ___________________    |
#  |   |Gate               |  |Gate               |  |Gate               |   |
#  |   |             -     |  |             -     |  |             -     |   |
#  |   |             -     |  |             -     |  |             -     |   |
#  |   |  -          -     |  |  -          -     |  |  -          -     |   |
#  |   |  -     NetShapeRoute |  -     NetShapeRoute |  -     NetShapeRoute  |
#  |   |  --               |  |  --               |  |  --               |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  --    OrInst     |  |  --    OrInst     |  |  --    OrInst     |   |
#  |   |  ---   NandInst1  |  |  ---   NandInst1  |  |  ---   NandInst1  |   |
#  |   |  ---   NandInst2  |  |  ---   NandInst2  |  |  ---   NandInst2  |   |
#  |   |  -     InvInst    |  |  -     InvInst    |  |  -     InvInst    |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   |  -                |  |  -                |  |  -                |   |
#  |   | ConnectRouteForNet|  | ConnectRouteForNet|  | ConnectRouteForNet|   |
#  |   |___________________|  |___________________|  |___________________|   |
#  |                                                                         |
#  |        -                                                                |
#  |        -                                                                |
#  |        -                   -                                            |
#  |        -                RouteForNet                                     |
#  |        -                                                                |
#  |    EmptyNets                                                            |
#  |_________________________________________________________________________|
use strict;
use warnings;
use File::Basename;
use FindBin;
use Math::BigFloat;
#
## Path to bindings. Add to PERL5LIB env variable OR here
#use lib "path-to-bindings/amd-init/perl5";

use lib dirname($FindBin::RealBin);
use oa::design;

# ****************************** Gratuitous globals ******************************
our $ns = new oa::oaNativeNS();
our $mode_read  = "r";
our $mode_write = "w";



# ****************************** MAIN ******************************

eval {

    oa::oaDesignInit( $oa::oacAPIMajorRevNumber,
		      $oa::oacAPIMinorRevNumber,
		      $oa::oacDataModelRevNumber );
    











    if (scalar(@ARGV) < 2) {
        printf(" Use : \tsample.pl  LibName  LibDirectoryPath\n");
        exit 1;
    }

    #$ns = new oa::oaNativeNS();
     
    my ($libStr, $strPathLib) = @ARGV;

    my $scNameLib = &openLibrary( $libStr, $strPathLib );

    &createInvSchematic( $scNameLib );
    &createOrSchematic( $scNameLib );
    &createNandSchematic( $scNameLib );
    &createGateSchematic($scNameLib);
    &createHierarchy($scNameLib);
    printf("\n......Normal completion......\n");

};

if ($@) {
    print STDERR "Caught exception: $@\n";
}

exit(0);


#-------------------------------------------------------------------------------
# ****************************** Functions ******************************

sub iround ($) {
    return int($_[0]);
##    return int($_[0] + ($_[0] > 0 ? 0.5 : -0.5));
}
sub iround2 ($) {

##    return int($_[0]);
    return int($_[0] + ($_[0] > 0 ? 0.5 : -0.5));
}




sub assert_def($$){
   my $condition = shift;
   my  $line = shift;
    if (! $condition) {
        printf("***ASSERT error %s", $line);
    }
}

sub getLayerPurpose ($$$) {
    my $design = shift;
    my $layerName = shift;
    my $purpName = shift;

#    upvar 1 $num  layerNum
#    upvar 1 $purp layerPurp

    my $techFile = $design->getTech();
    my $layerNum;
    my $layerPurp;

    if  ( ! $techFile) {
        if ($layerName eq "pin"){
            $layerNum = 229;
        } elsif ($layerName eq "device") {
            $layerNum = 231;
	}	
        $layerPurp = 252;
        return (1,$layerNum,$layerPurp);
    }

    my $layer   =   oa::oaLayer::find($techFile, $layerName);
    my $purpose =   oa::oaPurpose::find($techFile, $purpName);

    if  ((! $layer) || (! $purpose)) {
        return (0,0,0);
    }

    $layerNum  = $layer->getNumber();
    $layerPurp = $purpose->getNumber();

    return (1, $layerNum,$layerPurp);
}


# *************************** BBox  ********************

sub bboxLeft ($){
    my $bbox = shift;
    return $bbox->[0];
}

sub bboxRight($) {
    my $bbox = shift;
    return $bbox->[2];
}

sub bboxTop ($){
    my $bbox = shift;
    return $bbox->[3];
}

sub  bboxBottom($) {
    my $bbox = shift;
    return $bbox->[1];
}
sub bboxGetWidth($) {
    my $bbox = shift;
    my $llX = $bbox->[0];
    my $urX = $bbox->[2];
    my $width = abs($urX - $llX);
    return  $width;
}

sub bboxGetHeight($) {
   my $bbox = shift;
   my $llY = $bbox->[1];
   my $upY = $bbox->[3];
   my $height= abs($upY - $llY);
   return $height ;
}
sub bboxGetCenter($) {
  my $bbox = shift;
  my $llX = $bbox->[0]; #left
  my $llY = $bbox->[1]; #bottom 
  my $urX = $bbox->[2]; #right
  my $upY = $bbox->[3]; #top

  my $centerX = iround(($llX+$urX) / 2) ;
  my $centerY = iround(($llY+$upY) / 2) ;

  return ($centerX,$centerY) ;
}

# *****************************************************************************
# Create a ScalarInst in a Block with the given cellname. The View is
# assumed to be 'schematic for this purpose. Open the Design if it is not
# already open in the current session.
# *****************************************************************************
sub createInstance ($$$$$) {
    my $block = shift;
    my $trans = shift;
    my $cellName = shift;
    my $instStr = shift;
    my $scNameLib = shift;
    
    my @transform = ($trans->[0],$trans->[1],$trans->[2]);

    my $gate = new oa::oaScalarName( $ns, $cellName );
    my $view = new oa::oaScalarName( $ns, "schematic" );
    my $master = oa::oaDesign::find( $scNameLib, $gate, $view );
    if (! $master ) {
        $master = oa::oaDesign::open( $scNameLib, $gate, $view, $mode_read );
    }
    if (! $master ) {
        return;
    }
    my $instName = new  oa::oaScalarName( $ns, $instStr );

    my $inst=  oa::oaScalarInst::create( $block, $master, $instName, \@transform );
    
    my $box;
    $inst->getBBox($box);

    return $inst;
}


# *****************************************************************************
# Create Route with the ObjectArray, attach to the specified net, set end Conns
# *****************************************************************************

sub connectRouteForNet($$$$$) {
    my $block = shift;
    my $net = shift;
    my $start = shift;
    my $end = shift;
    my @routeObjectArray = @{(shift)};

    my $route = oa::oaRoute::create( $block );
    $route->setObjects( \@routeObjectArray );
    $route->addToNet( $net );
    if ($start) {
        $route->setBeginConn( $start );
    }
    if ($end) {
        $route->setEndConn( $end );
    }
}


# *****************************************************************************
# Create oaInstTerm object with the given parameters, which may not be bound.
# *****************************************************************************

sub createInstTerm($$$) {
    my $net = shift;
    my $inst = shift;
    my $termName = shift;

    # It seems that we can't use any subclass of oaName for creating InstTerms.
    #set simpleTermName [oa::oaSimpleName $ns $termName]
    #return [oa::oaInstTerm_create $net $inst [oa::oaSimpleName $ns $termName]]

    #set scTermName [oa::oaScalarName $ns $termName]
    #return [oa::oaInstTerm_create $net $inst $scTermName]

    my $name = new oa::oaName( $ns, $termName );
    return oa::oaInstTerm::create( $net, $inst, $name );

}

# *****************************************************************************
# Create an InstTerm object for the given terminal with 'termName' in 'inst'
# *****************************************************************************

sub getInstTerm($$$) {
    my $inst = shift; 
    my $net = shift;
    my $termName = shift;

    my $instTerm = createInstTerm( $net, $inst, $termName );
    if (! $instTerm->getTerm()) {
        printf("Inst term for %s unbound. Unable to create InstTerm", $termName);
	return;
    }  else {
        return $instTerm;
    }
}

# *****************************************************************************
# Create a Pin with the given name, Points and direction, on 'term'
# *****************************************************************************

sub createPin ($$$$$){
    my $block = shift;
    my $term  = shift;
    my $name = shift;
    my $dir = shift;
    my @points = @{(shift)};  

    my $element;

    my $layer   = "";
    my $purpose =  "";
    my $success;
    ($success,$layer,$purpose) =getLayerPurpose( $block->getDesign(), "pin", "drawing" );

    my $fig = oa::oaPolygon::create( $block, $layer, $purpose, \@points );
    my $pin = oa::oaPin::create( $term, $name, $dir );
    $fig->addToPin($pin);
    return $pin;
}



# *****************************************************************************
# Create a Net with the given name in the given Block.
# *****************************************************************************

sub createNet($$) {
    my $block = shift;
    my $str = shift;

    my $name = new oa::oaScalarName( $ns, $str );
    return oa::oaScalarNet::create( $block, $name );
}


# ***************************************************************************
# createNetShapeRoute()
# ***************************************************************************

sub createNetShapeRoute($$$$) {
    my $design = shift;
    my $block  = shift;
    my $refx   = shift;
    my $refy   = shift;
    # Create path shape, route, terminal and net
    my $ net = createNet( $block, "ShapeRouteNet");

    my $termTypeInput = new oa::oaTermType( $oa::oacInputTermType );
    my $termTypeOutput = new oa::oaTermType( $oa::oacOutputTermType );

    createTerm( $net, "TermOnShapeRouteNet", $termTypeInput );

    my $beginExt = 1;
    my $endExt   = 1;
    my $width    = 2;
    my($success,$lNum,$pNum)  = getLayerPurpose( $design, "device", "drawing1" );
    
    my $endStyle = new oa::oaEndStyle("variable");
    my $segSty   = new oa::oaSegStyle( $width, $endStyle, $endStyle, $beginExt, $endExt );

    # Route  is created.
    my @point1 = ($refx, $refy);
    my @point2 = (($refx+100), $refy);
    my $seg1   = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty);

    my @routeObjectArray1 = ( $seg1 );

    my $route1 = oa::oaRoute::create( $block );
    $route1->setObjects( \@routeObjectArray1 );

    print "Unconnected route is created in Gate view\n";

    # Creating the shape: Ellipse
    my $centerx = ($refx+100);
    my $centery = $refy;

    my @ellRect = (($centerx-10), ($centery-10),($centerx+10), ($centery+10));

    my $fig  = oa::oaEllipse::create( $block, $lNum, $pNum, \@ellRect );
    $fig->addToNet( $net );

    print "An Ellipse is created in the Gate view\n";

    $route1->addToNet( $net );
    $route1->setEndConn( $fig );

    my $startx = $centerx;
    my $starty = $centery;

    @point1 = ($startx, $starty);
    @point2 = (($startx+50), $starty);
    my $seg2   = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray2 = ( $seg2 );

    my $route2 = oa::oaRoute::create( $block );
    $route2->setObjects( \@routeObjectArray2 );
    $route2->addToNet( $net );
    $route2->setBeginConn( $fig );

    my @p1 = (($startx+55), $starty);
    my @p2 = (($startx+65), $starty);
    my @pArray = ( \@p1, \@p2 );

    my $netPath = oa::oaPath::create( $block, $lNum, $pNum, 1, \@pArray );
    $netPath->addToNet( $net );

    print "Net with Shape Route is created in Gate view\n";

    @point1 = (($startx+70), ($starty+5));
    @point2 = (($startx+70), ($starty+20));

    my $seg3   = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray3 = ( $seg3 );

    my $route3 = oa::oaRoute::create( $block );
    $route3->setObjects( \@routeObjectArray3 );

    @p1 = (($startx+70), ($starty-5));
    @p2 = (($startx+70), ($starty-20));
    my @pArray2 = ( \@p1, \@p2 );

    oa::oaPath::create( $block, $lNum, $pNum, 1, \@pArray2 );

}



# *****************************************************************************
# Create a Term with the given name on the given Net.
# *****************************************************************************

sub createTerm($$$) {
    my $net = shift;
    my $str = shift;
    my $type = shift;

    my $name = new oa::oaScalarName( $ns, $str );
    return oa::oaScalarTerm::create( $net, $name, $type );
}

# *****************************************************************************
# Create a gate schematic which contains 4 gates, (Or, 2 Nand, and an Inv) and
# connect the gates with physical and logical elements. Routes are used for
# physical connections. The created Design is represented by 'Lib' 'Gate'
# 'schematic' (lib, cell and view names), respectively
# *****************************************************************************

sub createGateSchematic($) {
    my $scNameLib = shift;

    my $cell = new oa::oaScalarName( $ns, "Gate" );
    my $view = new oa::oaScalarName( $ns, "schematic" );

    printf( "Creating Gate schematic with 0r, 2 nand, Inv instances\n");

    my $vtSch = oa::oaViewType::get( new oa::oaReservedViewType("schematic") );
  
    my $design = oa::oaDesign::open( $scNameLib, $cell, $view, $vtSch, $mode_write );
    $design->setCellType( new oa::oaCellType("softMacro") );
    my $block = oa::oaBlock::create( $design );

    my @trans = ( 0, 0, $oa::oacR0);
    my $orInst = createInstance( $block, \@trans, "Or", "OrInst", $scNameLib );

    my $orBox;
    $orInst->getBBox( $orBox);

    my $x1 = $orBox->[0];
    my $y1 = $orBox->[1];
    my $x2 = $orBox->[2];
    my $y2 = $orBox->[3];
   
    my $height =  bboxGetHeight( $orBox ); 
    my $width =   bboxGetWidth( $orBox ); 

    my $xMid =    bboxLeft( $orBox );
    my $bottom  = bboxBottom( $orBox );

    my $yMid = $bottom +  iround($height / 2);

    my @trans2 = ( 0, ((0-2) * $height),$oa::oacR0);

    my $nand1 = createInstance( $block,  \@trans2, "Nand", "NandInst1", $scNameLib);

    my $nand1Box;
    $nand1->getBBox($nand1Box);

    my $nheight = bboxGetHeight( $nand1Box ) ;
    my $nwidth =  bboxGetWidth( $nand1Box );

    my $xMid2 = bboxLeft ($nand1Box);
    my $nbb  =  bboxBottom( $nand1Box );
   #>>> my $yMid2 = $nbb + ( $nheight / 2);
    my $yMid2 = ($nbb) + iround( $height / 2);

    my $net1 = createNet( $block,"N1" );

    my @bbox = ( 0, 0, 0, 0);
    my $stein1 = oa::oaSteiner::create( $block, \@bbox);

    my $beginExt = 0;
    my $endExt   = 0;
    my $widthRt  = 2;

    my $success;
    my $lNum;
    my $pNum;

    ($success,$lNum,$pNum) = getLayerPurpose( $design, "device", "drawing1" );

    my $endStyle = new oa::oaEndStyle("truncate");
    my $segSty   = new oa::oaSegStyle( $widthRt, $endStyle, $endStyle, $beginExt, $endExt );

    my @point1 = ( ($xMid-50), ($yMid+10) ); 
    my @point2 = ( ($xMid-15), ($yMid+10) );
    my $seg1 = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray1 =  ( $seg1 );

    my @p1 = ( ($xMid-70), ($yMid+15));
    my @p2 = ( ($xMid-55), ($yMid+15));
    my @p3 = ( ($xMid-50), ($yMid+10));
    my @p4 = ( ($xMid-55), ($yMid+5));
    my @p5 = ( ($xMid-70), ($yMid+5));

    my $element;
    my @ptArray = ( \@p1, \@p2, \@p3, \@p4, \@p5 );

    my $termTypeInput =  new oa::oaTermType( $oa::oacInputTermType );
    my $term1 = createTerm( $net1, "a", $termTypeInput );

    my $pin1 = createPin( $block, $term1,  "P1-In", $oa::oacLeft, \@ptArray); 

    &connectRouteForNet( $block, $net1, $pin1, $stein1, \@routeObjectArray1 );

    @point1 = ( ($xMid-15), ($yMid+10));
    @point2 = ( ($xMid),    ($yMid+10));
    my $seg2 = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty);

    my @routeObjectArray2 = ( $seg2 );

    my $instTerm = getInstTerm( $orInst, $net1, "T1" );

    &connectRouteForNet( $block, $net1, $stein1, $instTerm, \@routeObjectArray2 );

    @point1 = (($xMid-15), ($yMid+10));
    @point2 = (($xMid-15), ($yMid2+10));

    my $seg3 = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty);
    

    @point1 = (($xMid-15), ($yMid2+10));
    @point2 = (($xMid),    ($yMid2+10));
    my $seg4 = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray3 = ( $seg3, $seg4 );

    $instTerm = getInstTerm( $nand1, $net1, "T1" );
    &connectRouteForNet( $block, $net1, $stein1, $instTerm, \@routeObjectArray3 );

    my $net2 = createNet( $block, "N2" );
    @bbox = (0, 0, 0, 0);
    my $stein2 = oa::oaSteiner::create( $block, \@bbox );

    @point1 = (($xMid-50), ($yMid-10));
    @point2 = (($xMid-25), ($yMid-10));
    my $seg5 = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray4 = ( $seg5 );
    
    my $term2 = createTerm( $net2, "b", $termTypeInput );

    @p1 = (($xMid-70), ($yMid-5));
    @p2 = (($xMid-55), ($yMid-5));
    @p3 = (($xMid-50), ($yMid-10));
    @p4 = (($xMid-55), ($yMid-15));
    @p5 = (($xMid-70), ($yMid-15));
    my @ptArray2 = ( \@p1, \@p2, \@p3, \@p4, \@p5 );


    my $pin2 = createPin( $block, $term2,  "P2-In", $oa::oacLeft, \@ptArray2 );

    &connectRouteForNet( $block, $net2, $pin2, $stein2, \@routeObjectArray4 );

    @point1 = (($xMid-25), ($yMid-10));
    @point2 = (($xMid),    ($yMid-10));
    my $seg6 = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty);

    my @routeObjectArray5 = ( $seg6 );

    $instTerm = getInstTerm( $orInst, $net2, "T2" );

    &connectRouteForNet( $block, $net2, $stein2, $instTerm, \@routeObjectArray5 );

    @point1 = (($xMid-25), ($yMid-10));
    @point2 = (($xMid-25), ($yMid2-10));
    my $seg7   = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    @point1 = (($xMid-25), ($yMid2-10));
    @point2 = (($xMid), ($yMid2-10));
    my $seg8   = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray6 = ( $seg7, $seg8 );

    $instTerm = getInstTerm( $nand1, $net2, "T2" );

    &connectRouteForNet( $block, $net2, $stein2, $instTerm, \@routeObjectArray6 );

    my $xPt  = ($xMid+$width);
    my $yPt  = $yMid;
    my $yPt2 = $yMid2;
    my $yPt3 = iround(($yPt+$yPt2)/2);

    my $yNand2 =  iround(($yPt+$yPt2)/ 2 - ($height/2));  
    # 0, 0 of nand gate is 65 units inside in x from lower left corner of its bBox.
    my $xNand2 = $xPt+40+65;

    @trans = ( $xNand2, $yNand2, $oa::oacR0);
    my $nand2 = createInstance( $block, \@trans, "Nand", "NandInst2", $scNameLib);

    my $net3 = createNet( $block, "N3" );

    my $instTerm1 = getInstTerm( $orInst, $net3, "T3" );
    my $instTerm2 = getInstTerm( $nand2,  $net3, "T1" );

    @point1 = ($xPt, $yPt);
    @point2 = (($xPt+20), $yPt);
    my $seg9   = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    
    @point1 = (($xPt+20), $yPt);
    @point2 = (($xPt+20), ($yPt3+10));
    my $seg10  = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    @point1 = (($xPt+20), ($yPt3+10));
    @point2 = (($xPt+40), ($yPt3+10));
    my $seg11  = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray7 = ( $seg9, $seg10, $seg11 );

    &connectRouteForNet( $block, $net3, $instTerm1, $instTerm2, \@routeObjectArray7 );

    my $net4 = createNet( $block, "N4" );

    $instTerm1 = getInstTerm( $nand1, $net4, "T3" );
    $instTerm2 = getInstTerm( $nand2, $net4, "T2" );

    @point1 = ($xPt, $yPt2);
    @point2 = (($xPt+20), $yPt2);
    my $seg12  = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    @point1 = (($xPt+20), $yPt2);
    @point2 = (($xPt+20), ($yPt3-10));
    my $seg13  = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    @point1 = (($xPt+20), ($yPt3-10));
    @point2 = (($xPt+40), ($yPt3-10));
    my $seg14  = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    my $testbox1;
    $seg14->getBBox($testbox1);

    my @routeObjectArray8 = ( $seg12, $seg13, $seg14 );

    &connectRouteForNet( $block, $net4, $instTerm1, $instTerm2, \@routeObjectArray8 );

    my $xfto9 = ($xPt+40+$width);
    my $yfto9 = iround(($yPt+$yPt2)/2);
    my $net5  = createNet( $block, "N5" );

    @point1 = ($xfto9, $yfto9);
    @point2 = (($xfto9+30), $yfto9);

    my $seg15  = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    my $testbox;
    $seg15->getBBox($testbox);


    my @routeObjectArray9 = ( $seg15 );

    my $yInv = ($yfto9 - 25);
    # {0, 0} of Inv gate is 35 units inside in x from lower left corner of its bBox.
    my $xInv = ($xfto9 + 30 + 35);

    @trans = ( $xInv, $yInv, $oa::oacR0);
    my $inv   = createInstance( $block, \@trans, "Inv", "InvInst", $scNameLib );
    # createInstTerm(net5, inv, "T1"]

    $instTerm1 = getInstTerm( $nand2, $net5, "T3" );
    $instTerm2 = getInstTerm( $inv, $net5, "T1" );

    &connectRouteForNet( $block, $net5, $instTerm1, $instTerm2, \@routeObjectArray9 );

    @point1 = (($xfto9+15), $yfto9);
    @point2 = (($xfto9+15), ($yfto9-90));

    my $seg16  = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray10 = ( $seg16 );

    my $termTypeOutput = new oa::oaTermType( $oa::oacOutputTermType);
    my $danglingTerm = createTerm( $net5, "Term-open", $termTypeOutput );

    &connectRouteForNet( $block, $net5, "" , "", \@routeObjectArray10 );
    
    my $xpt = ($xfto9+15);
    my $ypt = ($yfto9-90);

    @p1 = (($xpt+5), ($ypt-20));
    @p2 = (($xpt+5),  ($ypt-5));
    @p3 = ( $xpt,     $ypt);
    @p4 = (($xpt-5), ($ypt-5));
    @p5 = (($xpt-5), ($ypt-20));
    my @ptArray3 =  ( \@p1, \@p2, \@p3, \@p4, \@p5 );

    createPin( $block, $danglingTerm, "P-Out", $oa::oacBottom,  \@ptArray3);

    my $xfto10 = ($xfto9+30+160);
    my $yfto10 = $yfto9;

    my $net6 = createNet( $block, "N6" );

    my $xPinPt = ($xfto10+30);

    my @p6  = (($xPinPt+20), ($yfto10+5));
    my @p7  = (($xPinPt+ 5), ($yfto10+5));
    my @p8  = (($xPinPt),    ($yfto10));
    my @p9  = (($xPinPt+ 5), ($yfto10-5));
    my @p10 = (($xPinPt+20), ($yfto10-5));
    my @ptArray4 = ( \@p6, \@p7, \@p8, \@p9, \@p10 );


    my $term3 = createTerm( $net6, "SUM", $termTypeOutput );
    my $pin4  = createPin( $block, $term3,  "P3-Out", $oa::oacRight, \@ptArray4 );

    $instTerm = getInstTerm( $inv, $net6, "T2" );

    @point1 = ($xfto10, $yfto10);
    @point2 = (($xfto10+30), $yfto10);
    my $seg17  = oa::oaPathSeg::create( $block, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray11 = ( $seg17 );

    &connectRouteForNet( $block, $net6,  $instTerm, $pin4, \@routeObjectArray11 );

    &createNetShapeRoute( $design, $block, $xMid, $yfto9 );

    $design->save();
    $design->close();

}

# *****************************************************************************
# Creates the NAND schematic Design
# *****************************************************************************

sub createNandSchematic($) {
    my $scNameLib = shift;

    my $cell = new oa::oaScalarName( $ns, "Nand" );
    my $viewStr = new oa::oaScalarName( $ns, "schematic" );

    my $vtSch = oa::oaViewType::get( new oa::oaReservedViewType( "schematic" ) );

    my $view = oa::oaDesign::open( $scNameLib, $cell, $viewStr, $vtSch, $mode_write);
    my $block = oa::oaBlock::create( $view );

    assert_def($view->isValid(), __LINE__ );

    my $layer = "pin";
    my $purpose = "drawing";

    my $pinLayer = 0;
    my $pinPurp  = 0;
    my $success;

    ($success, $pinLayer, $pinPurp) = getLayerPurpose( $view, $layer, $purpose );
    assert_def($success, __LINE__ );

    my $drLayer = 0;
    my $drPurp  = 0;

    ($success, $drLayer, $drPurp) = getLayerPurpose( $view, "device", "drawing" );
    assert_def($success, __LINE__ ); 

    my @p1 = (-50, 50);
    my @p2 = (  0, 50);
    my @ptArray1 = ( \@p1, \@p2 );

    my $line =  oa::oaLine::create( $block, $drLayer, $drPurp, \@ptArray1 );
    assert_def($line->isValid(), __LINE__ );

    my $netName1 = new  oa::oaScalarName( $ns, "TopTail" );
    my $net =  oa::oaScalarNet::create( $block, $netName1 );
    assert_def($net->isValid(), __LINE__ );
    $line->addToNet( $net );

    my $termName1 =  new oa::oaScalarName( $ns, "T1" );
    my $termTypeInput = new oa::oaTermType( $oa::oacInputTermType );
    my $term = oa::oaScalarTerm::create( $net, $termName1, $termTypeInput );
    assert_def($term->isValid(), __LINE__ );

    my @box = (-65, 46, -50, 54);
    my $rect = oa::oaRect::create( $block, $pinLayer, $pinPurp, \@box );
    assert_def($rect->isValid(), __LINE__ );

    my $pin = oa::oaPin::create( $term, "IN1", $oa::oacLeft );
    $rect->addToPin( $pin );
    assert_def($pin->isValid(), __LINE__ );
    my @p3 = (-50, 20);
    my @p4 = (  0, 20);
    my @ptArray2 = ( \@p3, \@p4 );

    $line = oa::oaLine::create( $block, $drLayer, $drPurp, \@ptArray2 );
    assert_def($line->isValid(), __LINE__ );

    my $netName2 = new oa::oaScalarName( $ns, "BotTail" );
    $net = oa::oaScalarNet::create( $block, $netName2 );
    assert_def($net->isValid(), __LINE__ );
    $line->addToNet( $net );

    my $termName2 = new oa::oaScalarName( $ns, "T2" );
    $term = oa::oaScalarTerm::create( $net, $termName2, $termTypeInput );
    assert_def($term->isValid(), __LINE__ );

    @box = (-65, 16, -50, 24);
    $rect = oa::oaRect::create( $block, $pinLayer, $pinPurp, \@box );
    assert_def($rect->isValid(), __LINE__ );

    $pin = oa::oaPin::create( $term, "IN2", $oa::oacLeft );
    $rect->addToPin( $pin );
    assert_def($pin->isValid(), __LINE__ );

    my @p5  = (  0,  0);
    my @p6  = ( 80, 20);  
    my @p7  = (100, 20);
    my @p8  = (100, 50);
    my @p9  = ( 80, 70);
    my @p10 = (  0, 70);
    my @ptArray3 = ( \@p5, \@p6, \@p7, \@p8, \@p9, \@p10 );

    my $gate = oa::oaPolygon::create( $block, $drLayer, $drPurp, \@ptArray3 );
    assert_def($gate->isValid(), __LINE__ );

    my @p11  = (100, 40);
    my @p12  = (100, 30);  
    my @p13  = (110, 26);
    my @p14  = (118, 35);
    my @p15  = (110, 44);
    my @ptArray4 = ( \@p11, \@p12, \@p13, \@p14, \@p15 );

    my $bubble = oa::oaPolygon::create( $block, $drLayer, $drPurp, \@ptArray4 );
    assert_def($bubble->isValid(), __LINE__ );

    my @p16  = (118, 35);
    my @p17  = (140, 35);  
    my @ptArray5 = ( \@p16, \@p17 );

    my $line2 = oa::oaLine::create( $block, $drLayer, $drPurp, \@ptArray5 );
    assert_def($line2->isValid(), __LINE__ );

    my $netName3 = new oa::oaScalarName( $ns, "OutTail" );
    my $net2 = oa::oaScalarNet::create( $block,  $netName3 );
    assert_def($net2->isValid(), __LINE__ );

    $line2->addToNet( $net2 );
    my $termName3 = new oa::oaScalarName( $ns, "T3" );
    my $termTypeOutput = new oa::oaTermType( $oa::oacOutputTermType );
    my $term2 = oa::oaScalarTerm::create( $net2, $termName3, $termTypeOutput );

    @box = (140, 31, 155, 39);
    my $rect2 = oa::oaRect::create( $block, $pinLayer, $pinPurp, \@box );
    assert_def($rect2->isValid(), __LINE__ );

    my $pin2 = oa::oaPin::create( $term2, "OUT", $oa::oacRight );
    $rect2->addToPin( $pin2 );
    assert_def($pin2->isValid(), __LINE__ );

    $view->save();
    $view->close();

}

# *****************************************************************************
# Create the INV schematic Design
# *****************************************************************************


sub createInvSchematic($) {
    my $scNameLib = shift;

    my $cell = new oa::oaScalarName( $ns, "Inv");
    my $viewStr = new oa::oaScalarName( $ns, "schematic");

    my $type = oa::oaViewType::get(new oa::oaReservedViewType("schematic"));

    my $view  = oa::oaDesign::open( $scNameLib, $cell, $viewStr, $type, $mode_write);
    my $block = oa::oaBlock::create( $view );

    &assert_def($view->isValid(), __LINE__ );

    my $layer   = "pin";
    my $purpose = "drawing";

    my $pinLayer = 0;
    my $pinPurp  = 0;
    my $success  = 0;

    ($success, $pinLayer, $pinPurp) = getLayerPurpose($view, $layer, $purpose);
    assert_def( $success, __LINE__ );

    my @p1 = (-20, 25);
    my @p2 = (  0, 25);
    my @ptArray1 = (\@p1, \@p2);
    
    my $path = oa::oaPath::create( $block, $pinLayer, $pinPurp, 2, \@ptArray1);
    &assert_def($path->isValid(), __LINE__ );

    my $name = new oa::oaScalarName($ns, "TopTail");
    my $net  = oa::oaScalarNet::create($block, $name);
    assert_def($net->isValid(), __LINE__ );
    $path->addToNet( $net );

    my $termName = new oa::oaScalarName($ns, "T1");
    my $termTypeInput = new oa::oaTermType( $oa::oacInputTermType );
    my $term = oa::oaScalarTerm::create($net, $termName, $termTypeInput);
    assert_def($term->isValid(), __LINE__ );

    my @box  = (-35, 21,-20, 29);
    my $rect = oa::oaRect::create($block, $pinLayer, $pinPurp, \@box);
    assert_def($rect->isValid(), __LINE__ );

    my $pin = oa::oaPin::create($term, "IN1", $oa::oacLeft);
    $rect->addToPin( $pin );
    assert_def($pin->isValid(), __LINE__ );

    my $drLayer = 0;
    my $drPurp  = 0;

    ($success, $drLayer, $drPurp) = getLayerPurpose($view, "device", "drawing2");
    assert_def($success, __LINE__ );

    my @p3 = ( 0,  0); 
    my @p4 = ( 0, 50); 
    my @p5 = (70, 25);
    my @ptArray3 = ( \@p3, \@p4, \@p5 );

    my $gate =  oa::oaPolygon::create( $block, $drLayer, $drPurp, \@ptArray3);
    assert_def($gate->isValid(), __LINE__ );


    my @p6  = (70, 30);
    my @p7  = (70, 20);
    my @p8  = (80, 16);
    my @p9  = (88, 25);
    my @p10 = (80, 34);
    my @ptArray4 = ( \@p6, \@p7, \@p8, \@p9, \@p10 );

    my $bubble = oa::oaPolygon::create( $block, $drLayer, $drPurp, \@ptArray4 );
    assert_def($bubble->isValid(), __LINE__ );

    my @p11 = ( 88, 25);
    my @p12 = (110, 25);
    my @ptArray5 = ( \@p11, \@p12 );

    my $path2 = oa::oaPath::create( $block, $pinLayer, $pinPurp, 2, \@ptArray5 );
    assert_def($path2->isValid(), __LINE__ );

    $name = new oa::oaScalarName( $ns, "OutTail");
    my $net2 = oa::oaScalarNet::create( $block, $name );
    assert_def($net2->isValid(), __LINE__ );

    $path2->addToNet( $net2 );
    $termName = new oa::oaScalarName( $ns, "T2");
    my $termTypeOutput = new oa::oaTermType( $oa::oacOutputTermType );
    my $term2 = oa::oaScalarTerm::create( $net2, $termName, $termTypeOutput);

    @box = (110, 21, 125, 29);
    my $rect2 = oa::oaRect::create( $block, $pinLayer, $pinPurp, \@box);
    assert_def($rect2->isValid(), __LINE__ );

    my $pin2 = oa::oaPin::create($term2, "OUT", $oa::oacRight);
    $rect2->addToPin( $pin2 );
    assert_def($pin2->isValid(), __LINE__ );

    $view->save();
    $view->close();
}

# ***************************************************************************
# Create a few Nets in the top Block with some Routes associated.
# ***************************************************************************

sub createEmptyNets($$$) {
    my $top = shift;
    my $topBlock = shift;
    my $box =shift;

    my $refx = bboxRight($box);
    my $refy = bboxTop($box);

    # A Net with one terminal but without connection to top
    my $net1 = createNet( $topBlock, "EmptyNet1" );
    createTerm( $net1, "TermEmptyNet", new oa::oaTermType($oa::oacInputTermType) );

    print "An empty net with one terminal is created\n";

    # Creating Net that has route->shape->route connection

    my @rec_box = (($refx+50), $refy, ($refx+70), ($refy+ 20));
    my $lNum = "";
    my $pNum = "";

    my $beginExt = 0;
    my $endExt   = 0;
    my $width    = 2;
    my $success;
    ($success , $lNum, $pNum)= getLayerPurpose( $top, "device", "drawing1" );

    my $endStyle = new  oa::oaEndStyle("truncate");
    my $segSty   = new oa::oaSegStyle( $width, $endStyle, $endStyle, $beginExt, $endExt);

    my $rect = oa::oaRect::create( $topBlock, $lNum, $pNum, \@rec_box);

    my @point1 = ($refx, ($refy+10));
    my @point2 = (($refx+50), ($refy+10));
    my $seg1   = oa::oaPathSeg::create( $topBlock, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray1 = ( $seg1 );
    my $route1 = oa::oaRoute::create( $topBlock );
    $route1->setObjects( \@routeObjectArray1 );

    @point1 = (($refx+60), ($refy+10));
    @point2 = (($refx+90), ($refy+10));
    my $seg2   = oa::oaPathSeg::create( $topBlock, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray2 = ( $seg2 );
    my $route2 = oa::oaRoute::create( $topBlock );
    $route2->setObjects( \@routeObjectArray2 );

    $route1->setEndConn( $rect );
    $route2->setBeginConn( $rect );

    my $routeNet1 = createNet( $topBlock, "RouteNet1" );
    $rect->addToNet( $routeNet1 );
    $route1->addToNet( $routeNet1 );
    $route2->addToNet( $routeNet1 );

    print "A net with 2 routes and a shape is created\n";

    # A net with route->route

    @point1 = (($refx+100), $refy);
    @point2 = (($refx+120), $refy);
    my $seg3   = oa::oaPathSeg::create( $topBlock, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray3 = ( $seg3 );
    my $route3 = oa::oaRoute::create( $topBlock );
    $route3->setObjects( \@routeObjectArray3 );

    @point1 = (($refx+120), ($refy-20));
    @point2 = (($refx+120), ($refy+20));
    my $seg4   = oa::oaPathSeg::create( $topBlock, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray4 = ( $seg4 );
    my $route4 = oa::oaRoute::create( $topBlock );
    $route4->setObjects( \@routeObjectArray4 );

    @point1 = (($refx+120), ($refy-10));
    @point2 = (($refx+130), ($refy-10));
    my $seg5   = oa::oaPathSeg::create( $topBlock, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArray5 = ( $seg5 );
    my $route5 = oa::oaRoute::create( $topBlock );
    $route5->setObjects( \@routeObjectArray5 );

    my $routeNet2 = createNet( $topBlock, "RouteNet2" );
    $route3->addToNet( $routeNet2 );
    $route4->addToNet( $routeNet2 );
    $route5->addToNet( $routeNet2 );

    print "A net with 2 routes connecting to each other is created\n";
}

# *****************************************************************************
# Creates a hierarchy in the top level Design with cellname "Sample".
# It contains three Inst of the Design with the cellname "Gate" and
# connects the input and output Terms of the gates. Logical and physical
# connections are also created. The Design has 4 input and an 1 output Terms.
# *****************************************************************************

sub createHierarchy($) {
    my $scNameLib = shift;

    my $cell  = new oa::oaScalarName( $ns, "Sample" );
    my $cell2 = new oa::oaScalarName( $ns, "Gate" );
    my $view  = new oa::oaScalarName( $ns, "schematic" );
    
    my $type =  oa::oaViewType::get( new  oa::oaReservedViewType("schematic") );

    print "\nCreating Sample schematic with 3 Gate instances\n";
    my $top = oa::oaDesign::open( $scNameLib, $cell, $view, $type, $mode_write );
    my $topBlock = oa::oaBlock::create( $top );

    $top->setCellType( new oa::oaCellType("softMacro") );
    my $master = oa::oaDesign::open( $scNameLib, $cell2, $view, $type, $mode_read );
    my $masterBlock = $master->getTopBlock();

    my $bbox;
    $masterBlock->getBBox( $bbox );

    my $height =  bboxGetHeight( $bbox ); 
    my $width =   bboxGetWidth( $bbox ); 


    my @trans = ( (-$height-5), 0, $oa::oacR90 );
    my $inst1 = createInstance( $topBlock, \@trans, "Gate", "Inst1", $scNameLib );

    @trans = (($height+5), 0, $oa::oacR90 );
    my $inst2 = createInstance( $topBlock, \@trans, "Gate", "Inst2", $scNameLib );

    @trans = (0, ($width+20), $oa::oacR0 );
    my $inst3 = createInstance( $topBlock, \@trans, "Gate", "Inst3", $scNameLib );

    my $net1  = createNet( $topBlock, "N1" );
    my $term1 = createTerm( $net1, "T1", new oa::oaTermType($oa::oacInputTermType));

    my $box;
    $inst1->getBBox($box);

    my $xEnd = bboxLeft( $box ); 
    my $yEnd = bboxBottom( $box ); 

    my @point1 = (($xEnd+20), $yEnd);
    my @point2 = (($xEnd+20), ($yEnd-50));

    &createFigForNet( $top, $topBlock, $term1, $net1, \@point1, \@point2, "P-1", 1 );

    my $pin = oa::oaPin::find( $term1, "P-1" );
    my $instTerm = getInstTerm( $inst1, $net1, "a" );

    my $lNum = "";
    my $pNum = "";
    my $beginExt = 0;
    my $endExt   = 0;
    my $widthRt  = 2;
    my $success;
    ($success,$lNum, $pNum) = getLayerPurpose( $top, "device", "drawing1" );

    my $endStyle = new oa::oaEndStyle("truncate");
    my $segSty = new oa::oaSegStyle( $widthRt, $endStyle, $endStyle, $beginExt, $endExt );

    @point1 = (($xEnd+20), $yEnd);
    @point2 = (($xEnd+20), ($yEnd-50));
    my $seg1 = oa::oaPathSeg::create( $topBlock, $lNum, $pNum, \@point1, \@point2, $segSty );

    my @routeObjectArrayElements = ( $seg1 );

    &connectRouteForNet( $topBlock, $net1,  $instTerm, $pin, \@routeObjectArrayElements);

    my $net2  = createNet( $topBlock, "N2" );
    my $term2 = createTerm( $net2, "T2", new oa::oaTermType($oa::oacInputTermType));
    createInstTerm( $net2, $inst1, "b" );

    @point1 = (($xEnd+40), $yEnd);
    @point2 = (($xEnd+40), ($yEnd-50));
    createFigForNet( $top, $topBlock, $term2, $net2, \@point1, \@point2, "P-2", 1 );

    my $net3 = createNet( $topBlock, "N3" );
    createInstTerm( $net3, $inst1, "SUM" );

    
    my @center=bboxGetCenter($box);
    
    $inst3->getBBox($box);

    my $xEnd3 = bboxLeft($box);
    my $yEnd3 = bboxBottom($box);

    my @center3 = bboxGetCenter($box);

    my $centerX = $center[0];
    my $centerY = $center[1];
    @point1 = (($centerX+0), ($centerY + iround2($width/2)) );
    @point2 = ($xEnd3,  ($yEnd3+$height-20));
    my $myUndef = undef;
    my $none = "None";

    createFigForNet( $top, $topBlock, $none , $net3, \@point1, \@point2, "", 0 );

    my $net4  = createNet( $topBlock, "N4" );
    my $term3 = createTerm( $net4, "T3", new oa::oaTermType($oa::oacInputTermType));
    createInstTerm( $net4, $inst2, "a" );


    $inst2->getBBox($box);
    my $xEnd2 =  bboxLeft($box);
    my $yEnd2 =  bboxBottom($box);

    my @center2;
    @center2 = bboxGetCenter($box);

    @point1 = (($xEnd2+20), $yEnd2);
    @point2 = (($xEnd2+20), ($yEnd2-50));
    createFigForNet( $top, $topBlock, $term3, $net4, \@point1, \@point2, "P-3", 1 );

    my $net5  = createNet( $topBlock, "N5" );
    my $term4 = createTerm( $net5, "T4", new oa::oaTermType($oa::oacInputTermType) );
    createInstTerm( $net5, $inst2, "b" );

    @point1 = (($xEnd2+40), $yEnd2);
    @point2 = (($xEnd2+40), ($yEnd2-50));
    createFigForNet( $top, $topBlock, $term4, $net5, \@point1, \@point2, "P-4", 1 );

    my $net6 =  createNet( $topBlock, "N6" );
    createInstTerm( $net6, $inst2, "SUM" );

    my $end2X = $center2[0];
    my $end2Y = $center2[1];

    @point1 = (($end2X+0), ($end2Y + iround($width/2) ) );
    @point2 = ($xEnd3, ($yEnd3+$height-40));

    createFigForNet( $top, $topBlock, "None", $net6, \@point1, \@point2, "", 0 );

    createInstTerm( $net3, $inst3, "a" );
    createInstTerm( $net6, $inst3, "b" );

    my $net7  = createNet( $topBlock, "N7" );
    my $term5 = createTerm( $net7, "T5", new oa::oaTermType($oa::oacOutputTermType) );
    createInstTerm( $net7, $inst3, "SUM" );

    my $end3X = $center3[0];
    my $end3Y = $center3[1];

    @point1 = (($end3X+ iround($width/2) ), ($end3Y+0));
    @point2 = (($end3X+ iround($width/2+50)), ($end3Y+0));

    createFigForNet( $top, $topBlock, $term5, $net7, \@point1, \@point2, "P-5", 1 );
    createEmptyNets( $top, $topBlock, $box );

    $top->save();
    $top->close();

}


# *****************************************************************************
# Create an oaPath element between 'start' and 'end'. Also create a
# Rect Fig and oaPin object with the Rect if 'addpin' is true
# *****************************************************************************

sub createFigForNet ($$$$$$$$){
    my $design = shift;
    my $block = shift;
    my $term  = shift;
    my $net   = shift;
    my $startpt = shift;
    my $endpt = shift;
    my $pinName = shift;
    my $addpin  = shift;

    my ($success, $lNum, $pNum) = getLayerPurpose( $design, "device", "drawing1" );

    my @startptArray = ($startpt->[0],$startpt->[1]);
    my @endptArray   = ($endpt->[0], $endpt->[1]);
    my @ptarray = ( \@startptArray, \@endptArray );

    my $startX = $startpt->[0];
    my $startY = $startpt->[1];

    my $endX = $endpt->[0];
    my $endY = $endpt->[1];

    my @pt1 = ($startX, $endY);
    my @pt2 = ($endX, $startY);

    if (($endX != $startX) && ($endX > $startX)) {
        if ($endY ne $startY) {
            @ptarray  = ( \@startptArray, \@pt1, \@endptArray );
	}
    } elsif (($endX != $startX) && ($endX < $startX)) {
        if ($endY != $startY) {
	    @ptarray = ( \@startptArray, \@pt2,  \@endptArray);
	}
    }

#<<<
#<<<    my $element;
#<<<    printf("Num elements ptarray: %d\n", $#ptarray );
#<<<    foreach $element (@ptarray) {printf("%d,%d \n", $element->[0],$element->[1]);}
#<<<
#<<<    
    
    my $pathStyle = new oa::oaPathStyle($oa::oacTruncatePathStyle);
    my $beginExt = 0;
    my $endExt = 0;
    my $width  = 2;
    my $path = oa::oaPath::create( $block, $lNum, $pNum, $width, \@ptarray, $pathStyle, $beginExt, $endExt);
    $path->addToNet( $net );

    my $ttypeStr;
    my $ttype ;
   
    if ( $term ne "None" ) {
        $ttype = new oa::oaTermType($term->getTermType());
        $ttypeStr = $ttype->getName();
        
	if ( $addpin ) {
	    my $dir = "";
	    @pt1 = @endptArray;
	    @pt2 = @endptArray;

	    if ($ttypeStr eq "input") {
		@pt1 = (($endX-2), ($endY-2));
		$dir = $oa::oacLeft;
	    } elsif ( $ttypeStr eq "output") {
		@pt2 = (($endX+2), ($endY+2));
		$dir = $oa::oacRight;
	    }

	    my @box2  = ( $pt1[0], $pt1[1], $pt2[0], $pt2[1]);
	    my $rect = oa::oaRect::create( $block, $lNum, $pNum, \@box2);
	    my $pin  = oa::oaPin::create( $term, $pinName, $dir );
	    $rect->addToPin( $pin );

	}
    }

    return $path;

}

# *****************************************************************************
# Creates the OR schematic Design.
# *****************************************************************************

sub createOrSchematic($) {
    my $scNameLib = shift;

    my $cell = new oa::oaScalarName( $ns, "Or" );
    my $viewStr = new oa::oaScalarName( $ns, "schematic" );

    my $vtSch  = oa::oaViewType::get( new oa::oaReservedViewType( "schematic") );

    my $view  = oa::oaDesign::open( $scNameLib, $cell, $viewStr, $vtSch, $mode_write );
    my $block = oa::oaBlock::create( $view );

    my $layer = "pin";
    my $purpose = "drawing";

    my $pinLayer = 0;
    my $pinPurp  = 0;
    my $success;

    ($success, $pinLayer, $pinPurp) = getLayerPurpose( $view, $layer, $purpose );
    assert_def($success, __LINE__ );
    my @p1 = (-50,50);
    my @p2 = ( 15,50);
    my @pointArray1 = ( \@p1, \@p2 );

    my $path  = oa::oaPath::create( $block, $pinLayer, $pinPurp, 2, \@pointArray1 );
    assert_def($path->isValid(), __LINE__ );

    my $name = new oa::oaScalarName( $ns, "TopTail");
    my $net  = oa::oaScalarNet::create( $block, $name );
    assert_def($net->isValid(), __LINE__ );
    $path->addToNet( $net );

    my $termName = new oa::oaScalarName( $ns, "T1" );
    my $termTypeInput = new oa::oaTermType( $oa::oacInputTermType );
    my $term = oa::oaScalarTerm::create( $net, $termName, $termTypeInput );
    assert_def($term->isValid(), __LINE__ );

    my @box = (-65, 46, -50, 54);
    my $rect = oa::oaRect::create( $block, $pinLayer, $pinPurp, \@box );
    assert_def($rect->isValid(), __LINE__ );

    my $pin = oa::oaPin::create( $term, "IN1", $oa::oacLeft );
    $rect->addToPin( $pin );
    assert_def($pin->isValid(), __LINE__ );

    my @p3 = (-50,20);
    my @p4 = ( 15,20);
    my @pointArray2 = ( \@p3, \@p4 );

    $path = oa::oaPath::create( $block, $pinLayer, $pinPurp, 2, \@pointArray2 );
    assert_def($path->isValid(), __LINE__ );

    $name = new oa::oaScalarName( $ns, "BotTail" );
    $net  = oa::oaScalarNet::create( $block, $name );
    assert_def( $net->isValid(), __LINE__ );
    $path->addToNet( $net );

    $termName  = new oa::oaScalarName( $ns, "T2" );
    $term  = oa::oaScalarTerm::create( $net, $termName, $termTypeInput );
    assert_def($term->isValid(), __LINE__ );

    @box  = (-65, 16, -50, 24);
    $rect = oa::oaRect::create( $block, $pinLayer, $pinPurp, \@box );
    assert_def($rect->isValid(), __LINE__ );

    $pin  = oa::oaPin::create( $term, "IN2", $oa::oacLeft );
    $rect->addToPin( $pin );
    assert_def($pin->isValid(), __LINE__ );

    my $drLayer = 0;
    my $drPurp  = 0;
    

    ($success, $drLayer, $drPurp) = getLayerPurpose( $view, "device", "drawing2" );
    assert_def($success, __LINE__ );
   
    my @pt1  = (  0,  0);
    my @pt2  = ( 50,  0);
    my @pt3  = ( 80, 10);
    my @pt4  = (100, 35);
    my @pt5  = ( 80, 60);
    my @pt6  = ( 50, 70);
    my @pt7  = (  0, 70);
    my @pt8  = ( 14, 55);
    my @pt9  = ( 20, 35);
    my @pt10 = ( 14, 15);
    my @ptArray3 = ( \@pt1, \@pt2, \@pt3, \@pt4, \@pt5, \@pt6, \@pt7, \@pt8, \@pt9, \@pt10 );

    my $gate = oa::oaPolygon::create( $block, $drLayer, $drPurp, \@ptArray3 );
    assert_def($gate->isValid(), __LINE__ );

    my @pt11 = (100, 35);
    my @pt12 = (140, 35);
    my @ptArray5 = ( \@pt11, \@pt12 );

    my $path2 = oa::oaPath::create( $block, $pinLayer, $pinPurp, 2, \@ptArray5 );
    assert_def($path2->isValid(), __LINE__ );

    $name  =  new oa::oaScalarName( $ns, "OutTail");
    my $net2  = oa::oaScalarNet::create( $block, $name );
    assert_def($net2->isValid(), __LINE__ );

    $path2->addToNet( $net2 );
    $termName  = new  oa::oaScalarName( $ns, "T3");
    my $termTypeOutput = new oa::oaTermType( $oa::oacOutputTermType);
    my $term2  = oa::oaScalarTerm::create( $net2, $termName, $termTypeOutput );

    @box = (140, 31, 155, 39);
    my $rect2 = oa::oaRect::create( $block, $pinLayer, $pinPurp, \@box);
    assert_def($rect2->isValid(), __LINE__ );

    my $pin2 = oa::oaPin::create( $term2, "OUT", $oa::oacRight );
    $rect2->addToPin( $pin2 );
    assert_def($pin2->isValid(),__LINE__ );

    $view->save();
    $view->close();

}



#-------------------------------------------------------------------------------

sub openLibrary ($$) {
    my $libStr = shift;
    my $strPathLib = shift;
    
    my $nsUnix = new oa::oaUnixNS();
    my $scNameLib = new oa::oaScalarName($nsUnix, $libStr);

    my $lib;

    $lib = oa::oaLib::find($scNameLib);
    if ($lib) {
        printf("***ERROR error lib is already open\n");
        exit 1;
    }

    # The following commented out code illustrates how the API locates a lib.defs file.
    # Unfortunately, the results are unpredictable because the current user may or may
    # not have a lib.defs file either in the current directory or in $HOME.
    # It has been commented out so that automated run/check tests can predict the output.
    # If you are a user who is interested in this, please feel free to uncomment this section.
    #$strLibDefsFile = oa::oaString();
    #oa::oaLibDefList::getDefaultPath($strLibDefsFile);
    
    #if strLibDefsFile == "":
    #    print "No libdefs file"
    #else:
    #    print "Existing lib.defs file"


    my $ldl = oa::oaLibDefList::get( "lib.defs", "w" );

    if (oa::oaLib::exists($strPathLib)) {
        $lib =  oa::oaLib::open($scNameLib, $strPathLib);
        printf("Opened existing %s in %s\n", $libStr, $strPathLib );
    } else {
        $lib =  oa::oaLib::create($scNameLib, $strPathLib, new oa::oaLibMode($oa::oacSharedLibMode), 'oaDMFileSys');
        printf("Created %s in %s\n", $libStr, $strPathLib );
    }

    &oa::oaLibDef::create($ldl, $scNameLib, $strPathLib);
    $ldl->save();
    printf("Adding def for %s to %s to newly created libdefs file\n",$libStr, $strPathLib);

    if (! $lib->isValid() ) {
        printf("***ERROR lib is not valid\n");
    }
    if (!  &oa::oaLib::exists($strPathLib)) {
        printf("***ERROR lib does not exist\n");
    }

    return $scNameLib;
}


1;
