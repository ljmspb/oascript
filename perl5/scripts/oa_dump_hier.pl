#!/usr/bin/env perl

#-------------------------------------------------------------------------------
#
# Copyright (C) 2010, Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not use
# this file except in compliance with the License. You may obtain a copy of the
# License at http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software distributed
# under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
# CONDITIONS OF ANY KIND, either express or implied. See the License for the
# specific language governing permissions and limitations under the License.
#
#-------------------------------------------------------------------------------


# oa_dump_hier.pl - Perl script to dump the hierarchy of an OA cellview using a
# printf-like format specifier.

use strict;
no strict qw(refs);
use warnings;
use File::Basename;
use FindBin;
use Getopt::Long;

use lib dirname($FindBin::RealBin);
use oa::design;

our $ns;
our $_d;

eval {
    oa::oaDesignInit( $oa::oacAPIMajorRevNumber,
		      $oa::oacAPIMinorRevNumber,
		      $oa::oacDataModelRevNumber );
    our $format = '%./%c';
    if (!GetOptions('format=s' => \$format) || scalar(@ARGV) < 3) {
	print << 'EOD';
Use : 

  oa_dump_hier.pl [-format <format string>] libraryName cellName viewName

Format string can contain:

   %i - instance name
   %l - library name
   %c - cell name
   %v - view name
   %n - number of placements
   %L - master cell library name
   %C - master cell name
   %V - master cell view name
   %s - indented spaces 
   %d - depth number (level)
   %o - instance orientation
   %O - instance x & y coordinate
   %. - all strings on levels before this point
   %% - a literal % sign (to escape the percent)

All other characters are printed literally.

EOD
	exit 1;
    }

    $ns = new oa::oaNativeNS;

    my ($libstr, $cellstr, $viewstr) = @ARGV;
    my $libName = new oa::oaScalarName($ns, $libstr);
    my $cellName = new oa::oaScalarName($ns, $cellstr);
    my $viewName = new oa::oaScalarName($ns, $viewstr);

    my $pathLibDefs = "./lib.defs";
    if (! (new oa::oaFile($pathLibDefs))->exists()) {
	die(sprintf( "***Must have %s file.\n", $pathLibDefs) );
    }

    &oa::oaLibDefList::openLibs();
    my $lib;
    if (!($lib = oa::oaLib::find($libName))) {
	die(sprintf( "***Either Lib name=%s or its path mapping in file=%s is invalid.\n",
		$libstr, $pathLibDefs) );
    }

    my $design;
    $design = oa::oaDesign::open($libName, $cellName, $viewName, "r");

    unless ($design) {
	die(sprintf("Can't read Design %s/%s/%s (check lib.defs)\n",
	       $libstr, $cellstr, $viewstr));
    }

#    printf( "Reading Design %s/%s/%s\n", $libstr, $cellstr, $viewstr);

    my $block = $design->getTopBlock();
    &dumpHier($block, $design, $format);
};

if ($@) {
    die(sprintf("Error: %s\n", $@));
}

exit(0);

#-------------------------------------------------------------------------------

sub dumpHier {
    my ($block, $design, $format, $excludeTopCell, @level) = @_;
    my %masterTable;
    my $depth = scalar(@level);
    my $schView = "schematic";
    my $str;
    my $dbuPerUU = $block->getDBUPerUU();

    my ($lstr, $cstr, $vstr);
    $design->getLibName ($ns, $lstr);
    $design->getCellName($ns, $cstr);
    $design->getViewName($ns, $vstr);

    if ($depth == 0 && !$excludeTopCell) {
	$str = &dumpHierBuildString($format, 'TOP', $lstr, $cstr, $vstr, $lstr, $cstr, $vstr, 0, 0, 'n/a', 'n/a', '');
	print "$str\n";
	$depth = 1;
	push @level, $str;
    }

    my %masters;

    my $iIter = new oa::oaIter::oaInst($block->getInsts());
    while (my $inst = $iIter->getNext()) {
	my ($libStr, $cellStr, $viewStr);
	
	$inst->getLibName ($ns, $libStr );
	$inst->getCellName($ns, $cellStr);
	$inst->getViewName($ns, $viewStr);

	push @{$masters{$libStr,$cellStr,$viewStr}}, $inst;
    }

    foreach my $master (sort keys %masters) {
	my $insts = $masters{$master};
	my @insts = @{$insts};
	my $num = 0;
	foreach my $inst (@insts) {
	    my ($numInsts, $numRows, $numCols, $addnum);
	    $addnum = 1; 
	    $numInsts = $inst->getNumBits();
	    $addnum *= ($numInsts || 1);
	    if ($inst->getType eq $oa::oacArrayInstType) {
		$numRows = $inst->getNumRows();
		$addnum *= ($numRows || 1);
		$numCols = $inst->getNumCols();
		$addnum *= ($numCols || 1);
	    }
	    $num += $addnum;
	}
	@insts = @{$insts};
	if ($format !~ "\%i") {
	    @insts = ($insts[0]);
	}
	foreach my $inst (@insts) {
	    my ($iStr, $origin, $orient, $transform, $bbox, $libStr, $cellStr, $viewStr);
	    $inst->getName($ns, $iStr);
	    $inst->getOrigin($origin);
	    foreach (@$origin) {$_ /= $dbuPerUU}

	    $orient = new oa::oaOrient($inst->getOrient());
#	    $inst->getTransform($transform);
	    $inst->getBBox($bbox);
	    $inst->getLibName ($ns, $libStr );
	    $inst->getCellName($ns, $cellStr);
	    $inst->getViewName($ns, $viewStr);

	    my $libName = new oa::oaScalarName($ns, $libStr);
	    my $cellName = new oa::oaScalarName($ns, $cellStr);
	    my $viewName = new oa::oaScalarName($ns, $viewStr);
	    my $subdesign;
	    eval {
		if ($viewStr eq "symbol") {
		    $viewName = new oa::oaScalarName($ns, "schematic");
		    $subdesign = oa::oaDesign::open($libName, $cellName, $viewName, "r");
		} else {
		    $subdesign = oa::oaDesign::open($libName, $cellName, $viewName, "r");
		}
	    };
	    if (!$@ && $subdesign) {
		$str = &dumpHierBuildString($format, $iStr, $libStr, $cellStr, $viewStr, $lstr, $cstr, $vstr, $num, $depth, $orient->getName(), join(',',@$origin), $level[0]);
		print "$str\n";
		my $subblock = $subdesign->getTopBlock();
		&dumpHier($subblock, $subdesign, $format, 0, $str, @level);
	    }
	}
    }
}

sub dumpHierBuildString {
    our ($format, $i, $l, $c, $v, $L, $C, $V, $n, $d, $o, $O, $a) = @_;

    our $b = '%%';

    our $S = ' ' x length($a);
    our $s = ' ' x (2 * $d);

    $format =~ s/\%\%/\%b/g;
    $format =~ s/\%\./\%a/g;

    $format =~ s/\%(\w)/$${1}/g;

    return $format;
}
