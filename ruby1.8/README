###############################################################################
#
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################

This is a description of the OpenAccess Scripting Ruby bindings (oasRuby)

# TODO: Hashing behavior for wrapped types

--------------------------------------------------------------------------------
Building
--------------------------------------------------------------------------------

To build the Ruby bindings, go to the root of the language directory and type:

make ruby


--------------------------------------------------------------------------------
Testing
--------------------------------------------------------------------------------

A few test targets are defined in the Ruby Makefile.  When inside the Ruby
directory use one of the commands below:

make test_basic       # << Test that OAS Ruby libraries load
make test_unit        # << Test various OAS Ruby 
make test             # << Run test_basic, test_unit, and OA labs in sequence

There are test programs in the test/ directory.  If invoked without
arguments, they will allow OA to use its default search algorithm to find
a lib.defs when a library definition file is needed.  An alternate library
definitions file location may be specified as follows:

% (base|dm|tech|design)_test.pl --libdefs <path to lib.defs>

Some test programs also require a library, cell, and view to be specified.  The
--lib, --cell, and --view arguments must be specified in these cases.


--------------------------------------------------------------------------------
Using
--------------------------------------------------------------------------------

To use the Ruby bindings the $RUBYLIB environment variable should be set to this
Ruby build directory (where oa.rb and the oa directory exists).  Ruby 1.8.4 was
used extensively for testing but any version at 1.8 and below 1.9 should work
(Ruby 1.9 support will come later).  The bindings can be loaded by adding the
following to the top of your Ruby script:

  require 'oa'

If the OA Ruby bindings need to be moved to an alternate directory then the
oa.rb file and oa directory should be copied and $RUBYLIB should be set to the
location where this file and directory is located.


--------------------------------------------------------------------------------
Examples
--------------------------------------------------------------------------------

The "test" directory has examples of scripts.  Also there is a "labs" directory
which contains Ruby versions of the OA C++ labs provided by Si2.


--------------------------------------------------------------------------------
Naming Conventions
--------------------------------------------------------------------------------

The C++ API is wrapped as-is and name changes are made only to be compatible
with some Ruby naming requirements.  For example, all modules, classes and
constants in Ruby must start with a capital letter.  Therefore, the class
oa::oaLib in the C++ API will become Oa::OaLib in Ruby and the constant
oa::oacReadLibAccess will become Oa::OacReadLibAccess in Ruby.  Method names
match those in the C++ API.


--------------------------------------------------------------------------------
Modules and Namespaces
--------------------------------------------------------------------------------

To keep the code size manageable, the API is split into parts, based on the
subdirectories in the OA source area.  Currently, the Ruby modules are:

oa/base
oa/common
oa/design
oa/dm
oa/plugin
oa/tech
oa/util
oa/wafer

However, to avoid unnecessary nesting of Ruby namespaces, no discrete modules
are defined by these names.  Instead all the classes defined in these modules
are promoted into the 'Oa' module.  For example, the C++ oaLib class, which is
defined in module oa::dm, becomes the Oa::OaLib class in Ruby (rather than
Oa::OaDm::OaLib).

A top-level module named "oa" is defined to make loading all of the OA modules
into Ruby easy:

  require 'oa'

To avoid having to qualify all access to classes, functions, constants, etc.
using Oa:: the module may be included after the "require" line:

  include Oa


--------------------------------------------------------------------------------
Enumerated Types and Enum Wrappers
--------------------------------------------------------------------------------

All enumerated values in OA (e.g., oaLibAccessEnum) are available as integer
constants in the Oa module:

  access = Oa::OacReadLibAccess

Classes that are simple wrappers around an enumerated value (e.g. oaLibAccess)
are supported in the wrappers:

  access = Oa::OaLibAccess.new(Oa::OacReadLibAccess)

As a convenience, when a function takes an enum wrapper argument, you may
pass an enum wrapper object, the enumerated integer value, or a Ruby symbol
named after the enumeration name.  The following are all equivalent:

  lib.getAccess(Oa::OaLibAccess.new(Oa::OacReadLibAccess))
  lib.getAccess(Oa::OaLibAccess.new('read'))
  lib.getAccess(Oa::OacReadLibAccess)
  lib.getAccess(:read)


The return value for enumerated types will be the wrapped enumerated class.
Conversions from the wrapped class to an integer and string can be accomplished
through the #to_i and #to_s methods respectively.  The getName() method is
still available as well.  For example:

  access.to_i  => 0        # (value of Oa::OacReadLibAccess)
  access.to_s  => 'read'

TODO: Should this also accept a string as an input?


--------------------------------------------------------------------------------
oaString
--------------------------------------------------------------------------------

The oaString class is translated as a plain Ruby string, and a Ruby string
is used in place of an oaString object for function arguments.  The Ruby
API uses the same pass-by-reference model that the C++ API uses.  For
example (from dm_test.pl):

  lib.getPath(libpath='')
  puts "libpath = " + libpath


--------------------------------------------------------------------------------
oaScalarName
--------------------------------------------------------------------------------

The "name" classes in OA (oaSimpleName, etc.) with the exception of oaScalarName
are not handled specially; they are fully-fledged class objects.  An
oaScalarName may be used as is traditionally:

  ns = Oa::OaNativeNS.new
  libname = Oa::OaScalarName.new(ns, "stdcell")
  lib = Oa::OaLib.find(libname)

Or a string can be used instead in which case the default namespace will be used
(the default namespace is initially set to oaNativeNS):

  lib = Oa::OaLib.find("stdcell")

The default namespace can be retrieved and set as follows:

  Oa::OaNameSpace.default
  Oa::OaNameSpace.default = Oa::OaCdbaNS.new

To temporarily use a different namespace the Oa::OaNameSpace#use method can be
called.  The temporary default namespace will revert outside of the provided
block.  For example:

  # Default NS is OaNativeNS
  Oa::OaNameSpace.use(OaCdbaNS.new) do
    # Default NS is OaCdbaNS in this block
  end
  # Default NS is OaNativeNS again


--------------------------------------------------------------------------------
oaPoint
--------------------------------------------------------------------------------

The oaPoint class is wrapped; however, a two-element array containing integer
values may be supplied as an oaPoint input instead of creating an oaPoint
object.

  ref.getOrigin(point=Oa::OaPoint.new)
  x = point.x
  y = point.y

Using oaPoint as an input:

  ref.setOrigin(Oa::OaPoint.new(new_x, new_y))

Using a two-element array as an input:

  ref.setOrigin [new_x, new_y]


--------------------------------------------------------------------------------
oaBox
--------------------------------------------------------------------------------

The oaBox class is wrapped; however, a four-element array containing integer
values may be supplied as an oaBox input instead of creating an oaBox object.

  design.getBBox(bbox=Oa::OaBox.new)
  x1 = bbox.left
  y1 = bbox.bottom
  x2 = bbox.right
  y2 = bbox.top

Using oaBox as an input:

  rect.setBBox(Oa::OaBox.new(new_x1, new_y1, new_x2, new_y2))

Using a four-element array as an input:

  rect.setBBox [new_x1, new_y1, new_x2, new_y2]

An oaBox can be quickly manipulated using +, -, *, / with another oaPoint object
(not shown) or simply a Ruby Array (shown):

  box = Oa::OaBox.new(0,0,2,2)   #=> OaBox(l:0, b:0, r:2, t:2)
  new_box = box * 2.0            #=> OaBox(l:0, b:0, r:4, t:4)
  new_box = box / 2.0            #=> OaBox(l:0, b:0, r:1, t:1)
  new_box = box + 1              #=> OaBox(l:1, b:1, r:3, t:3)
  new_box = box - 1              #=> OaBox(l:-1, b:-1, r:1, t:1)
  new_box = box + [1,1]          #=> OaBox(l:1, b:1, r:3, t:3)
  new_box = box - [1,1]          #=> OaBox(l:-1, b:-1, r:1, t:1)


--------------------------------------------------------------------------------
oaTransform
--------------------------------------------------------------------------------

The oaTransform class is wrapped; however, a three-element array containing
two integer values for the x and y offset and a Ruby Symbol for the orientation
(e.g. :R0, :R90, etc.) may be supplied as an oaTransform input instead of
creating an oaTransform object.

  inst.getTransform(xform=Oa::OaTransform.new)
  x = xform.xOffset
  y = xform.yOffset
  o = xform.orient

Using oaTransform as an input - the following are equivalent:

  inst.setTransform(Oa::OaTransform.new(1, 2, Oa::OaOrient.new(OacR90)))
  inst.setTransform [1, 2, :R90]

Concatenation of two transformations can be done using the #transform method
or by simply using "+":

  new_xform = xform.concat(other_xform)
  new_xform = xform + other_xform


--------------------------------------------------------------------------------
oaViewType
--------------------------------------------------------------------------------

The oaViewType class is wrapped; however, a Ruby Symbol can be provided in its
place.  The Symbol name matches the name of the oaViewType.  The following are
equivalent:

  design = OaDesign.open(libname, cellname, viewname, OaViewType.get(OaReservedViewType.new('maskLayout')), 'w')
  design = OaDesign.open(libname, cellname, viewname, OaViewType.get(:maskLayout), 'w')
  design = OaDesign.open(libname, cellname, viewname, :maskLayout, 'w')


--------------------------------------------------------------------------------
oaTime
--------------------------------------------------------------------------------

The oaTime type is converted to/from a native Ruby Time object.  For example:

  design.getTech.getCreateTime   #=> Wed Aug 18 07:32:07 MST 2010
  tprop = Oa::OaTimeProp.create(design, 'mytime', Time.now))
  tprop.getValue                 #=> Wed Aug 18 07:34:50 MST 2010


--------------------------------------------------------------------------------
oaTimeStamp
--------------------------------------------------------------------------------

The oaTimeStamp type is converted to/from a native Ruby Fixnum object.  For 
example:

  design.getTimeStamp(:designDataType)   #=> 17


--------------------------------------------------------------------------------
Collections and Iterators
--------------------------------------------------------------------------------

oaCollection is supported:

  cell_collection = lib.getCells
  unless cell_collection.isEmpty
    ...
  end

For the templated class oaIter<class T>, a sub-namespace of oaIter is used
for each possible template parameter.  Iterators can be used in C++ API style
or as a native Ruby Enumerable object.  Example of the traditional method:

  cell_iter = Oa::OaIter::OaCell(lib.getCells)
  while cell = cell_iter.getNext do
    ...
  end

oaCollection objects can be converted to an iterator object of the same type
by simply calling the #to_iter method:

  cell_iter = lib.getCells.to_iter

Both oaCollection and oaIter objects respond to the native Ruby #each method and
inherit extra methods from the Ruby Enumerable class (e.g. #find, #map, #sort).
In the case of oaCollection, the #each method first converts the oaCollection
into an oaIter and then iterates over the new object.  For example (from
dm_test.rb):

  lib.getCells.each do |cell|
    ...
  end


--------------------------------------------------------------------------------
Arrays
--------------------------------------------------------------------------------

All oaArray<> template instantiations, and all classes that are derived from
an oaArray<> template instantiation, are translated as regular Ruby arrays.
For example (from base_test.pl):

  Oa::OaBuildInfo.getPackages(bi_arr=[])
  bi_arr.each do |bi|
    printf "%-15s  %-15s\n", bi.getPackageName, bi.getBuildName
  end

TODO: get* Array methods with void return values should return the Array


--------------------------------------------------------------------------------
oaAppDef
--------------------------------------------------------------------------------

The oa*AppDef template sub-classes are wrapped into two classes:

Oa*AppDefTemplate - constructs the template for an object class
Oa*AppDefProxy - constructs a proxy to the oa*AppDef class

The Oa*AppDefTemplate classes are also named without the "Template" suffix for
convenience to match the C++ API.  The #[] class method on these classes is
used to create a template for a given type.

For example:

  int = Oa::OaIntAppDef[Oa::OaNet].get('foo')
  printf "name: %s, default: %s\n", int.getName, int.getDefault


--------------------------------------------------------------------------------
Exceptions
--------------------------------------------------------------------------------

Exceptions thrown by the C++ API are caught, and raised as a Oa::OaException
in Ruby.  Scripters are advised to wrap their OA calls inside of a 'begin' block
like this:

begin
  ...
rescue Oa::OaException => excp
  abort "Caught exception: #{excp}"
end

TODO: In the future may raise with child oaException (e.g. oaDesignException)


--------------------------------------------------------------------------------
oaLibDefList - special handling of open() and openLibs()
--------------------------------------------------------------------------------

The default OA behavior is to silently fail if open() or openLibs() encounter
an error in reading the lib.defs file.  All oaScript languages will throw an
exception if this happens to avoid the pitfalls of silent failures.  Ruby is
no exception (pun intended).  Here is an example to trap parse errors and
report them to the user - but continue onward.

begin
  Oa::OaLibDefList::openLibs
rescue RuntimeError => e
  warn e
end

The warnings given are self-explanatory, so you can just expose them exactly
as received (as shown in the example) or you can add a summary header or some
other handling as desired.


--------------------------------------------------------------------------------
Duck Typing (coercions)
--------------------------------------------------------------------------------

Ruby uses "duck typing".  What this means is that an object can be made to 
"quack like a duck" or in other words an object can be made to look like
something else.  In Ruby there are some default standards for defining methods
to convert to various object types (when applicable).  For example:

  obj.to_s  => Convert object to a String
  obj.to_i  => Convert object to a Fixnum (integer)
  obj.to_f  => Convert object to a Float
  obj.to_a  => Convert object to an Array

The OA Ruby bindings define these conversions for certain wrapped OA objects.
A few examples include:

  <Wrapped Enums>    #to_i, #to_s
  Oa::OaCollection   #to_iter
  *::toInt           #to_i
  *::toDouble        #to_f


--------------------------------------------------------------------------------
Troubleshooting
--------------------------------------------------------------------------------

Ruby 1.8.4 has been tested but 1.8.5+ is required on RH 5.6 and SLES10 due to
a conflict with eaccess being redefined in Ruby's intern.h.  This is 
circumvented in 1.8.5+ because the eaccess prototype is protected with a 
#ifndef HAVE_EACCESS.
