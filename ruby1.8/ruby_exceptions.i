/* ****************************************************************************

   Copyright 2010 Intel Corporation

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 ******************************************************************************
   Version History

    DATE      BY               DESCRIPTION
    08/18/10  jamesm, Intel    Initial copy (broken out from ruby_oa.i)

 *************************************************************************** */


// This line places oaException as a child class of Ruby's RuntimeError
// exception class thereby making oaException (and subclasses) raiseable.
%exceptionclass OpenAccess_4::oaException;

// Trap exceptions on all wrapped calls.  We must use %raise in this manner
// since we want to raise on an *instance* of an exception object (not the
// normal way in Ruby using a class).  When the class version is used there
// is an assumption that the object constructor takes a string as an argument
// and this is not the case for oaException (it takes an integer).  Much of
// this method was gleaned from the SWIG page on Ruby.
%exception {
   try {
     $action
   } catch(OpenAccess_4::oaException& excp) {
     LANG_THROW_OAEXCP(excp);
   }
}
