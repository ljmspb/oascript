#! /usr/bin/env ruby
###############################################################################
#
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   06/07/2010: Initial copy created
#
###############################################################################

require 'oa'
require 'getoptlong'

include Oa

##############################################################################
# DEFAULTS
##############################################################################

# Layer definitions: name, lay_num, material_str
layers = [['poly',    1, 'poly'],
          ['ndiff',   2, 'nDiff'],
          ['pdiff',   3, 'pDiff'],
          ['contact', 4, 'cut'],
          ['metal1',  5, 'metal'],
          ['via1',    6, 'cut']]

max_shapes = 1_000_000
unit_size = 500

##############################################################################
# FUNCTIONS
##############################################################################
def show_time(title)
  printf "%-20s", title
  timer = OaTimer.new
  yield
  printf "%-4.2f\n", timer.getElapsed
end

##############################################################################
# ARGUMENTS
##############################################################################

opts = GetoptLong.new(['--libdefs', GetoptLong::REQUIRED_ARGUMENT],
                      ['--lib', GetoptLong::REQUIRED_ARGUMENT],
                      ['--cell', GetoptLong::REQUIRED_ARGUMENT],
                      ['--view', GetoptLong::REQUIRED_ARGUMENT],
                      ['--test-safety', GetoptLong::NO_ARGUMENT])

libname = nil
cellname = nil
viewname = nil
libdefs = nil
test_safety = nil

opts.each do |opt, arg|
  case opt
  when '--libdefs'
    libdefs = arg
  when '--lib'
    libname = arg
  when '--cell'
    cellname = arg
  when '--view'
    viewname = arg
  when '--test-safety'
    test_safety = true
  end
end

abort "Missing mandatory --lib argument" unless libname
abort "Missing mandatory --cell argument" unless cellname
abort "Missing mandatory --view argument" unless viewname                                     

begin
  oaDesignInit

  if libdefs
    OaLibDefList.openLibs libdefs
  else
    OaLibDefList.openLibs
  end

  # Create the library
  if lib = OaLib.find(libname)
    abort "Library already exists - remove first: #{libname}"
  end

  lib = OaLib.create(libname, "./#{libname}", OaLibMode.new(OacSharedLibMode), 'oaDMFileSys')
  unless lib.getAccess(OaLibAccess.new('write'))
    abort "lib #{libname}: Cannot get write access!"
  end

  # Create the tech in the new library
  # TODO: Should be able to use lib object - need to fix the oaScalarName type map
  #tech = OaTech.create(lib)
  tech = OaTech.create libname
  layers.each do |layer|
    name, num, mat_str = layer
    OaPhysicalLayer.create(tech, name, num, OaMaterial.new(mat_str))
  end

  # Use the first layer number in creating shapes on drawing purpose
  use_lnum = layers.first[1]
  use_pnum = OavPurposeNumberDrawing

  # Create a design and top block
  design = OaDesign.open(libname, cellname, viewname, OaViewType.get(OaReservedViewType.new('maskLayout')), 'w')
  block = OaBlock.create design

  # Create a bunch of rectangles
  show_time 'Create rectangles' do
    for i in 1..max_shapes do 
      #box = OaBox.new(i*unit_size, 0, (i+1)*unit_size, unit_size)
      box = [i*unit_size, 0, (i+1)*unit_size, unit_size]
      OaRect.create block, use_lnum, use_pnum, box
    end
  end

  # Create a bunch of paths
  show_time 'Create paths' do
    for i in 1..max_shapes do
      st = i*2*unit_size
      #pts = [OaPoint.new(st, 0), OaPoint.new(st+i*unit_size, 0), OaPoint.new(st+i*unit_size, i*unit_size)]
      pts = [[st, 0], [st+i*unit_size, 0], [st+i*unit_size, i*unit_size]]
      OaPath.create block, use_lnum, use_pnum, unit_size/4, pts
    end
  end

  # Iterate through all shapes in the block
  show_time('Iterate shapes') { block.getShapes.each {|shape| shape.isValid}}

  design.save
  design.close

rescue OaException => excp
  abort "Caught exception: #{excp}"
end
