#! /usr/bin/env/ruby
###############################################################################
#
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   06/02/2010: Copied from design_test.rb to test for isValid performance.
#               Use $OASCRIPT_USE_ISVALID=1 to enable additional call to
#               isValid.
#
###############################################################################

require 'oa'
require 'getoptlong'

include Oa

module Oa
  class OaBlock
    def time_isvalid(num_iter)
      timer = OaTimer.new
      for i in (1..num_iter) do
        isValid
      end
      timer.getElapsed
    end
  end
end

opts = GetoptLong.new(['--libdefs', GetoptLong::REQUIRED_ARGUMENT],
                      ['--lib', GetoptLong::REQUIRED_ARGUMENT],
                      ['--cell', GetoptLong::REQUIRED_ARGUMENT],
                      ['--view', GetoptLong::REQUIRED_ARGUMENT],
                      ['--nums', GetoptLong::REQUIRED_ARGUMENT],
                      ['--verbose', GetoptLong::NO_ARGUMENT])

libname = nil
cellname = nil
viewname = nil
libdefs = nil
verbose = nil

nums = []
default_nums = [1_000, 10_000, 100_000, 1_000_000, 10_000_000, 100_000_000, 1_000_000_000]
warmup_num = 1_000

opts.each do |opt, arg|
  case opt
  when '--libdefs'
    libdefs = arg
  when '--lib'
    libname = arg
  when '--cell'
    cellname = arg
  when '--view'
    viewname = arg
  when '--nums'
    nums = arg.split.map {|e| e.to_f}
  when '--verbose'
    verbose = true
  end
end

nums = default_nums if nums.empty?
nums.map! {|e| e.to_i}

abort "Missing mandatory --lib argument" unless libname
abort "Missing mandatory --cell argument" unless cellname
abort "Missing mandatory --view argument" unless viewname                                     

begin
  oaDesignInit

  if libdefs
    OaLibDefList.openLibs libdefs
  else
    OaLibDefList.openLibs
  end

  unless lib = OaLib.find(libname)
    abort "Couldn't find library #{libname}"
  end

  unless lib.getAccess(OaLibAccess.new('read'))
    abort "lib #{libname}: No access!"
  end

  design = OaDesign.open(libname, cellname, viewname, 'r')
  block = design.getTopBlock

  abort "No design!" unless design
  abort "No top block!" unless block

  puts "Warmup call to isValid (#{warmup_num} times)..." if verbose
  block.time_isvalid(warmup_num)

  printf "%16s %16s\n", '# Iter', 'Elapsed (s)'
  printf "%16s %16s\n", '------', '-----------'
  nums.each do |num|
    printf "%16d %16.2f\n", num, block.time_isvalid(num)
  end

  design.close

rescue OaException => excp
  abort "Caught exception: #{excp}"
end
