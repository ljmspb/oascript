#! /usr/bin/env/ruby
###############################################################################
#
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   02/16/2010: Copied from Python bindings (authored by AMD) and modified by
#               Intel Corporation for Ruby.
#
###############################################################################

require 'oa'
require 'getoptlong'

include Oa

opts = GetoptLong.new(['--libdefs', GetoptLong::REQUIRED_ARGUMENT],
                      ['--quiet', '-q', GetoptLong::REQUIRED_ARGUMENT])

quiet = nil
libdefs = nil

opts.each do |opt, arg|
  case opt
  when '--libdefs'
    libdefs = arg
  when '--quiet'
    quiet = arg
  end
end


ns = OaNativeNS.new

oaDesignInit

if libdefs
  OaLibDefList.openLibs libdefs
else
  OaLibDefList.openLibs
end

OaLib.getOpenLibs.each do |lib|
  libname = ''
  lib.getName(ns, libname)
  # TODO: typemap for OaLibAccess
  if lib.getAccess(OacReadLibAccess)
    lib.getPath(libpath='')
    puts "lib #{libname} path=#{libpath}" unless quiet
    lib.getCells.each do |cell|
      cell.getName(ns, cellname='')
      puts "  #{cellname}" unless quiet
      cell.getCellViews.each do |cv|
        view = cv.getView
        view.getName(ns, viewname='')
        print "    #{viewname}" unless quiet
        cv.getDMFiles.each do |file|
          file.getPath(filename='')
          size = file.getOnDiskSize
          puts "      #{File.split(filename).last} (#{size} bytes)" unless quiet
        end
      end
    end
  else
    warn "lib #{libname}: No access!"
  end
end

