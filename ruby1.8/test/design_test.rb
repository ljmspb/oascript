#! /usr/bin/env/ruby
###############################################################################
#
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   02/16/2010: Copied from Python bindings (authored by AMD) and modified by
#               Intel Corporation for Ruby.
#
###############################################################################

require 'oa'
require 'getoptlong'

include Oa

opts = GetoptLong.new(['--libdefs', GetoptLong::REQUIRED_ARGUMENT],
                      ['--lib', GetoptLong::REQUIRED_ARGUMENT],
                      ['--cell', GetoptLong::REQUIRED_ARGUMENT],
                      ['--view', GetoptLong::REQUIRED_ARGUMENT])

libname = nil
cellname = nil
viewname = nil
libdefs = nil

opts.each do |opt, arg|
  case opt
  when '--libdefs'
    libdefs = arg
  when '--lib'
    libname = arg
  when '--cell'
    cellname = arg
  when '--view'
    viewname = arg
  end
end

abort "Missing mandatory --lib argument" unless libname
abort "Missing mandatory --cell argument" unless cellname
abort "Missing mandatory --view argument" unless viewname                                     

begin
  oaDesignInit

  if libdefs
    OaLibDefList.openLibs libdefs
  else
    OaLibDefList.openLibs
  end

  unless lib = OaLib.find(libname)
    abort "Couldn't find library #{libname}"
  end

  unless lib.getAccess(OaLibAccess.new('read'))
    abort "lib #{libname}: No access!"
  end

  design = OaDesign.open(libname, cellname, viewname, 'r')
  block = design.getTopBlock

  abort "No design!" unless design
  abort "No top block!" unless block

  block.getShapes.each do |shape|
    warn shape.inspect
    bbox = shape.getBBox()
    puts "Shape #{shape.getType.getName} on layer #{shape.getLayerNum} with bbox #{bbox.to_a.inspect}"
  end

  design.close

rescue OaException => excp
  abort "Caught exception: #{excp}"
end
