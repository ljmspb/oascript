#!/usr/bin/env tcl
##############################################################################
# Copyright 2009 Advanced Micro Devices, Inc.
# Copyright 2010 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

package forget oa

set path [file dirname [info script]]
set libpath [file normalize [file join $path .. ]]
lappend auto_path $libpath

package require oa

proc main {} {
    global argv
    if {[set i [lsearch $argv "--quiet"]] != -1} {
        set quiet 1
        set argv [lreplace $argv $i $i]
    } else {
        set quiet 0
    }
    array set opt $argv

    oa::oaDMInit
    if {[info exists opt(--libdefs)]} {
        oa::oaLibDefList_openLibs $opt(--libdefs)
    } else {
        oa::oaLibDefList_openLibs
    }

    set ns [oa::oaNativeNS]

    oa::foreach lib [oa::oaLib_getOpenLibs] {
        set libname [$lib getName $ns]
        set lib_access [oa::oaLibAccess "read"]
        if {![$lib getAccess $lib_access]} {
            puts "lib $libname: No Access!"
            continue
        }
        set libpath [$lib getPath]
        puts "lib $libname path=$libpath"
        oa::foreach cell [$lib getCells] {
            set cellname [$cell getName $ns]
            if {!$quiet} {
                puts "  $cellname"
            }
            oa::foreach cv [$cell getCellViews] {
                set view [$cv getView]
                set viewname [$view getName $ns]
                if {!$quiet} {
                    puts "    $viewname"
                }
                oa::foreach dmfile [oa::oaDMContainer_getDMFiles $cv] {
                    set filename [$dmfile getPath]
                    set size [$dmfile getOnDiskSize]
                    if {!$quiet} {
                        puts "      $filename ($size bytes)"
                    }
                }
            }
        }
    }
}

main

