#!/usr/bin/env tcl
##############################################################################
# Copyright 2011 Synopsys, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
##############################################################################

package forget oa

set path [file dirname [info script]]
set libpath [file normalize [file join $path .. ]]
lappend auto_path $libpath

package require oa

set path [file join [file dirname [info script]] oaUUTestLib]
exec rm -rf $path

proc check { testpoint expected actual } {
    if {[expr {$expected != $actual}]} {
        puts "'$testpoint' : expected $expected, got $actual"
        incr ::numFails 1
    }
}

set numFails 0

set lib [oa::oaLib_create "testLib" $path]
set tech [oa::oaTech_create $lib]
set viewType [oa::oaViewType_find "schematic"]
set dbuPerUU [$tech getDBUPerUU $viewType]

set vector [oa::oaVector 100 200]
set uuVector [oa::oaUUVector $vector $tech $viewType]
check "Constructor from vector x" [$tech dbuToUU $viewType 100] [$uuVector x]
check "Constructor from vector y" [$tech dbuToUU $viewType 200] [$uuVector y]

set uuVector [oa::oaUUVector 1.0 2.0 $tech $viewType]
check "Constructor from double x" 1.0 [$uuVector x]
check "Constructor from double y" 2.0 [$uuVector y]

set point [oa::oaPoint 200 300]
set uuPoint [oa::oaUUPoint $point $tech $viewType]
set uuVector [oa::oaUUVector $uuPoint $tech $viewType]
check "Constructor from point x" [$tech dbuToUU $viewType 200] [$uuVector x]
check "Constructor from point y" [$tech dbuToUU $viewType 300] [$uuVector y]

set uuPoint [oa::oaUUPoint 1.0 2.0 $tech $viewType]
set uuPoint2 [oa::oaUUPoint 2.0 4.0 $tech $viewType]
set uuVector [oa::oaUUVector $uuPoint $uuPoint2 $tech $viewType]
check "Constructor from points x" 1.0 [$uuVector x]
check "Constructor from points y" 2.0 [$uuVector y]

set uuVector [oa::oaUUVector $tech $viewType]
set uuPoint [oa::oaUUPoint 1.0 2.0 $tech $viewType]
set uuPoint2 [oa::oaUUPoint 4.0 6.0 $tech $viewType]
$uuVector set $uuPoint $uuPoint2
check "set x" 3.0 [$uuVector x]
check "set y" 4.0 [$uuVector y]

set uuVector [oa::oaUUVector $tech $viewType]
$uuVector setLength 5.0
check "getLength" 5.0 [$uuVector getLength]

set uuVector1 [oa::oaUUVector 1.0 1.0 $tech $viewType]
set uuVector2 [oa::oaUUVector 1.0 0.0 $tech $viewType]
check "leftOf false" 0 [$uuVector2 leftOf $uuVector1]
check "leftOf true" 1 [$uuVector1 leftOf $uuVector2]
check "rightOf false" 0 [$uuVector1 rightOf $uuVector2]
check "rightOf true" 1 [$uuVector2 rightOf $uuVector1]

set uuVector [oa::oaUUVector 1.0 0.0 $tech $viewType]
$uuVector rotate90
check "rotate90 x" 0.0 [$uuVector x]
check "rotate90 y" -1.0 [$uuVector y]

set uuVector [oa::oaUUVector 1.0 0.0 $tech $viewType]
$uuVector rotate180
check "rotate180 x" -1.0 [$uuVector x]
check "rotate180 y" 0.0 [$uuVector y]

set uuVector [oa::oaUUVector 1.0 0.0 $tech $viewType]
$uuVector rotate270
check "rotate270 x" 0.0 [$uuVector x]
check "rotate270 y" 1.0 [$uuVector y]

set uuVector [oa::oaUUVector 1.0 4.0 $tech $viewType]
set uuPoint [oa::oaUUPoint 2.0 3.0 $tech $viewType]
set uuPoint2 [oa::oaUUPoint $tech $viewType]
$uuVector addToPoint $uuPoint $uuPoint2
check "addToPoint x" 3.0 [$uuPoint2 x]
check "addToPoint y" 7.0 [$uuPoint2 y]

set uuVector [oa::oaUUVector 1.0 2.0 $tech $viewType]
set uuPoint [oa::oaUUPoint 3.0 4.0 $tech $viewType]
set uuPoint2 [oa::oaUUPoint $tech $viewType]
$uuVector subFromPoint $uuPoint $uuPoint2
check "subFromPoint x" 2.0 [$uuPoint2 x]
check "subFromPoint y" 2.0 [$uuPoint2 y]

set uuVector [oa::oaUUVector 2.0 2.0 $tech $viewType]
set uuVector2 [oa::oaUUVector -2.0 2.0 $tech $viewType]
set uuPoint [oa::oaUUPoint 0.0 0.0 $tech $viewType]
set uuPoint2 [oa::oaUUPoint 2.0 0.0 $tech $viewType]
set uuPoint3 [oa::oaUUPoint $tech $viewType]
check "intersects" 1 [$uuVector intersects $uuPoint $uuVector2 $uuPoint2 $uuPoint3]
check "intersects x" 1.0 [$uuPoint3 x]
check "intersects y" 1.0 [$uuPoint3 y]

set uuVector1 [oa::oaUUVector 2.0 3.0 $tech $viewType]
set uuVector2 [oa::oaUUVector 4.0 5.0 $tech $viewType]
check "operator*" [expr {(2.0*4.0) + (3.0*5.0)}] [$uuVector1 * $uuVector2]

set uuVector [oa::oaUUVector 2.0 2.0 $tech $viewType]
$uuVector normalize
check "normalize" 1.0 [expr {[$uuVector getLength] * [$tech getDBUPerUU $viewType]}]

set uuVector [oa::oaUUVector 2.0 0.0 $tech $viewType]
set uuVector2 [oa::oaUUVector -2.0 0.0 $tech $viewType]
check "getCosAngle" -1.0 [$uuVector getCosAngle $uuVector2]

set uuVector [oa::oaUUVector $tech $viewType]
check "getTech" $tech [$uuVector getTech]
check "getViewType" $viewType [$uuVector getViewType]

if {$numFails == 0} {
    puts "All tests passed."
} else {
    error "$numFails tests failed."
}

exec rm -rf $path

