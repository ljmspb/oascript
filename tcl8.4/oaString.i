/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%ignore OpenAccess_4::oaString;

%typemap(in) OpenAccess_4::oaString& (OpenAccess_4::oaString tmpString)
{
    /// oaString_1: %typemap(in) OpenAccess_4::oaString& (OpenAccess_4::oaString tmpString)
    $1 = &tmpString;
}

%typemap(argout) OpenAccess_4::oaString&
{
    /// oaString_2: %typemap(argout) OpenAccess_4::oaString& (OpenAccess_4::oaString tmpString)
    Tcl_SetVar(interp, Tcl_GetString($input), (const char*) *$1, 0);
}

%typemap(argout) const OpenAccess_4::oaString& {}

%typemap(in) const OpenAccess_4::oaString& (OpenAccess_4::oaString tmpString)
{
    /// oaString_3: %typemap(in) const OpenAccess_4::oaString& (OpenAccess_4::oaString tmpString)
    tmpString = Tcl_GetString($input);
    $1 = &tmpString;
}

%typemap(out) OpenAccess_4::oaString&
{
    /// oaString_4: %typemap(out) OpenAccess_4::oaString&
    Tcl_AppendElement(interp, (const char*) *$1);
}

%typemap(out) OpenAccess_4::oaString
{
    /// oaString_4: %typemap(out) OpenAccess_4::oaString
    Tcl_AppendElement(interp, (const char*) $1);
}

%typemap(typecheck) const OpenAccess_4::oaString& = char*;
%typemap(typecheck) OpenAccess_4::oaString& = char*;

%typemap(typecheck) const OpenAccess_4::oaString* = char*;
%typemap(typecheck) const OpenAccess_4::oaString* = char*;

