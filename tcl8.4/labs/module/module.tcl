#!/usr/bin/env tcl
################################################################################
#  Copyright {c} 2002-2010  Si2, Inc.  DUNS 62-191-1718
#
#  Licensed under the Apache License, Version 2.0 {the "License" you may not use
#  this file except in compliance with the License. You may obtain a copy of the
#  License at http:#www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed
#  under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#  CONDITIONS OF ANY KIND, either express or implied. See the License for the
#  specific language governing permissions and limitations under the License.
#
################################################################################
#  Version History
#
#   DATE      VERSION  BY               DESCRIPTION
#   04/15/11  10.0     scarver, Si2     Tutorial 10th Edition - Tcl version
#
################################################################################
#
#   Acknowledgments
#
#   The code herein uses syntax, concepts, appearance, style, formats, and
#   techniques, that appear in several copyrighted works without explicit
#   permisssion or attribution but in good faith within the limits of fair and
#   legal use to the extent known by the author[s]. Such works include,
#   - Classroom and Computer-Aided Instruction {CAI} projects from various
#     industries, including related pedagogical techniques, exercise and lab
#     formats, lesson-plans, and instructional models.
#   - K&R C
#   - Stroustroup C++
#   - ANSI C++
#   - C Library {UNIX man pages}
#   - The OpenAccess API and Reference Implementation and related materials
#   - The SWIG development tool and related documentation {www.swig.org}
#
#   The following members of the Scripting Languages Working Group, created the
#   initial version of the infrastructure and code to produce these lab code
#   examples based on the original Si2 OpenAccess Tutorial labs:
#
#     Rudy Albachten, Chair
#     James Masters
#     Christian Delbaere
#     Steve Potter
#     Stefan Zager
#     Carl Olson
#     Doug Keller
#     Koushik Kalyanaraman
#     Brandon Barclay
#
################################################################################


package require oa

oa::oaDesignInit

source ../emhlister/dumpUtils.tcl


#     This program creates a simple, hierarchical design: an adder out of
#     2 half-adders, each of which comprises an AND and XOR gate.
#                           _____           ____            _____
#     Leaf cells:       A--|     |      A--|    |       A--|     |
#     from netlist         | AND |--Y      | OR |--Y       | XOR |--Y
#     lab library       B--|_____|      B--|____|       B--|_____|
#
#
#     Embeded Modules created in this code:
#
#     Half-adder block:            Adder block:                           Co|
#         ____________________            __________________________________|__
#        |                    |          |                                  |  |
#        |          ______    |          |   _____       H1c         _____  |  |
#     A--|-----o---|      |   |       A--|--|     |-----------------|     | |  |
#        |     |   | AND1 |---|--C       |  | HA1 | H1s  _____  H2c | OR1 |-o  |
#        |  o------|______|   |       B--|--|_____|-----|     |-----|_____|    |
#        |  |  |              |          |              | HA2 |                |
#        |  |  |              |          |            o-|_____|----------------|--S
#        |  |  |    _____     |          |            |                        |
#        |  |  o---|      |   |          |            |_____________________   |
#        |  |      | XOR1 |---|--S       |__________________________________|__|
#     B--|--o------|______|   |                                             |
#        |                    |                                             |Ci
#        |____________________|
#
#
#     3-bit Top Module:
#
#               B0  A0           B1  A1           B2  A2
#                |  |             |  |             |  |
#        ________|__|_____________|__|_____________|__|______
#       |    ____|__|__       ____|__|__       ____|__|__    |
#       |   |    B  A  |     |    B  A  |     |    B  A  |   |
#       |   |          |     |          |     |          |   |
#       |   |   Add0   |     |   Add1   |     |   Add2   |   |
#       |   |          | C01 |          | C12 |          |   |
#  Ci---|---|Ci      Co|-----|Ci      Co|-----|Ci      Co|---|---Co
#       |   |___S______|     |___S______|     |___S______|   |
#       |_______|________________|________________|__________|
#               |                |                |
#               |                |                |
#               S0               S1               S2


namespace eval globals {
    variable mode_trunc "w"
    variable mode_read  "r"
    variable mode_edit  "a"

    variable visibleToBlock 1
}

proc newSection { id } {
    puts -nonewline "\n\n||||||||||||||||||||||||||||||||||||||||||| "
    puts -nonewline $id
    puts " |||||||||||||||||||||||||||||||||||||||||||\n\n"
}

proc assert { expression } {
    puts -nonewline "  ASSERT \["
    if { [uplevel 1 expr $expression] } {
        puts -nonewline "PASS"
    } else {
        puts -nonewline "FAIL"
    }
    puts "\] $expression"
}


proc assertion { assertion } {
    puts "ASSERTION: $assertion"
}


# ================================= SUBROUTINES  =============================


proc getMappedName { obj } {
    # Many of the getName functions allow you to enter
    #     $myObj getName
    # to get a simple string representing the Object's name.
    #
    # For those that don't, this function does the ScalarName setup and
    # retrieval, and returns the simple string.

    set ns [oa::oaNativeNS]
    set scalarName [oa::oaScalarName]
    set simpleString ""

    $obj getName $scalarName
    $scalarName get $ns simpleString
    return $simpleString
}


proc getLCVName { obj } {
    # Templatize a function that exploits the commonality of method names to
    # get the Lib/Cell/View name attributes from different types of containers.

    # Return a String that is the concatenation of the Lib/Cell/View names,
    # separated by the / character as a delimiter, using method[s] that are
    # are valid for all possible Inst types the template parameter might designate.
    return "[$obj getLibName]/[$obj getCellName]/[$obj getViewName]"
}


proc getNumTerms { module } {
    # Return the number of ModTerms in a Module.
    return [[$module getTerms] getCount]
}


proc getNumInstTerms { module } {
    # Return the number of ModInstTerms in a Module.
    return [[$module getInstTerms] getCount]
}


proc getNumNets { module } {
    # Return the number of ModNets in a Module
    return [[$module getNets] getCount]
}


proc createModInstTermByName { inst name net  } {
    # Templatize a function that exploits the commonality of method names to
    # get the master Module of an inst is the same for different ModInst
    # classes
    # Tcl note: a template is not needed here.

    # Set the mod variable to the master Module of the Inst argument.
    set mod [$inst getMasterModule]

    # A Block-specific Design will not have a master Module, so verify
    # mod exists before trying to use the handle to find its ModTerm ons.
    #
    set instTerm NULL

    if { $mod != "NULL" } {
        # Find the term with the name specified in the function argument.
        set term [oa::oaModTerm_find $mod [oa::oaName [oa::oaNativeNS] $name]]

        # Verify the handle is not null {ie, there was a Term by that name}.
        #
        if { [$term isValid] } {

            # Set the instTerm variable to a ModInstTerm created from the term handle.
            set instTerm [oa::oaModInstTerm_create  $net $inst $term]

            log "Created InstTerm of \"$name\" on Inst \"[$inst getName]\",\
                     attached to net \"[$net getName]\"\n"

            assert { [$instTerm isValid] }

        } else {
            puts "***Couldn't find term named '$name' on mod. Quitting."
            exit 2
        }
    }
    return $instTerm
}


proc expectModTermOnNet { term net } {
    # Create an oaCollection named collTermsOnNet consisting of the
    # ModTerms on the ModNet passed as a function argument.
    set collTermsOnNet [$net getTerms]

    assert { [$collTermsOnNet getCount]  == 1 }

    # The only Term in the collection should be the one passed in as an arg.
    oa::foreach gotTerm $collTermsOnNet {
        assert { [string equal $gotTerm $term] }
    }
}


proc createModNet { module netName } {
      # Create a ModScalarNet such that the following assertions pass and
      # a corresponding Net is automatically created in the Block Domain.
      set net [oa::oaModScalarNet_create $module $netName]

      assert { [ $net isValid] }
      assert { [string equal [$net getModule] $module] }
      assert { [string equal [$net getName]   $netName] }
      assert { [string equal [[$net getType] getName] "ModScalarNet"] }
      assert { [string equal [[$net getSigType] getName] "signal"] }
      assert { ![ $net isGlobal] }
      assert { ![ $net isImplicit] }
      assert { [ $net isEmpty] }
      assert { [[$net getEquivalentNets] isEmpty] }
      assert { [[$net getInstTerms] isEmpty] }
      assert { [[$net getTerms] isEmpty] }

      log "Created ModScalarNet '$netName'\n"
      return $net
}


proc addModNetTerm { module name ioDir } {
    # Use the createModNet helper function defined above to create a ModScalarNet
    # using the passed arguments. Then create a ModScalarTerm (with the proper
    # OA API function) with attributes specified by the passed arguments, so that
    # appropriate Objects are propagated to the Block Domain and the assertions pass.
    set net  [createModNet $module $name]
    set term [oa::oaModScalarTerm_create $net $name $ioDir]

    assert { [string equal [$net  getModule] $module] }
    assert { [string equal [[$net getType] getName] "ModScalarNet"] }
    assert { [string equal [[$net  getSigType] getName] "signal"] }
    assert { ![$net  isGlobal] }
    assert { [string equal [$term  getModule] $module] }
    assert { [string equal [[$term getType] getName] "ModScalarTerm"] }
    assert { [[$term  getTermType] typeEnum]  == $ioDir }
    assert { [string equal [$term  getNet] $net] }

    log "Term '[$term getName]' created attached to\
        Net '[[$term getNet] getName]'\n"

    expectModTermOnNet $term $net
    return $net
}


proc createModInst { Type module master instName } {
    # Templatize a function that exploits the commonality of method names to
    # create Insts of different types.

    # Tcl note: this C++ functionaility is mimicked here using the "Type" arg.
    # If Type is "oaModScalarInst", then the createFunction, below, computes
    # to "oa::oaModScalarInst_create"

    set create _create
    set createFunction oa::$Type$create

    # Create an Inst using the function arguments to specify its master
    # and its owner Module. Assign to the inst variable.

    set inst [$createFunction  $module $master $instName]

    assert { [$inst isValid] }
    log "Created [[$inst getType] getName] '[$inst getName]'\n"

    return $inst
}


proc createHalfAdder { design cellLibName designLibName view } {

    # Create the "HalfAdder" Module in the specified design Lib and View
    # (passed as args), assigning it to the modHA variable.
    set modHA [oa::oaModule_create $design "HalfAdderMod"]

    # Open the "Xor" and "And" Designs from the cell Lib and View
    # (passed as args) in READ mode.
    set designXor [oa::oaDesign_open $cellLibName "Xor" $view $globals::mode_read]
    set designAnd [oa::oaDesign_open $cellLibName "And" $view $globals::mode_read]

    if { [catch {
        # Use the createModInst helper function defined in this lab to attempt to
        # create a ModModuleInst in modHA of the top Module of the Xor Design.
        createModInst oaModModuleScalarInst $modHA [$designXor getTopModule] "Xor1q"
    } err ] } {
        if { [string first $oa::oacModulesNotInSameDesign $err] != -1 } {
            puts "\{As expected, cannot instantiate Modules from other Designs\}"
        }
    }

    # Use the createModInst helper function to create ModInsts named "Xor1"
    # and "And1" of  master Designs Xor and And to be owned/contained by modHA.
    set instXor [createModInst oaModScalarInst $modHA $designXor "Xor1"]
    set instAnd [createModInst oaModScalarInst $modHA $designAnd "And1"]

    log "Creating Design 'HA'.\n"
    add_indent
    assert { [$modHA isValid] }

    set netA [addModNetTerm $modHA "A" $oa::oacInputTermType ]
    set netB [addModNetTerm $modHA "B" $oa::oacInputTermType ]
    set netC [addModNetTerm $modHA "C" $oa::oacOutputTermType]
    set netS [addModNetTerm $modHA "S" $oa::oacOutputTermType]

    assert { [getNumNets  $modHA] == 4 }
    assert { [getNumTerms $modHA] == 4 }

    # Call createModInstTermByName, defined above to create ModInstTerms on
    # instXor named "A", "B", "Y", connected to netA, netB, NetS, respectively.
    set instTermXorA [createModInstTermByName $instXor "A" $netA]
    set instTermXorB [createModInstTermByName $instXor "B" $netB]
    set instTermXorY [createModInstTermByName $instXor "Y" $netS]

    # Call createModInstTermByName, defined above, to create ModInstTerms on
    # instAnd named "A", "B", "Y", connected to netA, netB, NetC, respectively.
    set instTermAndA [createModInstTermByName $instAnd "A" $netA]
    set instTermAndB [createModInstTermByName $instAnd "B" $netB]
    set instTermAndY [createModInstTermByName $instAnd "Y" $netC]

    assert { [getNumInstTerms $modHA] == 6 }
    sub_indent
    return $modHA
}

# ---------- Create Design with Modules and Blocks


proc createDesign { designLibName cell view } {
    # Create a Design with the names passed as arguments, maskLayout ViewType,
    # overwriting any Design of those names/type that might already exist.
    set vt [oa::oaViewType_get [oa::oaReservedViewType "maskLayout"]]

    set design [oa::oaDesign_open \
      [oa::oaScalarName [oa::oaNativeNS] $designLibName] \
      $cell $view $vt $globals::mode_trunc]

    assert { [$design isValid] }
    assert { [$design getRefCount] == 1 }
    assert { [ string equal [[$design getType] getName] "Design"] }

    assert { [string equal [[$design getViewType] getName] "maskLayout"] }

    # Assign design a "block" CellType attribute value.
    $design setCellType $oa::oacBlockCellType

    assert { [[$design getCellType] typeEnum] == $oa::oacBlockCellType }
    return $design
}


proc createFullAdder { design cellLibName designLibName view } {
    # Open the existing design named "Or" in the CellLib in read mode.
    set designOr [oa::oaDesign_open $cellLibName "Or" $view $globals::mode_read]

    # Create an embedded modFA FullAdder module owned by the Design passed as an argument.
    set modFA [oa::oaModule_create $design "FullAdderMod"]

    # Call the CreateHalfAdder utility function to create the modHA module, also owned by
    # the design, passing along the mapped names of the cell and design libraries, and View.
    set modHA [createHalfAdder $design $cellLibName $designLibName $view]

    # Call the CreateModInst utility function defined above to create in
    # the modFA parent an Inst named "Ha1" and one named "Ha2 of modHA.
    set instHA1 [createModInst oaModModuleScalarInst $modFA $modHA "Ha1"]
    set instHA2 [createModInst oaModModuleScalarInst $modFA $modHA "Ha2"]

    # Call the CreateModInst{} utility function defined above to create in
    # the modFA parent an Inst named "Or1" of designOr.
    set instOr1 [createModInst oaModScalarInst $modFA $designOr "Or1"]

    #  Create nets and terminals for each I/O on this block.
    #
    set netA  [addModNetTerm $modFA "A"  $oa::oacInputTermType ]
    set netB  [addModNetTerm $modFA "B"  $oa::oacInputTermType ]
    set netCi [addModNetTerm $modFA "Ci" $oa::oacInputTermType ]
    set netCo [addModNetTerm $modFA "Co" $oa::oacOutputTermType]
    set netS  [addModNetTerm $modFA "S"  $oa::oacOutputTermType]
      #
      # Create internal nets
      #
    set netH1c [createModNet $modFA "H1c"]
    set netH1s [createModNet $modFA "H1s"]
    set netH2c [createModNet $modFA "H2c"]
    assert { [getNumTerms $modFA] == 5 }

    add_indent

    # Create an iterator that collects all the ModuleInsts in modFA

    oa::foreach modInst [$modFA getInsts] {
        puts "  modInst '[$modInst getName]'\
            type=[[$modInst getType] getName]\
            InstHeader type=[[[$modInst getHeader] getType] getName]"
    }

  # Create an iterator that collects all the ModuleInstHeaders in modFA

    oa::foreach mmiHeader [$modFA getModuleInstHeaders] {
        puts "     modModuleInstHeader [getMappedName $mmiHeader]"

        # Create an iterator that collects all the Insts in the Header
        oa::foreach modInst [$mmiHeader getInsts] {
            puts "       modInst '[$modInst getName]'"
      }
    }

  # Create ModInstTerms on the HalfAdder Insts.

    createModInstTermByName $instHA1 "A" $netA
    createModInstTermByName $instHA1 "B" $netB
    createModInstTermByName $instHA1 "C" $netH1c
    createModInstTermByName $instHA1 "S" $netH1s

    createModInstTermByName $instHA2 "A" $netH1s
    createModInstTermByName $instHA2 "B" $netCi
    createModInstTermByName $instHA2 "C" $netH2c
    createModInstTermByName $instHA2 "S" $netS

    # Create ModInstTerms on the Or Insts.

    createModInstTermByName $instOr1 "A" $netH1c
    createModInstTermByName $instOr1 "B" $netH2c
    createModInstTermByName $instOr1 "Y" $netCo

    sub_indent
    return $modFA
}


proc createAdder3bit { cellLibName designLibName view } {
    # Create the top level 3-bit adder Design using the arguments passed in.
    #
    set design3bit [createDesign $designLibName "Adder3bit" $view]

    # Create a Module named "Adder3bitMod" in the newly created Design.
    set mod3bit [oa::oaModule_create $design3bit "Adder3bitMod"]

    # Set the module created as the topModule BEFORE creating more hierarchy
    # (or it won't propagate to Block domain).
    $design3bit setTopModule $mod3bit $globals::visibleToBlock

    # Create modFA, a FullAdder Module in the 3bit design.
     #
    set modFA [createFullAdder $design3bit $cellLibName $designLibName $view]

    # Use the CreateModInst helper function to create inside design3bit's top Module,
    # three ModInsts of the modFA FullAdder Module just created above. Name them "Fa0",
    # "Fa1", and "Fa2". {Use the schematic drawing at the top of this file as a guide.}
    set mmiAdder0 [createModInst oaModModuleScalarInst $mod3bit $modFA "Fa0"]
    set mmiAdder1 [createModInst oaModModuleScalarInst $mod3bit $modFA "Fa1"]
    set mmiAdder2 [createModInst oaModModuleScalarInst $mod3bit $modFA "Fa2"]

    # Create in the top module of design3bit each of the ModScalarNets declared above
    # by calling the addModNetTerm helper function defined above, passing in as
    # the name the last two characters of the corresponding Net name.
    # For example netA0 {and the Term created for it by the helper}
    # should be named "A0", and so on.  All "S*" and "Co" Terms should
    # be Output types, with the "A*", "B*", and "Ci" Terns as Input types.
    # {Use the schematic drawing at the top of this file as a guide.}
    set netA0 [addModNetTerm $mod3bit "A0" $oa::oacInputTermType ]
    set netB0 [addModNetTerm $mod3bit "B0" $oa::oacInputTermType ]
    set netS0 [addModNetTerm $mod3bit "S0" $oa::oacOutputTermType]
    set netA1 [addModNetTerm $mod3bit "A1" $oa::oacInputTermType ]
    set netB1 [addModNetTerm $mod3bit "B1" $oa::oacInputTermType ]
    set netS1 [addModNetTerm $mod3bit "S1" $oa::oacOutputTermType]
    set netA2 [addModNetTerm $mod3bit "A2" $oa::oacInputTermType ]
    set netB2 [addModNetTerm $mod3bit "B2" $oa::oacInputTermType ]
    set netS2 [addModNetTerm $mod3bit "S2" $oa::oacOutputTermType]
    set netA3 [addModNetTerm $mod3bit "A3" $oa::oacInputTermType ]
    set netB3 [addModNetTerm $mod3bit "B3" $oa::oacInputTermType ]
    set netS3 [addModNetTerm $mod3bit "S3" $oa::oacOutputTermType]
    set netCi [addModNetTerm $mod3bit "Ci" $oa::oacInputTermType ]
    set netCo [addModNetTerm $mod3bit "Co" $oa::oacOutputTermType]

    # Create in the top module of design3bit two ModScalarNets declared above
    # by calling the CreateModNet{} helper function defined above, passing in
    # as the names "C01" and "C02", respectively.
    set netC01 [createModNet $mod3bit "C01"]
    set netC12 [createModNet $mod3bit "C12"]

    # Call the CreateModInstTermByName{} helper function defined above
    # to create ModInstTerms on each of the three FullAdder Insts for all
    # master Terms, connecting each to the corresponding Net created above.
    # {Use the schematicdrawing at the top of this file as a guide.}
    #
    set itAdd0A  [createModInstTermByName $mmiAdder0 "A"  $netA0 ]
    set itAdd0B  [createModInstTermByName $mmiAdder0 "B"  $netB0 ]
    set itAdd0S  [createModInstTermByName $mmiAdder0 "S"  $netS0 ]
    set itAdd0Ci [createModInstTermByName $mmiAdder0 "Ci" $netCi ]
    set itAdd0Co [createModInstTermByName $mmiAdder0 "Co" $netC01]
    set itAdd1A  [createModInstTermByName $mmiAdder1 "A"  $netA1 ]
    set itAdd1B  [createModInstTermByName $mmiAdder1 "B"  $netB1 ]
    set itAdd1S  [createModInstTermByName $mmiAdder1 "S"  $netS1 ]
    set itAdd1Ci [createModInstTermByName $mmiAdder1 "Ci" $netC01]
    set itAdd1Co [createModInstTermByName $mmiAdder1 "Co" $netC12]
    set itAdd2A  [createModInstTermByName $mmiAdder2 "A"  $netA2 ]
    set itAdd2B  [createModInstTermByName $mmiAdder2 "B"  $netB2 ]
    set itAdd2S  [createModInstTermByName $mmiAdder2 "S"  $netS2 ]
    set itAdd2Ci [createModInstTermByName $mmiAdder2 "Ci" $netC12]
    set itAdd2Co [createModInstTermByName $mmiAdder2 "Co" $netCo ]

    return $design3bit
}


# ================================  M A I N  ================================

proc main {} {
    set libNetName  "Lib"
    set libModName  "Lib3bitAdder"
    set cellName    "Lib"
    set viewName    "netlist"
    set libModDir    "Lib3bit"
    set libDefsFile "[pwd]/lib.defs"


    # Perform logical lib name to physical path name mappings for the source
    # library created in the netlist/ lab, then the symbol library to be
    # created in this lab.
    set strLibDefDef [oa::oaLibDefList_getDefaultPath]

    if { [file normalize $strLibDefDef] != [file normalize $libDefsFile] } {
        puts "***Missing local $libDefsFile file."
        puts "   Please create one with the one line shown below: "
        puts "       INCLUDE ../netlist/lib.defs"
        exit 4
    }
    oa::oaLibDefList_openLibs

    set libNet [oa::oaLib_find $libNetName ]

    if { $libNet == "NULL" } {
        puts  "***Either lib.defs file missing or netlist lab was not completed."
        exit 1
    }


    # Create (or open if this lab is being rerun) the output 3bit Module
    # library.
    #
    set libMod [oa::oaLib_find $libModName]

    if { $libMod != "NULL" } {
        puts "LibMod opened via openLibs of $strLibDefDef"
    } else {
        if { [oa::oaLib_exists $libModDir] } {
            #
            # Directory there but DEFINE for it missing in lib.defs.
            #
            set libMod [oa::oaLib_open $libModName $libModDir]
            puts "Opened existing $libModName in directory $libModDir"
        } else {
            # No directory for LibMod.
            #
            set libMod [oa::oaLib_create $libModName $libModDir \
              $oa::oacSharedLibMode "oaDMFileSys"]
            puts "Created new $libModName in directory $libModDir"
        }
        # Add a LibDef for this new Module Lib to the lib.defs file
        #

        set ldl [oa::oaLibDefList_get $strLibDefDef "a"]

        puts "\nAppending new Lib def for [$libMod getName]->$libModDir\
                to LibDefs file = $strLibDefDef"

        oa::oaLibDef_create $ldl [$libMod getName] $libModDir
        $ldl save
    }

    # Pretend to "import" Verilog descriptions of design modules
    #
    #    module Adder3bit(Co, S0,S1,S2,S3, A0,A1,A2,A3, B0,B1,B2,B3, Ci);
    #      input A0,A1,A2,A3, B0,B1,B2,B3, Ci;
    #      output Co, S0,S1,S2,S3;
    #      FA Add0 (Co,S,A,B,Ci);
    #      FA Add1 (Co,S,A,B,Ci);
    #      FA Add2 (Co,S,A,B,Ci);
    #      FA Add3 (Co,S,A,B,Ci);
    #    endmodule
    #    module FullAdder(Co,S,A,B,Ci);
    #      input A,B,Ci;
    #      output Co,S;
    #      HA Ha1 (C,S,A,B);
    #      HA Ha2 (C,S,A,B);
    #      Or Or1 (Y,A,B);
    #    endmodule
    #    module HalfAdder(C,S,A,B);
    #      input A,B;
    #      output C,S;
    #      And And1 (Y,A,B);
    #      Xor Xor1 (Y,A,B);
    #    endmodule

    # Use the CreateAdder3bit function defined in this lab to
    # create design3bit using the mapped names of the:
    #    - cell library (from the netlist lab) containing the leaf OR, AND, XOR gates
    #    - design library to hold the 3-bit Adder begin created in this lab
    #    - View name to be used for all the design containers created.
    set design3bit [createAdder3bit $libNetName $libModName $viewName]

    # Save the design at this initial stage BEFORE any partitioning or
    # clustering is done to it.
    $design3bit save

    set emhGlobals::dumpOccDomain false
    dumpOpenDesigns

    newSection "Detach FullAdderMod"

    # Locate the full-adder module by name in the Design
    set modFA [oa::oaModule_find $design3bit "FullAdderMod"]
    assert { [$modFA isValid] }

    # Detach the full-adder Module hierarchy from Design 3bitAdder creating a new
    # Design in the same Lib, with the same View name, but a Cell name of "FAdetach".
    set desFAdet [$modFA detach $libModName "FAdetach" $viewName]

    dumpOpenDesigns

    newSection "Embed Or"

    # Embed the top Module of the Or Design into the new FADetach Design.
    oa::oaModule_embed $desFAdet \
           [oa::oaDesign_open $libNetName "Or" $viewName $globals::mode_read]

    dumpOpenDesigns

    $design3bit close
    $desFAdet close

    puts "\n...........Normal Termination........\n"

  return 0
}

main
