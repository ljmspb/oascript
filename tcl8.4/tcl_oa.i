/* -*- mode: c++ -*- */
/*
   Copyright 2009 Advanced Micro Devices, Inc.
   Copyright 2010 Synopsys, Inc.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

%{
bool lang_stringp(Tcl_Interp* interp, Tcl_Obj* input) {
    void* tmpVoidPtr;
    return SWIG_ConvertPtr(input, &tmpVoidPtr, NULL, 0) != SWIG_OK;
}
%}

%header %{
#define LANG_NULLP(obj) (obj == NULL)
#define LANG_STRINGP(input) lang_stringp(interp, input)
#define LANG_VALUE_TYPE Tcl_Obj*
#define LANG_GET_DEF_NS Tcl_GetVar2Ex(interp, "::oa::oaNameSpace::default", 0, 0)
#define LANG_OA_TRY(action) \
try { \
    action; \
} catch (const OpenAccess_4::oaException& e) { \
    SWIG_exception_fail(SWIG_RuntimeError, e.getMsg()); \
}
#define LANG_STR_TO_CSTR(langstr) Tcl_GetString(langstr)
#define LANG_APPEND_OUTPUT(result, obj) Tcl_ListObjAppendElement(interp, result, obj)

#define LANG_THROW_OAEXCP(e) char* msg = (char*)Tcl_Alloc(strlen((const oaChar*) e.getMsg()) + 64); \
  sprintf(msg, "OpenAccessException (%d) : %s", e.getMsgId(), (const oaChar*) e.getMsg()); \
  Tcl_SetResult(interp, msg, TCL_DYNAMIC); \
  return TCL_ERROR
  

#include <munged_headers/oaAppDefProxy.h>
%}

/* Catch exceptions, or else they are segfaults! */
%exception
{
    try {
        $action;
    } catch (OpenAccess_4::oaException& e) {
        LANG_THROW_OAEXCP(e);
    }
}

/* Useless in tcl */
%ignore OpenAccess_4::oaSwap;
%ignore OpenAccess_4::oaXYTreeRec::operator new[];
%ignore OpenAccess_4::oaXYTreeRec::operator delete[];
%ignore OpenAccess_4::oaXYTreeRec::operator new[];
%ignore OpenAccess_4::oaXYTreeNode::operator delete[];
%ignore OpenAccess_4::oaXYTreeNode::operator new[];
%ignore OpenAccess_4::operator+;
%ignore OpenAccess_4::operator-;
%ignore OpenAccess_4::operator*;
%ignore OpenAccess_4::operator/;

/* These classes have typemaps for ease-of-use */
%include "oaString.i"
%include "oaPoint.i"
%include "oaBox.i"
%include "oaEnumWrapper.i"
%include <generic/typemaps/oaNames.i>
%typemap(typecheck) const OpenAccess_4::oaByte& = char*;
%typemap(typecheck) OpenAccess_4::oaByte& = char*;
%typemap(typecheck) const OpenAccess_4::oaByte* = char*;
%typemap(typecheck) OpenAccess_4::oaByte* = char*;


/* Ignore forward declarations of these classes in oaArray.h */
%ignore OpenAccess_4::oaFeatureArray;
%ignore OpenAccess_4::oaManagedTypeArray;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaPoint>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaBox>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaString>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaFeature*>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaUInt4>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaObject*>;
%ignore OpenAccess_4::oaArray<OpenAccess_4::oaArray<OpenAccess_4::oaUInt4> >;

%ignore OpenAccess_4::oaDesignInit;

%include "oaArray.i"

#undef COLLCLASS
%define COLLCLASS(ns1, paramtype1, ns2, paramtype2)

%feature("valuewrapper") OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2>;

%template(oaCollection_##paramtype1##_##paramtype2##) OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2>;

%extend OpenAccess_4::oaCollection<ns1::paramtype1, ns2::paramtype2> {
    // Create an iterator
    OpenAccess_4::oaIter<ns1::paramtype1> createIter() const {
        return OpenAccess_4::oaIter<ns1::paramtype1>(*self);
    }
}
%enddef

