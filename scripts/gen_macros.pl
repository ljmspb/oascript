#!/usr/bin/perl

use strict;
use warnings;

use Carp;
use File::Basename;
use File::Find;
use FindBin;
use Getopt::Long qw(:config pass_through);

use lib $FindBin::RealBin;
use TU::Parser;

our @modules = qw(common base plugIn dm tech design util wafer);

my %opts;
GetOptions(\%opts,
	   "f=s",
	   "enums=s",
	   "arrays=s",
	   "collections=s",
	   "types=s",
	   "objects=s",
	   "oasrc=s",
	   "lookuptables=s",
	   "namespaces=s",
	   "oaroot=s",
           "yaml=s",
           "sqlite=s",
	   "d"
    );

my $infile;
if (exists($opts{f})) {
    $infile = $opts{f};
} else {
    $infile = shift(@ARGV) or die "Usage: $0 <.tu file>";
}

my $oasrc;

if (exists($opts{oasrc})) {
    $oasrc = $opts{oasrc};
} elsif (exists($ENV{OAROOT})) {
    $oasrc = $ENV{OAROOT};
} else {
    croak "No OAROOT env var, and no -oasrc argument provided.  Abandon ship!";
}

my $header_map = collateHeaders($oasrc);
croak "Error: Looks like you have a non-standard directory structure in your OA installation." unless scalar(@$header_map);

my $tu = new TU::Parser($infile);

if (exists($opts{arrays})) {
    dump_arrays($tu, $header_map, $opts{arrays});
}

if (exists($opts{collections})) {
    dump_collections($tu, $header_map, $opts{collections});
}

if (exists($opts{enums})) {
    dump_enums($tu, $header_map, $opts{enums});
}

if (exists($opts{types})) {
    dump_types($tu, $header_map, $opts{types});
}

if (exists($opts{objects})) {
    dump_objects($tu, $header_map, $opts{objects});
}

if (exists($opts{lookuptables})) {
    dump_lookuptables($tu, $header_map, $opts{lookuptables});
}

if (exists($opts{namespaces})) {
    dump_namespaces($tu, $header_map, $opts{namespaces});
}

if (exists($opts{yaml})) {
    dump_yaml($tu, $header_map, $opts{yaml});
}

if (exists($opts{sqlite})) {
    dump_sqlite($tu, $header_map, $opts{sqlite});
}

exit 0;

#-------------------------------------------------------------------------------

sub get_type_name {
    my ($tu, $type_decl) = @_;

    my $type_id = $tu->getRef($type_decl, 'name', 'identifier_node') || return (undef, undef);
    my $name = $type_id->{strg} || $type_id->{note};
    my $ns_decl = $tu->getRef($type_decl, 'scpe', 'namespace_decl') || return (undef, $name);
    my $ns_id = $tu->getRef($ns_decl, 'name', 'identifier_node') || return (undef, $name);
    my $ns = $ns_id->{strg};
    return ($ns, $name);
}

sub dump_arrays {
    my ($tu, $header_map, $outfile) = @_;

    my $fh;
    if (!open($fh, "> $outfile")) {
	carp "Couldn't open $outfile for write.";
	return;
    }

    my %integer_types;

    for my $node ($tu->getNodesByType(TU::INTEGER_TYPE)) {
	my $name = $node->getName();
	$integer_types{$name}++ if $name;
    }

    my %template_id_nodes;

    for my $node ($tu->getNodesByType(TU::TEMPLATE_DECL)) {
	my $name_node = $node->getRef('name');
	next unless ($name_node && $name_node->getType() == TU::IDENTIFIER_NODE);
	$template_id_nodes{$name_node->getNum()}++;
    }

    my %array;
    my %array_ext;
    my %ptr_array;
    my %ptr_array_ext;
    my %int_array;
    my %int_array_ext;
    my %string_array_ext;
    my %point_array_ext;
    my %box_array_ext;

    my %subset;
    my %subset_ext;
    my %ptr_subset;
    my %ptr_subset_ext;
    my %int_subset;
    my %int_subset_ext;
    my %string_subset_ext;
    my %point_subset_ext;
    my %box_subset_ext;

    my $stringarray = {};
    my $pointarray = {};
    my $boxarray = {};

    my $stringsubset = {};
    my $pointsubset = {};
    my $boxsubset = {};
    
    for my $type_node ($tu->getNodesByType(TU::TYPE_DECL)) {
	my $name_node = $type_node->getRef('name');
	next unless ($name_node && $name_node->getType() == TU::IDENTIFIER_NODE);
	next if exists($template_id_nodes{$name_node->getNum()});
	my $type_ns = $type_node->getScope();
	my $type_name = $name_node->getName();
	next unless ($type_ns and $type_name);
	my $src = $type_node->getField('srcp') || "";

	if ($type_name =~ /oaArray<(.*)\*>/) {
	    my $param_type = $1;
	    my ($ns, $base) = split(/::/, $param_type, 2);
	    next unless ($ns and $base);
	    next if ($base eq 'BaseArg' || $base =~ /[\s,<>]/);
	    next if exists($ptr_array{$ns}->{$base});
	    $ptr_array{$ns}->{$base} = $src;
	    next;
	} elsif ($type_name =~ /oaArray<(.*)>/) {
	    my $param_type = $1;
	    my ($ns, $base) = split(/::/, $param_type, 2);
	    next unless ($base);
	    if ($base eq 'oaString') {
		$stringarray->{$src}++;
		next;
	    }
	    if ($base eq 'oaPoint') {
		$pointarray->{$src}++;
		next;
	    }
	    if ($base eq 'oaBox') {
		$boxarray->{$src}++;
		next;
	    }
	    next if ($base eq 'BaseArg' || $base =~ /[\s,<>]/);
	    next if exists($array{$ns}->{$base});
	    $array{$ns}->{$base} = $src;
	    next;
	} elsif ($type_name =~ /oaSubset<(.*)\*>/) {
	    my $param_type = $1;
	    my ($ns, $base) = split(/::/, $param_type, 2);
	    next unless ($ns and $base);
	    next if ($base eq 'BaseArg' || $base =~ /[\s,<>]/);
	    next if exists($ptr_subset{$ns}->{$base});
	    $ptr_subset{$ns}->{$base} = $src;
	    next;
	} elsif ($type_name =~ /oaSubset<(.*)>/) {
	    my $param_type = $1;
	    my ($ns, $base) = split(/::/, $param_type, 2);
	    next unless ($base);
	    next if ($base eq 'BaseArg' || $base =~ /[\s,<>]/);
	    if ($base eq 'oaString') {
		$stringsubset->{$src}++;
		next;
	    }
	    if ($base eq 'oaPoint') {
		$pointsubset->{$src}++;
		next;
	    }
	    if ($base eq 'oaBox') {
		$boxsubset->{$src}++;
		next;
	    }
	    next if exists($subset{$ns}->{$base});
	    $subset{$ns}->{$base} = $src;
	    next;
	}

	my $record = $type_node->getRef('type');
	next unless ($record && $record->getType() == TU::RECORD_TYPE);
	my $bases = $record->getField('base');
	next unless ($bases && ref($bases) eq 'ARRAY');
	for my $base_entry (@$bases) {
	    my $base = $tu->getNode($base_entry->[0]);
	    next unless ($base && $base->getType() == TU::RECORD_TYPE);
	    my $base_type = $base->getName();
	    next unless $base_type;
	    
	    if ($base_type =~ /oaArray<(.*)\*>/) {
		my $param_type = $1;
		my ($param_ns, $param_base) = split(/::/, $param_type, 2);
		next unless ($param_ns && $param_base);
		next if ($param_base =~ /[\s,<>]/);
		$ptr_array{$param_ns}->{$param_base} = $src unless exists($ptr_array{$param_ns}->{$param_base});
		$ptr_array_ext{$type_ns}->{$type_name} = [ $src, $param_ns, $param_base ] unless exists($ptr_array_ext{$type_ns}->{$type_name});
		next;
	    } elsif ($base_type =~ /oaArray<(.*)>/) {
		my $param_type = $1;
		my ($param_ns, $param_base) = split(/::/, $param_type, 2);
		if (!$param_base) {
		    $param_base = $param_ns;
		    undef $param_ns;
		}
		next if ($param_base =~ /[,<>]/);
		if ($param_base eq 'oaString') {
		    $string_array_ext{$type_ns}->{$type_name} = $src unless exists($string_array_ext{$type_ns}->{$type_name});
		} elsif ($param_base eq 'oaPoint') {
		    $point_array_ext{$type_ns}->{$type_name} = $src unless exists($point_array_ext{$type_ns}->{$type_name});
		} elsif ($param_base eq 'oaBox') {
		    $box_array_ext{$type_ns}->{$type_name} = $src unless exists($box_array_ext{$type_ns}->{$type_name});
		} elsif (exists($integer_types{$param_type})) {
		    $int_array{$param_base} = $src unless exists($int_array{$param_base});
		    $int_array_ext{$type_ns}->{$type_name} = [ $src, $param_base ] unless exists($int_array_ext{$type_ns}->{$type_name});
		} else {
                    my $class = $type_node->getRef('scpe');
                    if ($class and $class->getType() == TU::RECORD_TYPE) {
                        my $classname = $class->getName();
                        my $classns = $class->getScope() || "";
                        next if $classns eq 'std';
                    }

		    $array{$param_ns}->{$param_base} = $src unless (!$param_ns || exists($array{$param_ns}->{$param_base}));
		    $array_ext{$type_ns}->{$type_name} = [ $src, $param_ns, $param_base ] unless exists($array_ext{$type_ns}->{$type_name});
		}
		next;
	    } elsif ($base_type =~ /oaSubset<(.*)\*>/) {
		my $param_type = $1;
		my ($param_ns, $param_base) = split(/::/, $param_type, 2);
		next unless ($param_ns && $param_base);
		next if ($param_base =~ /[\s,<>]/);
		$ptr_subset{$param_ns}->{$param_base} = $src unless exists($ptr_subset{$param_ns}->{$param_base});
		$ptr_subset_ext{$type_ns}->{$type_name} = [ $src, $param_ns, $param_base ] unless exists($ptr_subset_ext{$type_ns}->{$type_name});
		next;
	    } elsif ($base_type =~ /oaSubset<(.*)>/) {
		my $param_type = $1;
		my ($param_ns, $param_base) = split(/::/, $param_type, 2);
		if (!$param_base) {
		    $param_base = $param_ns;
		    undef $param_ns;
		}
		next if ($param_base =~ /[,<>]/);
		if ($param_base eq 'oaString') {
		    $string_subset_ext{$type_ns}->{$type_name} = $src unless exists($string_subset_ext{$type_ns}->{$type_name});
		} elsif ($param_base eq 'oaPoint') {
		    $point_subset_ext{$type_ns}->{$type_name} = $src unless exists($point_subset_ext{$type_ns}->{$type_name});
		} elsif ($param_base eq 'oaBox') {
		    $box_subset_ext{$type_ns}->{$type_name} = $src unless exists($box_subset_ext{$type_ns}->{$type_name});
		} elsif (exists($integer_types{$param_type})) {
		    $int_subset{$param_base} = $src unless exists($int_subset{$param_base});
		    $int_subset_ext{$type_ns}->{$type_name} = [ $src, $param_base ] unless exists($int_subset_ext{$type_ns}->{$type_name});
		} else {
		    $subset{$param_ns}->{$param_base} = $src unless (!$param_ns || exists($subset{$param_ns}->{$param_base}));
		    $subset_ext{$type_ns}->{$type_name} = [ $src, $param_ns, $param_base ] unless exists($subset_ext{$type_ns}->{$type_name});
		}
		next;
	    }
	}
    }

    print $fh ("STRINGARRAY\n");
    print $fh ("POINTARRAY\n");
    print $fh ("BOXARRAY\n");

    for my $type (sort keys %int_array) {
	my $safe_type = join('_', split(/\s+/, $type));
	printf $fh ("%-60s // %s\n", "INTARRAY(${safe_type},${type})", $int_array{$type});
    }

    for my $ns (sort keys %array) {
	for my $type (sort keys %{$array{$ns}}) {
	    printf $fh ("%-60s // %s\n", "ARRAYCLASS(${ns},${type})", $array{$ns}->{$type});
	}
    }

    for my $ns (sort keys %ptr_array) {
	for my $type (sort keys %{$ptr_array{$ns}}) {
	    printf $fh ("%-60s // %s\n", "PTRARRAYCLASS(${ns},${type})", $ptr_array{$ns}->{$type});
	}
    }

    for my $ns (sort keys %array_ext) {
	for my $type (sort keys %{$array_ext{$ns}}) {
	    my $fields = $array_ext{$ns}->{$type};
	    my $src = shift(@$fields);
	    my $strg = "ARRAYEXT(${ns},${type}," . join("::", @$fields) . ")";
	    printf $fh ("%-60s // %s\n", $strg, $src);
	}
    }

    for my $ns (sort keys %ptr_array_ext) {
	for my $type (sort keys %{$ptr_array_ext{$ns}}) {
	    my $fields = $ptr_array_ext{$ns}->{$type};
	    my $src = shift(@$fields);
	    my $strg = "PTRARRAYEXT(${ns},${type}," . join("::", @$fields) . ")";
	    printf $fh ("%-60s // %s\n", $strg, $src);
	}
    }

    for my $ns (sort keys %int_array_ext) {
	for my $type (sort keys %{$int_array_ext{$ns}}) {
	    my $fields = $int_array_ext{$ns}->{$type};
	    my $src = shift(@$fields);
	    my $inttype = join('::', @$fields);
	    my $safe_inttype = join('_', split(/\s+/, $inttype));
	    my $strg = "INTARRAYEXT(${ns},${type},${safe_inttype},${inttype})";
	    printf $fh ("%-60s // %s\n", $strg, $src);
	}
    }

    for my $ns (sort keys %string_array_ext) {
	for my $type (sort keys %{$string_array_ext{$ns}}) {
	    printf $fh ("%-60s // %s\n", "STRINGARRAYEXT(${ns},${type})", $string_array_ext{$ns}->{$type});
	}
    }

    for my $ns (sort keys %point_array_ext) {
	for my $type (sort keys %{$point_array_ext{$ns}}) {
	    printf $fh ("%-60s // %s\n", "POINTARRAYEXT(${ns},${type})", $point_array_ext{$ns}->{$type});
	}
    }

    for my $ns (sort keys %box_array_ext) {
	for my $type (sort keys %{$box_array_ext{$ns}}) {
	    printf $fh ("%-60s // %s\n", "BOXARRAYEXT(${ns},${type})", $box_array_ext{$ns}->{$type});
	}
    }

    for my $type (sort keys %int_subset) {
	my $safe_type = join('_', split(/\s+/, $type));
	printf $fh ("%-60s // %s\n", "INTSUBSET(${safe_type},${type})", $int_subset{$type});
    }

    for my $ns (sort keys %subset) {
	for my $type (sort keys %{$subset{$ns}}) {
	    printf $fh ("%-60s // %s\n", "SUBSETCLASS(${ns},${type})", $subset{$ns}->{$type});
	}
    }

    for my $ns (sort keys %ptr_subset) {
	for my $type (sort keys %{$ptr_subset{$ns}}) {
	    printf $fh ("%-60s // %s\n", "PTRSUBSETCLASS(${ns},${type})", $ptr_subset{$ns}->{$type});
	}
    }

    for my $ns (sort keys %subset_ext) {
	for my $type (sort keys %{$subset_ext{$ns}}) {
	    my $fields = $subset_ext{$ns}->{$type};
	    my $src = shift(@$fields);
	    my $strg = "SUBSETEXT(${ns},${type}," . join("::", @$fields) . ")";
	    printf $fh ("%-60s // %s\n", $strg, $src);
	}
    }

    for my $ns (sort keys %ptr_subset_ext) {
	for my $type (sort keys %{$ptr_subset_ext{$ns}}) {
	    my $fields = $ptr_subset_ext{$ns}->{$type};
	    my $src = shift(@$fields);
	    my $strg = "PTRSUBSETEXT(${ns},${type}," . join("::", @$fields) . ")";
	    printf $fh ("%-60s // %s\n", $strg, $src);
	}
    }

    for my $ns (sort keys %int_subset_ext) {
	for my $type (sort keys %{$int_subset_ext{$ns}}) {
	    my $fields = $int_subset_ext{$ns}->{$type};
	    my $src = shift(@$fields);
	    my $inttype = join('::', @$fields);
	    my $safe_inttype = join('_', split(/\s+/, $inttype));
	    my $strg = "INTSUBSETEXT(${ns},${type},${safe_inttype},${inttype})";
	    printf $fh ("%-60s // %s\n", $strg, $src);
	}
    }

    for my $ns (sort keys %string_subset_ext) {
	for my $type (sort keys %{$string_subset_ext{$ns}}) {
	    printf $fh ("%-60s // %s\n", "STRINGSUBSETEXT(${ns},${type})", $string_subset_ext{$ns}->{$type});
	}
    }

    for my $ns (sort keys %point_subset_ext) {
	for my $type (sort keys %{$point_subset_ext{$ns}}) {
	    printf $fh ("%-60s // %s\n", "POINTSUBSETEXT(${ns},${type})", $point_subset_ext{$ns}->{$type});
	}
    }

    for my $ns (sort keys %box_subset_ext) {
	for my $type (sort keys %{$box_subset_ext{$ns}}) {
	    printf $fh ("%-60s // %s\n", "BOXSUBSETEXT(${ns},${type})", $box_subset_ext{$ns}->{$type});
	}
    }

    close($fh);
}

sub dump_collections {
    my ($tu, $header_map, $outdir) = @_;

    if (! -d $outdir) {
	carp "$outdir is not a directory.";
	return;
    }    

    my %collection_records;

    for my $type_node ($tu->getNodesByType(TU::TYPE_DECL)) {
	my $type_name = $type_node->getName();
	next unless ($type_name and $type_name =~ /^oaCollection<(.*),(.*)>$/);
	my $param1 = $1;
	my $param2 = $2;
	next unless (length($param1) > 1 and length($param2) > 1);
	my $record = $type_node->getRef('type');
	next unless ($record && $record->getType() == TU::RECORD_TYPE);
	$collection_records{$record->getNum()} = [ $param1, $param2 ];
    }

    my %coll2subdir;
    my %param2subdir;
    
    for my $func_node ($tu->getNodesByType(TU::FUNCTION_DECL)) {
	my $full_src = $func_node->getField('srcp');
	my $src = (split(/:/, $full_src, 2))[0];
	my $func_type = $func_node->getRef('type');
	next unless $func_type;
	next unless ($func_type->getType() == TU::METHOD_TYPE || $func_type->getType() == TU::FUNCTION_TYPE);
	my $retn = $func_type->getRef('retn') || next;
	my $params = $collection_records{$retn->getNum()} || next;
	
	push(@$params, $full_src);

	if (exists($header_map->[0]->{$src})) {
	    my $subdir = $header_map->[0]->{$src};
	    $coll2subdir{$retn->getNum()}->{$subdir} = $params;
	    next if exists($param2subdir{$params->[0]}->{$subdir});
	    $param2subdir{$params->[0]}->{$subdir} = $params;
	}
    }

    my %sorted_colls;
    my %sorted_params;

    for my $type (keys %coll2subdir) {
	for my $subdir (@modules) {
	    if (exists($coll2subdir{$type}->{$subdir})) {
		push(@{$sorted_colls{$subdir}}, $coll2subdir{$type}->{$subdir});
		last;
	    }
	}
    }

    for my $type (keys %param2subdir) {
	for my $subdir (@modules) {
	    if (exists($param2subdir{$type}->{$subdir})) {
		push(@{$sorted_params{$subdir}}, $param2subdir{$type}->{$subdir});
		last;
	    }
	}
    }

    for my $subdir (@modules) {
	my $fh;
	my $fh_pre;
	my $lc_subdir = lc($subdir);

	# Output files $fh and $fh_pre contain the same data, except that
	# $fh contains macro COLLCLASS(), which is to be evaluated after the #includes,
	# and $fh_pre contains COLLCLASSPRE(), to be evaluated before the #includes.
	open ($fh,     "> $outdir/${lc_subdir}_collections.i");
	open ($fh_pre, "> $outdir/${lc_subdir}_collections_pre.i");
	if (!$fh) {
	    carp "Couldn't open $outdir/${lc_subdir}_collections.i for write.";
	    next;
	}

	my $coll_types = $sorted_colls{$subdir};
	for my $params (@$coll_types) {
	    my $macro_args = sprintf("(%s, %s, %s, %s)", split(/::/, $params->[0]), split(/::/, $params->[1]));
	    printf $fh     ("COLLCLASS%-80s // %s\n",    $macro_args, $params->[2]);
	    printf $fh_pre ("COLLCLASSPRE%-77s // %s\n", $macro_args, $params->[2]);
	}

	close($fh);
	close($fh_pre);

	open ($fh, "> $outdir/${lc_subdir}_iters.i");
	if (!$fh) {
	    carp "Couldn't open $outdir/${lc_subdir}_iters.i for write.";
	    next;
	}

	my $iter_types = $sorted_params{$subdir};
	for my $params (@$iter_types) {
	    my $macro_string = sprintf("ITERCLASS(%s, %s)", split(/::/, $params->[0]));
	    printf $fh ("%-80s // %s\n", $macro_string, $params->[2]);
	}

	close($fh);
    }
}

sub dump_enums {
    my ($tu, $header_map, $outdir) = @_;

    if (! -d $outdir) {
	carp "$outdir is not a directory.";
	return;
    }    

    my %enums;

    for my $node ($tu->getNodesByType(TU::ENUMERAL_TYPE)) {
	my $type = $node->getRef('name');
	next unless ($type && $type->getType() == TU::TYPE_DECL);
	my $typename = $type->getName();
	next if ($typename =~ /^\./);
	my $ns = $type->getScope();
	next unless ($ns && ($ns eq 'OpenAccess_4' or grep { 'oa' . lc($_) eq lc($ns) } @modules));
	my $full_src = $type->getField('srcp');
	my $src = (split(/:/, $full_src, 2))[0];

        if (exists($header_map->[0]->{$src})) {
            my $subdir = $header_map->[0]->{$src};
	    $enums{$typename}->{$subdir} = [ $ns, $typename, $full_src ];
        }
    }

    my %enum_wrappers;

    for my $node ($tu->getNodesByType(TU::FUNCTION_DECL)) {
	my $func_name = $node->getName();
	next if (!$func_name or $func_name ne 'operator');
	my $full_src = $node->getField('srcp');
	my $src = (split(/:/, $full_src, 2))[0];
	my $class = $node->getRef('scpe');
	next unless ($class and $class->getType() == TU::RECORD_TYPE);
	my $classname = $class->getName();
	my $classns = $class->getScope() || "";
	next if $classns eq 'std';
        next if $classns eq 'SchemaDefinition_1';
	my $type = $node->getRef('type');
	next unless ($type and $type->getType() == TU::METHOD_TYPE);
	my $retn = $type->getRef('retn');
	next unless ($retn and $retn->getType() == TU::ENUMERAL_TYPE);
	my $retn_type = $retn->getRef('name');
	next unless ($retn_type and $retn_type->getType() == TU::TYPE_DECL);
	my $retn_typename = $retn_type->getName();
	my $retn_ns = $retn_type->getScope();
	next unless ($retn_typename and $retn_ns);

        if (exists($header_map->[0]->{$src})) {
            my $subdir = $header_map->[0]->{$src};
	    $enum_wrappers{$classname}->{$subdir} = [ $classns, $classname, $retn_ns, $retn_typename, $full_src ];
        }
    }

    my %sorted_enums;
    my %sorted_enum_wrappers;

    for my $type (keys %enums) {
        for my $subdir (@modules) {
            if (exists($enums{$type}->{$subdir})) {
                push(@{$sorted_enums{$subdir}}, $enums{$type}->{$subdir});
                last;
            }
        }
    }

    for my $type (keys %enum_wrappers) {
        for my $subdir (@modules) {
            if (exists($enum_wrappers{$type}->{$subdir})) {
                push(@{$sorted_enum_wrappers{$subdir}}, $enum_wrappers{$type}->{$subdir});
                last;
            }
        }
    }

    for my $subdir (@modules) {
        my $fh;
        my $lc_subdir = lc($subdir);

        if (!open($fh, "> $outdir/${lc_subdir}_enums.i")) {
            carp "Couldn't open $outdir/${lc_subdir}_enums.i for write.";
            return;
        }

        my $enum_types = $sorted_enums{$subdir};
        for my $params (@$enum_types) {
            my $ns = $params->[0];
            my $type = $params->[1];
            my $src = $params->[2];
	    printf $fh ("%-60s // %s\n", "ENUM(${ns},${type})", $src);
        }

        my $enum_wrapper_types = $sorted_enum_wrappers{$subdir};
        for my $params (@$enum_wrapper_types) {
            my $class_ns = $params->[0];
            my $class_name = $params->[1];
            my $enum_ns = $params->[2];
            my $enum_name = $params->[3];
            my $src = $params->[4];
	    printf $fh ("%-60s // %s\n", "ENUMWRAPPER(${class_ns}, ${class_name}, ${enum_ns}, ${enum_name})", $src);
        }

        close($fh);
    }

    my $fh;
    if (!open($fh, "> $outdir/oa_enums.i")) {
        carp "Couldn't open $outdir/oa_enums.i for write.";
        return;
    }

    for my $subdir (@modules) {
        my $lc_subdir = lc($subdir);
        printf $fh ("\%\%include \"$outdir/${lc_subdir}_enums.i\"\n");
    }

    close($fh);
}

sub expand_access {
    my ($acc_str) = @_;

    my %mapping = ('pub'  => 'public',
                   'prot' => 'protected',
                   'priv' => 'private',
                   ''     => '');

    if (defined($acc_str)) {
        (exists($mapping{$acc_str})) ? $mapping{$acc_str} : $acc_str;
    } else {
        '';
    }
}

sub dump_yaml {
    my ($tu, $header_map, $outfile) = @_;

    my $tu_debug = 0;

    my $fh;
    if (!open($fh, "> $outfile")) {
	carp "Couldn't open $outfile for write.";
	return;
    }

    my %classes;

    for my $node ($tu->getNodesByType(TU::FUNCTION_DECL)) {

        my $fn_name = $node->getName();
        next if (!$fn_name);
        my $fn_access = &expand_access($node->getField('accs'));
        my $class = $node->getRef('scpe');
        next unless ($class and $class->getType() == TU::RECORD_TYPE);
        my $classname = $class->getName();
        my $classns = $class->getScope() || "";
        next if $classns eq 'std';

        $classes{$classname}{'node'} = $class;
        $classes{$classname}{'access'} = &expand_access($class->getField('accs'));
        $classes{$classname}{'tuid'} = $class->getNum();

	my $type = $node->getRef('type');
	next unless ($type);
	my $retn = $type->getRef('retn');
	next unless ($retn);
        my %retn_hash = $retn->getTypeHash();

        my $fn_yaml = '';
        $fn_yaml .= "      - name: \"$fn_name\"\n";
        $fn_yaml .= "        tuid: ".$node->getNum()."\n" if ($tu_debug);
        $fn_yaml .= "        access: \"$fn_access\"\n";
        $fn_yaml .= "        return:\n";
        $fn_yaml .= "          tuid: ".$retn->getNum()."\n" if ($tu_debug);
        $fn_yaml .= "          const: true\n" if ($retn_hash{'const'});
        $fn_yaml .= "          type: \"$retn_hash{'name'}\"\n";
        $fn_yaml .= "          ptr: true\n" if ($retn_hash{'ptr'});
        $fn_yaml .= "          ref: true\n" if ($retn_hash{'ref'});

        my @args = ();
        my @func_args = $node->getFunctionArgs();
        if (($#func_args+1) > 0) {
            $fn_yaml .= "        args:\n";
            for my $arg_node ($node->getFunctionArgs()) {
                my $arg_name = ($arg_node->getName() || '?');
                my $arg_fqn = ($arg_node->getFQN() || '?');
                my $arg_type = $arg_node->getRef('type');
                my %arg_hash = $arg_type->getTypeHash();
                $fn_yaml .= "          - name: \"$arg_fqn\"\n";
                $fn_yaml .= "            tuid: ".$arg_node->getNum()."\n" if ($tu_debug);
                $fn_yaml .= "            const: true\n" if ($arg_hash{'const'});
                $fn_yaml .= "            type: \"$arg_hash{'name'}\"\n";
                $fn_yaml .= "            ptr: true\n" if ($arg_hash{'ptr'});
                $fn_yaml .= "            ref: true\n" if ($arg_hash{'ref'});
            }
        } else {
            $fn_yaml .= "        args: []\n";
        }

        unless (exists ${classes{$classname}{'fns'}{$fn_name}}) {
            $classes{$classname}{'fns'}{$fn_name} = ();
        }
        push(@{$classes{$classname}{'fns'}{$fn_name}}, $fn_yaml);

    }

    printf($fh "classes:\n\n");
    for my $classname (sort keys %classes) {

        my $class = $classes{$classname}{'node'};

        printf($fh "### CLASS: $classname ###\n");
        printf($fh "  - name: \"$classname\"\n");
        printf($fh "    access: \"$classes{$classname}{'access'}\"\n");
        printf($fh "    tuid: $classes{$classname}{'tuid'}\n") if ($tu_debug);

        my $class_flds = $class->getRef('flds');
        if ($class_flds) {
            my $full_src = $class_flds->getField('srcp');
            if ($full_src) {
                my $src = (split(/:/, $full_src, 2))[0];
                if (exists($header_map->[0]->{$src})) {
                    printf($fh "    module: \"".$header_map->[0]->{$src}."\"\n");
                }
                printf($fh "    header: \"".$src."\"\n");
            }
        }

        # Capture inheritance
        my @base_names = $class->getBaseNames();
        if (($#base_names+1) > 0) {
            printf($fh "    inherits:\n");
            for my $base_name (@base_names) {
                printf($fh "      - name: \"$base_name\"\n");
            }
        } else {
            printf($fh "    inherits: []\n");
        }
        printf($fh "    functions:\n");
        for my $fn_name (sort keys %{$classes{$classname}{'fns'}}) {
            for my $fn_inst (@{$classes{$classname}{'fns'}{$fn_name}}) {
                printf($fh $fn_inst);
            }
        }
        printf($fh "\n");
    }

    close($fh);
}

sub sql_create_table {
    my ($name, @types) = @_;

    return("CREATE TABLE $name\n".
           "(\n".
           join(",\n", @types).
           "\n);\n\n");

}

sub sql_format {
    my ($string) = @_;

    if (defined($string)) {
        (($string =~ /^\d+/) ? $string : "'".$string."'")
    }
    else { 'NULL' }
}

sub sql_insert {
    my ($name, @values) = @_;
    
    return("INSERT INTO $name\n".
           "  VALUES (".(join(", ", map { &sql_format($_) } @values)).");\n");
}

sub dump_sqlite {
    my ($tu, $header_map, $outfile) = @_;

    my %class_id;
    my %type_id;
    my %func_id;
    my %cpptype_id;
    my %param_id;
    
    my $curr_class_id = 0;
    my $curr_type_id = 0;
    my $curr_func_id = 0;
    my $curr_cpptype_id = 0;
    my $curr_param_id = 0;
    my $curr_funarg_id = 0;

    my %class_fqns;
    my $num_class_fqn = 0;

    my $fh;
    if (!open($fh, "> $outfile")) {
	carp "Couldn't open $outfile for write.";
	return;
    }

    print $fh &sql_create_table("scopes",
                                "id int NOT NULL UNIQUE",
                                "name TEXT");

    print $fh &sql_create_table("classes",
                                "id int NOT NULL UNIQUE",
                                "scope_id int",
                                "name TEXT");

    print $fh &sql_create_table("functions",
                                "id int NOT NULL UNIQUE",
                                "class_id int",
                                "return_id int",
                                "name TEXT");

    print $fh &sql_create_table("cpptypes",
                                "id int NOT NULL UNIQUE",
                                "name TEXT");

    print $fh &sql_create_table("types",
                                "id int NOT NULL UNIQUE",
                                "name TEXT");

    print $fh &sql_create_table("params",
                                "id int NOT NULL UNIQUE",
                                "type_id int",
                                "cpptype_id int",
                                "name TEXT",
                                "fqn TEXT");

    print $fh &sql_create_table("funargs",
                                "id int NOT NULL UNIQUE",
                                "function_id int",
                                "param_id int",
                                "position int");

    for my $node ($tu->getNodesByType(TU::FUNCTION_DECL)) {

        my $func_name = $node->getName();
        next if (!$func_name);
        my $class = $node->getRef('scpe');
        next unless ($class and $class->getType() == TU::RECORD_TYPE);
        my $classname = $class->getName();
        my $class_scope = $class->getRef('chan');
        my $classns = $class->getScope() || "";
        next if $classns eq 'std';

        # TESTING
        #printf ("class=%s(%s) func=%s(%s)\n", $classname, $class->getNum(), $func_name, $node->getNum());
        #for my $arg_node ($node->getFunctionArgs()) {
        #    my $arg_name = ($arg_node->getName() || '?');
        #    my $arg_type = $arg_node->getRef('type');
        #    my $arg_typename = ($arg_type) ? $arg_type->getName() : '?';
        #    my $type_qual = $TU::Parser::TOKEN_REVERSE_MAP[$arg_type->getType()] || '?';
        #    printf (" > @%s arg=%s (%s; %s)\n", $arg_node->getNum(), $arg_name, $type_qual, $arg_typename);
        #}
        # END TESTING

	my $type = $node->getRef('type');
	next unless ($type);
	my $retn = $type->getRef('retn');
	next unless ($retn);
	my $retn_type = $retn->getRef('name');

        my $retn_id = 0;
        my $retn_typename = 'void';
        if ($retn_type) {
            #$retn_typename = $retn_type->getName();
            $retn_typename = $retn_type->getTypeString(1);
            $retn_id = ($retn_typename eq 'void') ? 0 : $retn_type->getNum();
            #$retn_ns = $retn_type->getScope();
            #next unless ($retn_typename and $retn_ns);
        }

        my $scope_id;
        #my $scope_fqn;
        #if ($class_scope) {
        #    my $scope_fqn = $class_scope->getFQN();
        #    if (exists $class_fqns{$scope_fqn}) {
        #        $scope_id = $class_fqns{$scope_fqn};
        #    } 
        #    else {
        #        $scope_id = $num_class_fqn += 1;
        #        $class_fqns{$scope_fqn} = $scope_id;
        #    }
        #}

        #if ($scope_id) {
        #    print $fh &sql_insert("scopes", $scope_id, $scope_fqn);
        #}

        unless (exists $type_id{$retn_typename}) {
            $type_id{$retn_typename} = $curr_type_id++;
            print $fh &sql_insert("types", $type_id{$retn_typename}, $retn_typename);
        }

        unless (exists $class_id{$classname}) {
            $class_id{$classname} = $curr_class_id++;
            print $fh &sql_insert("classes", $class_id{$classname}, $scope_id, $classname);
        }

        $func_id{$func_name} = $curr_func_id++;
        print $fh &sql_insert("functions", $func_id{$func_name}, $class_id{$classname}, $type_id{$retn_typename}, $func_name);

        my $position = 0;
        for my $arg_node ($node->getFunctionArgs()) {
            my $arg_name = ($arg_node->getName() || '?');
            my $arg_fqn = ($arg_node->getFQN() || '?');
            my $arg_type = $arg_node->getRef('type');
            my $arg_typename = ($arg_type) ? $arg_type->getName() : '?';
            my $arg_cpptype = $TU::Parser::TOKEN_REVERSE_MAP[$arg_type->getType()] || '?';
            unless (exists $cpptype_id{$arg_cpptype}) {
                $cpptype_id{$arg_cpptype} = $curr_cpptype_id++;
                print $fh &sql_insert("cpptypes", $cpptype_id{$arg_cpptype}, $arg_cpptype);
            }
            unless (exists $type_id{$arg_typename}) {
                $type_id{$arg_typename} = $curr_type_id++;
                print $fh &sql_insert("types", $type_id{$arg_typename}, $arg_typename);
            }
            my $param_key = "${type_id{$arg_typename}},${cpptype_id{$arg_cpptype}},${arg_fqn}";
            unless (exists $param_id{$param_key}) {
                $param_id{$param_key} = $curr_param_id++;
                print $fh &sql_insert("params", $param_id{$param_key}, $type_id{$arg_typename}, $cpptype_id{$arg_cpptype}, $arg_name, $arg_fqn);
            }
            print $fh &sql_insert("funargs", $curr_funarg_id++, $func_id{$func_name}, $param_id{$param_key}, $position++);
        }

    }

    close($fh);
}

sub dump_types {
    my ($tu, $header_map, $outfile) = @_;
    
    my $fh;
    open($fh, ">${outfile}") or croak "Couldn't open ${outfile} for write.";
    
    my $idnode;
    for my $node ($tu->getNodesByType(TU::IDENTIFIER_NODE)) {
	my $strg = $node->getField('strg');
	next unless ($strg and $strg eq 'objectTypeEnumVal');
	$idnode = $node;
	last;
    }
    
    croak "Couldn't find identifier_node for 'objectTypeEnumVal'." unless $idnode;

    my @types;

    for my $node ($tu->getNodesByType(TU::CONST_DECL)) {
	my $name = $node->getRef('name');
	next unless ($name and $name == $idnode);
	my $type = $node->getRef('type');
	next unless ($type and $type->getType() == TU::ENUMERAL_TYPE);
	my $cnst = $node->getRef('cnst');
	next unless ($cnst and $cnst->getType() == TU::INTEGER_CST);
	my $enum_val = $cnst->getField('low ');
	next unless defined($enum_val);
	my $class = $node->getRef('scpe');
	next unless ($class and $class->getType() == TU::RECORD_TYPE);
	my $classname = $class->getName();
	next unless ($classname and $classname =~ /oaTraits<(.*)>\s*$/);
	my $type_name = $1;
	my $swig_type_name = $type_name;
	$swig_type_name =~ s/::/__/g;
	$swig_type_name = '_p_' . $swig_type_name;
	$types[$enum_val] = [ $type_name, $swig_type_name ];
    }

    for my $i (0..$#types) {
	next unless $types[$i];
	print $fh ("OATYPENUM(" . join(", ", @{$types[$i]}, $i) . ")\n");
    }
}

my %oa_objects;

sub isOaObject {
    my ($tu, $record) = @_;
    my $type_decl = $record->getRef('name');
    return 0 unless $type_decl;
    my $classname = $type_decl->getName();
    return 0 unless ($classname && $classname !~ /<T>/);
    my $fqn = $type_decl->getFQN();
    return 1 if exists($oa_objects{$fqn});
    my $base_info = $record->getRef('binf');
    my $binf = $base_info ? $base_info->getField('binf') : [];
    for my $parent (@$binf) {
	my $parent_record = $tu->getNode($parent->[1])->getRef('type');
	if ($parent_record->getFQN() eq 'OpenAccess_4::oaObject' or isOaObject($tu, $parent_record)) {
	    $oa_objects{$fqn}++;
	    return 1;
	}
    }
    return 0;
}    

sub dump_objects {
    my ($tu, $header_map, $outfile) = @_;

    my $fh;
    open($fh, ">${outfile}") or croak "Couldn't open ${outfile} for write.";

    map { isOaObject($tu, $_) } $tu->getNodesByType(TU::RECORD_TYPE);

    map { print $fh "OAOBJECT($_)\n" } sort keys %oa_objects;

    close($fh);
}

sub dump_lookuptables {
    my ($tu, $header_map, $outfile) = @_;

    my $fh;
    open($fh, ">${outfile}") or croak "Couldn't open ${outfile} for write.";

    my %tables;

    for my $type_node ($tu->getNodesByType(TU::TYPE_DECL)) {
	my $type_name = $type_node->getName();
	next unless $type_name;
	if ($type_name =~ /oa1DLookupTbl\s*<(.*),(.*)>/) {
	    my $param1 = $1;
	    my $param2 = $2;
	    next if ($param1 eq 'T' and $param2 eq 'U');
	    my $base1 = (split(/::/, $param1))[-1];
	    $base1 =~ s/[\s\*]//g;
	    my $base2 = (split(/::/, $param2))[-1];
	    $base2 =~ s/[\s\*]//g;
	    my $template_name = "oa1DLookupTbl_${base1}_${base2}";
	    my $table = "LOOKUP1D($template_name, $param1, $param2)";
	    $tables{$table}++;
	} elsif ($type_name =~ /oa2DLookupTbl\s*<(.*),(.*),(.*)>/) {
	    my $param1 = $1;
	    my $param2 = $2;
	    my $param3 = $3;
	    my $base1 = (split(/::/, $param1))[-1];
	    $base1 =~ s/[\s\*]//g;
	    my $base2 = (split(/::/, $param2))[-1];
	    $base2 =~ s/\s//;
	    $base2 =~ s/[\s\*]//g;
	    my $base3 = (split(/::/, $param3))[-1];
	    $base3 =~ s/\s//;
	    $base3 =~ s/[\s\*]//g;
	    next if ($param1 eq 'T' and $param2 eq 'U' and $param3 eq 'V');
	    my $template_name = "oa2DLookupTbl_${base1}_${base2}_${base3}";
	    my $table = "LOOKUP2D($template_name, $param1, $param2, $param3)";
	    $tables{$table}++;
	}
    }

    map { print $fh ("$_\n") } sort keys %tables;

    close($fh);

    return 0;
}

my %oa_namespaces;

sub isOaNameSpace {
    my ($tu, $record) = @_;
    my $type_decl = $record->getRef('name');
    return 0 unless $type_decl;
    my $classname = $type_decl->getName();
    return 0 unless ($classname && $classname !~ /<T>/);
    my $fqn = $type_decl->getFQN();
    return 1 if exists($oa_namespaces{$fqn});
    my $base_info = $record->getRef('binf');
    my $binf = $base_info ? $base_info->getField('binf') : [];
    for my $parent (@$binf) {
	my $parent_record = $tu->getNode($parent->[1])->getRef('type');
	if ($parent_record->getFQN() eq 'OpenAccess_4::oaNameSpace' or isOaNameSpace($tu, $parent_record)) {
	    $oa_namespaces{$fqn}++;
	    return 1;
	}
    }
    return 0;
}    

sub dump_namespaces {
    my ($tu, $header_map, $outfile) = @_;

    my $fh;
    open($fh, ">${outfile}") or croak "Couldn't open ${outfile} for write.";

    map { isOaNameSpace($tu, $_) } $tu->getNodesByType(TU::RECORD_TYPE);

    for my $type_name (sort keys %oa_namespaces) {
        my $swig_type_name = $type_name;
        $swig_type_name =~ s/::/__/g;
        $swig_type_name = '_p_' . $swig_type_name;
        print $fh ("OANAMESPACECLASS($type_name,$swig_type_name)\n");
    }

    close($fh);
}

sub collateHeaders {
    my $dir = shift;

    return [] unless ($dir and -d $dir);

    my $result = [];

    my $debug = exists($opts{d});
    print STDOUT ("Crawling ${dir} for header files ...\n") if $debug;

    for my $subdir (@modules) {
	print STDOUT ("  Looking in ${dir}/oa/src/${subdir} ...\n") if $debug;
	if (not -d "${dir}/oa/src/${subdir}") {
	    carp "Couldn't find directory '${dir}/oa/src/${subdir}'.";
	}

	my @headers;
	find(sub { /\.(cpp|h|inl)$/ && push(@headers, "$File::Find::name") }, "${dir}/oa/src/${subdir}");

	for my $header (@headers) {
	    my $header_base = basename($header);
	    print STDOUT ("    ${header_base}\n") if $debug;
	    $result->[0]->{$header_base} = $subdir;
	    push(@{$result->[1]->{$subdir}}, $header_base);
	}
    }

    return $result;
}
