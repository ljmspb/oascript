#! /usr/bin/env ruby
###############################################################################
#
# Copyright 2011 Intel Corporation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
###############################################################################
#
# Change Log:
#   01/15/2011: Initial copy
#
###############################################################################

require 'getoptlong'
require File.join(File.split($0).first, 'oasptree')
require 'yaml'


##############################################################################
## USAGE ##
###########

usage = "
fnquery.rb [OPTIONS]

-y, --yaml       YAML parse tree file
-c, --class      Class query
-f, --function   Function query
-a, --arg        Argument query
-l, --local      Only show local functions (do not follow inherited classes)
-h, --help       Shorthand help
-H, --HELP       Longhand help

Query info:
* $1 represents the object to query (class, function, arg)
* Ruby syntax is used to test (<, <=, ==, !=, >=, >)
* Regular expressions are allowed: $1.name =~ /^oaDesign/
* Use --HELP to get a long listing of query examples

Examples:

fnquery.rb -y tu.yaml -c '$1.name==\"oaPolygon\"' -f '$1.args.length==0'
fnquery.rb -y tu.yaml -c '$1.name=~/^oaVia/' -f '$1.name==\"create\"'
fnquery.rb -y tu.yaml -a '$1.type==\"oaString\" && ! $1.const? && ($1.ref? || $1.ptr?)'

"

more_usage = "
Typical query attributes/methods:

CLASS
-----
$1.name          Class name (string)
$1.module        OA module name (e.g. design)
$1.header        OA header name (e.g. oaDesign.h)
$1.public?       Is this class public? (true/false)
$1.private?      Is this class private? (true/false)
$1.protected?    Is this class protected? (true/false)

FUNCTION
--------
$1.name          Function name (string)
$1.public?       Is this function public? (true/false)
$1.private?      Is this function private? (true/false)
$1.protected?    Is this function protected? (true/false)
$1.return.type   Return type
$1.return.const? Return type is const? (true/false)
$1.return.ptr?   Return type is a pointer? (true/false)
$1.return.ref?   Return type is a reference? (true/false)
$1.args.length   Argument length

ARG
---
$1.name          Argument variable name (string)
$1.type          Argument type
$1.const?        Is this argument const? (true/false)
$1.ptr?          Is this argument a pointer? (true/false)
$1.ref?          Is this argument a reference? (true/false)

"


opts = GetoptLong.new(['--yaml', '-y', GetoptLong::REQUIRED_ARGUMENT],
                      ['--class', '-c', GetoptLong::REQUIRED_ARGUMENT],
                      ['--function', '-f', GetoptLong::REQUIRED_ARGUMENT],
                      ['--arg', '-a', GetoptLong::REQUIRED_ARGUMENT],
                      ['--local', '-l', GetoptLong::OPTIONAL_ARGUMENT],
                      ['--debug', GetoptLong::OPTIONAL_ARGUMENT],
                      ['--HELP', '-H', GetoptLong::OPTIONAL_ARGUMENT],
                      ['--help', '-h', GetoptLong::OPTIONAL_ARGUMENT])

yaml = nil
klass_query = nil
function_query = nil
arg_query = nil
local = nil
debug = nil

opts.each do |opt, arg|
  case opt
  when '--help'
    abort usage
  when '--HELP'
    abort usage + more_usage
  when '--yaml'
    yaml = arg
  when '--class'
    klass_query = arg.gsub('$1', 'klass')
  when '--function'
    function_query = arg.gsub('$1', 'function')
  when '--arg'
    arg_query = arg.gsub('$1', 'arg')
  when '--local'
    local = true
  when '--debug'
    debug = true
  end
end

abort usage+"Missing mandatory --yaml argument" unless yaml

if debug
  warn 'QUERIES:'
  puts klass_query if klass_query
  puts function_query if function_query
  puts arg_query if arg_query
end

##############################################################################
## QUERY ##
###########

tree = OasParseTree::Tree::load_yaml(yaml)

tree.klasses.each do |klass|
  if klass_query.nil? or eval(klass_query)
    klass.functions(:local => local).each do |function|
      if function_query.nil? or eval(function_query)
        if arg_query
          show = function.args.any? {|arg| eval(arg_query)}
        else
          show = true
        end
        
        puts function if show
      end
    end
  end
end
