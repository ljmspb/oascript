#!/usr/bin/perl
# Fix the few OpenAccess header files that are incompatible with SWIG.
#
# Copyright 2009 Advanced Micro Devices, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

use strict;
use warnings;

use Carp;
use Getopt::Long qw(:config pass_through);

my %opts;
GetOptions(\%opts, "o=s", "oaroot=s");

my $oaroot;

if (exists($opts{oaroot})) {
    $oaroot = $opts{oaroot};
} elsif (exists($ENV{OAROOT})) {
    $oaroot = $ENV{OAROOT};
} else {
    croak "Can't determine OA installation root (try -oaroot option)."
}

my $outdir = ".";

if (exists($opts{o})) {
    $outdir = $opts{o};
    if (! -d $opts{o}) {
	croak "-o option must be a directory.";
    }
}

#-------------------------------------------------------------------------------
# swig chokes on 'SPtr<T>::operator T*&()' in oaCommonSPtr.h.

my $hfile = "oaCommonSPtr.h";
open(OAHFILE,  "< ${oaroot}/include/oa/${hfile}") or die "Couldn't open '${oaroot}/include/oa/${hfile}' for read.";
open(OUTHFILE, "> ${outdir}/${hfile}")            or die "Couldn't open '${outdir}/${hfile}' for write.";
while (<OAHFILE>) {
	if (/^\s*operator\s*T\*&\s*()/) {
		print OUTHFILE "// SWIG cannot handle 'SPtr<T>::operator T*&()'\n";
		print OUTHFILE "#ifndef SWIG\n";
		print OUTHFILE;
		print OUTHFILE "#endif\n";
	} else {
		print OUTHFILE;
	}
}
close(OAHFILE);
close(OUTHFILE);
